

CREATE SCHEMA testing;


CREATE TABLE  testing.student (
    "studentID" bigint NOT NULL,
    name character varying(60) NOT NULL,
    dob date NOT NULL,
    sex character varying(10) NOT NULL,
    religion character varying(25),
    email character varying(40),
    phone text,
    address text,
    "classesID" integer NOT NULL,
    "sectionID" integer NOT NULL,
    section character varying(60) NOT NULL,
    roll text NOT NULL,
    library integer DEFAULT 0 NOT NULL,
    hostel integer DEFAULT 0 NOT NULL,
    transport integer DEFAULT 0 NOT NULL,
    create_date date DEFAULT now() NOT NULL,
    totalamount character varying(128),
    paidamount character varying(128),
    photo character varying(200),
    "parentID" integer NOT NULL,
    year integer,
    username character varying(40) NOT NULL,
    password character varying(128) NOT NULL,
    usertype character varying(20) NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    parent_type character varying(50),
    academic_year_id bigint,
    health character varying(500),
   health_other text,
nationality character varying(500),
    status integer DEFAULT 1
);




COMMENT ON COLUMN    testing.student.status IS '0=not active, 1=active, 2=finish school';




CREATE TABLE testing.subject_section (
    subject_section_id integer NOT NULL,
    subject_id bigint,
    section_id bigint,
    created_at timestamp without time zone DEFAULT now()
);


ALTER TABLE testing.subject_section OWNER TO postgres;



CREATE TABLE testing.subject_student (
    subject_student_id integer NOT NULL,
    subject_id integer,
    student_id integer,
    academic_year_id integer,
    created_at timestamp without time zone DEFAULT now()
);


ALTER TABLE testing.subject_student OWNER TO postgres;



CREATE TABLE testing.mark (
    "markID" bigint NOT NULL,
    "examID" integer NOT NULL,
    exam character varying(60) NOT NULL,
    "studentID" integer NOT NULL,
    "classesID" integer NOT NULL,
    "subjectID" integer NOT NULL,
    subject character varying(60) NOT NULL,
    mark integer,
    year integer NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    postion integer,
    academic_year_id smallint
);


ALTER TABLE testing.mark OWNER TO super;



COMMENT ON COLUMN    testing.mark.postion IS 'for this mark, this student has take which position among the whole student in that subject';




CREATE TABLE testing.subject (
    "subjectID" bigint NOT NULL,
    "classesID" integer NOT NULL,
    "teacherID" integer NOT NULL,
    subject character varying(60) NOT NULL,
    subject_author character varying(100),
    subject_code text NOT NULL,
    teacher_name character varying(60) NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    is_counted integer DEFAULT 1,
    is_penalty integer DEFAULT 0,
    pass_mark integer,
    grade_mark character(5) DEFAULT 0
);


ALTER TABLE testing.subject OWNER TO super;



COMMENT ON COLUMN    testing.subject.is_counted IS '1=counted in grading,0=not counted';




COMMENT ON COLUMN    testing.subject.is_penalty IS '0=no penalty, 1=has penalty when student failed';




COMMENT ON COLUMN    testing.subject.pass_mark IS 'Subject can have a specific pass mark. For example, GS, Math etc';



COMMENT ON COLUMN    testing.subject.grade_mark IS '0=it use normal grade mark or otherwise place a default value if someone pass above this mark. For example, GS, if someone pass, just put S';




CREATE TABLE testing.academic_year (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    class_level_id smallint NOT NULL,
    created_at timestamp(0) without time zone DEFAULT ('now'::text)::timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone,
 start_date date,
  end_date date,
    status integer
);


ALTER TABLE testing.academic_year OWNER TO postgres;


COMMENT ON COLUMN    testing.academic_year.status IS '1=active, 2=next year, 0=not active';



CREATE SEQUENCE   testing.accademic_year_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.accademic_year_id_seq OWNED BY testing.academic_year.id;




CREATE TABLE testing.alert (
    "alertID" bigint NOT NULL,
    "noticeID" integer NOT NULL,
    notice_for character varying NOT NULL,
    username character varying(128) NOT NULL,
    usertype character varying(128) NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);



CREATE SEQUENCE   testing.alert_alertid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.alert_alertid_seq OWNED BY alert."alertID";



CREATE TABLE testing.attendance (
    "attendanceID" bigint NOT NULL,
    "studentID" integer NOT NULL,
    "classesID" integer NOT NULL,
    "userID" integer NOT NULL,
    usertype character varying(20) NOT NULL,
    monthyear character varying(10) NOT NULL,
    a1 character varying(3),
    a2 character varying(3),
    a3 character varying(3),
    a4 character varying(3),
    a5 character varying(3),
    a6 character varying(3),
    a7 character varying(3),
    a8 character varying(3),
    a9 character varying(3),
    a10 character varying(3),
    a11 character varying(3),
    a12 character varying(3),
    a13 character varying(3),
    a14 character varying(3),
    a15 character varying(3),
    a16 character varying(3),
    a17 character varying(3),
    a18 character varying(3),
    a19 character varying(3),
    a20 character varying(3),
    a21 character varying(3),
    a22 character varying(3),
    a23 character varying(3),
    a24 character varying(3),
    a25 character varying(3),
    a26 character varying(3),
    a27 character varying(3),
    a28 character varying(3),
    a29 character varying(3),
    a30 character varying(3),
    a31 character varying(3),
    created_at timestamp without time zone DEFAULT now()
);



CREATE SEQUENCE testing.attendance_attendanceid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.attendance_attendanceid_seq OWNED BY attendance."attendanceID";



CREATE TABLE testing.automation_rec (
    "automation_recID" bigint NOT NULL,
    "studentID" integer NOT NULL,
    date date NOT NULL,
    day character varying(3) NOT NULL,
    month character varying(3) NOT NULL,
    year integer NOT NULL,
    nofmodule integer NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);



CREATE SEQUENCE testing.automation_rec_automation_recid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE  testing.automation_rec_automation_recid_seq OWNED BY automation_rec."automation_recID";


CREATE TABLE testing.automation_shudulu (
    "automation_shuduluID" bigint NOT NULL,
    date date NOT NULL,
    day character varying(3) NOT NULL,
    month character varying(3) NOT NULL,
    year integer NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);


COMMENT ON TABLE testing.automation_shudulu IS 'This table is used to schedulate all cron process and schedulated processes needed to run in the application';


CREATE SEQUENCE   testing.automation_shudulu_automation_shuduluid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




ALTER SEQUENCE  testing.automation_shudulu_automation_shuduluid_seq OWNED BY automation_shudulu."automation_shuduluID";



CREATE TABLE testing.bank_account (
    bank_account_id integer NOT NULL,
    bank_name character varying(90),
    account_number character varying(50),
    currency character varying(10),
    bank_branch character varying,
    bank_swiftcode character varying,
    note text,
    created_at timestamp without time zone DEFAULT now(),
    account_name character varying(500)
);


COMMENT ON TABLE testing.bank_account IS 'each school will have a bank account';



CREATE SEQUENCE testing.bank_account_bank_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE  testing.bank_account_bank_account_id_seq OWNED BY bank_account.bank_account_id;



CREATE TABLE testing.book (
    "bookID" bigint NOT NULL,
    book character varying(150) NOT NULL,
    subject text NOT NULL,
    author character varying NOT NULL,
    quantity integer NOT NULL,
    due_quantity integer NOT NULL,
    rack text NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    edition character varying(250),
    user_id integer,
    book_for character varying(30)
);



COMMENT ON COLUMN    testing.book.book_for IS 'only teachers, students';



CREATE SEQUENCE testing.book_bookid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




ALTER SEQUENCE  testing.book_bookid_seq OWNED BY book."bookID";



CREATE TABLE testing.book_class (
    book_class_id integer NOT NULL,
    book_id bigint,
    classes_id bigint,
    created_at timestamp without time zone DEFAULT now()
);



CREATE SEQUENCE testing.book_class_book_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.book_class_book_class_id_seq OWNED BY book_class.book_class_id;



CREATE TABLE testing.category (
    "categoryID" bigint NOT NULL,
    "hostelID" integer NOT NULL,
    class_type character varying(60) NOT NULL,
    hbalance character varying(20) NOT NULL,
    note text,
    created_at timestamp without time zone DEFAULT now()
);




CREATE SEQUENCE   testing.category_categoryid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




ALTER SEQUENCE  testing.category_categoryid_seq OWNED BY category."categoryID";



CREATE TABLE testing.class_exam (
    class_exam_id integer NOT NULL,
    class_id bigint,
    exam_id bigint
);


CREATE SEQUENCE   testing.class_exam_class_exam_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.class_exam_class_exam_id_seq OWNED BY class_exam.class_exam_id;


CREATE TABLE testing.classlevel (
    classlevel_id integer NOT NULL,
    name character varying(50),
    start_date character varying(50),
    end_date character varying(50),
    span_number integer,
    created_at timestamp without time zone DEFAULT now(),
    updated_at timestamp without time zone,
    note text,
    result_format character varying(15)
);


COMMENT ON COLUMN    testing.classlevel.name IS 'class level name eg o-level,a-level etc';



COMMENT ON COLUMN    testing.classlevel.start_date IS 'character value, which date (month and day) do the level (semester) start ';


COMMENT ON COLUMN    testing.classlevel.end_date IS 'character value, which date (month and day) do the level (semester) end';

COMMENT ON COLUMN    testing.classlevel.span_number IS 'No of years this level takes. Eg , for o-level it will be 4 years';

COMMENT ON COLUMN    testing.classlevel.updated_at IS 'when did last time updated';


COMMENT ON COLUMN    testing.classlevel.note IS 'any notes';



COMMENT ON COLUMN    testing.classlevel.result_format IS 'Fixed type. This depends on country and school curriculumn. Once specified, it must have a fixed formula derived according to that curriculum, for example, ACSEE, CSEE, etc';


CREATE SEQUENCE   testing.class_level_class_level_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.class_level_class_level_id_seq OWNED BY classlevel.classlevel_id;


CREATE TABLE testing.classes (
    "classesID" bigint NOT NULL,
    classes character varying(60) NOT NULL,
    classes_numeric integer NOT NULL,
    "teacherID" integer NOT NULL,
    note text,
    created_at timestamp without time zone DEFAULT now(),
    classlevel_id integer
);



COMMENT ON COLUMN    testing.classes.classlevel_id IS 'This class belongs to which level';



CREATE SEQUENCE   testing.classes_classesid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.classes_classesid_seq OWNED BY classes."classesID";


CREATE VIEW   testing.core_option_subject_count AS
    SELECT count(DISTINCT a.subject_id) AS core_subjects, b."studentID", count(DISTINCT c.subject_id) AS option_subjects, (count(DISTINCT a.subject_id) + count(DISTINCT c.subject_id)) AS total_subjects FROM ((testing.subject_section a JOIN testing.student b ON ((b."sectionID" = a.section_id))) LEFT JOIN testing.subject_student c ON ((c.student_id = b."studentID"))) GROUP BY b."studentID";



CREATE TABLE testing.eattendance (
    "eattendanceID" bigint NOT NULL,
    "examID" integer NOT NULL,
    "classesID" integer NOT NULL,
    "subjectID" integer NOT NULL,
    date date NOT NULL,
    "studentID" integer,
    s_name character varying(60),
    eattendance character varying(20),
    year integer NOT NULL,
    eextra character varying(60),
    created_at timestamp without time zone DEFAULT now()
);



CREATE SEQUENCE   testing.eattendance_eattendanceid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




ALTER SEQUENCE  testing.eattendance_eattendanceid_seq OWNED BY eattendance."eattendanceID";


CREATE TABLE testing.email (
    email_id integer NOT NULL,
    body text,
    subject text,
    users_id integer DEFAULT 0,
    created_at timestamp without time zone DEFAULT now(),
    status integer DEFAULT 0,
    email character varying(250)
);


ALTER TABLE testing.email OWNER TO postgres;

COMMENT ON COLUMN    testing.email.status IS '0=pending, 1=sent,2=failed';

CREATE SEQUENCE   testing.email_email_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE  testing.email_email_id_seq OWNED BY email.email_id;


CREATE TABLE testing.exam (
    "examID" bigint NOT NULL,
    exam character varying(60) NOT NULL,
    date date NOT NULL,
    semister_id smallint NOT NULL,
    note text,
    CONSTRAINT exam_semister_id_fkey FOREIGN KEY (semister_id)
      REFERENCES new.semister (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
    created_at timestamp without time zone DEFAULT now()
);


CREATE SEQUENCE   testing.exam_examid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




ALTER SEQUENCE  testing.exam_examid_seq OWNED BY exam."examID";


CREATE TABLE testing.exam_report (
    exam_report_id integer NOT NULL,
    "classesID" integer,
    combined_exams character varying(25),
    name character varying(180),
    created_at timestamp without time zone DEFAULT now(),
    updated_at timestamp without time zone,
    rank integer,
    remark text,
    student_id integer
);


COMMENT ON COLUMN    testing.exam_report.combined_exams IS 'combined reports exam ID';



CREATE SEQUENCE   testing.exam_report_exam_report_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.exam_report_exam_report_id_seq OWNED BY exam_report.exam_report_id;


CREATE TABLE testing.examschedule (
    "examscheduleID" bigint NOT NULL,
    "examID" integer NOT NULL,
    "classesID" integer NOT NULL,
    "sectionID" integer NOT NULL,
    "subjectID" integer NOT NULL,
    edate date NOT NULL,
    examfrom character varying(10) NOT NULL,
    examto character varying(10) NOT NULL,
    room text,
    year integer NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);



CREATE SEQUENCE   testing.examschedule_examscheduleid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




ALTER SEQUENCE  testing.examschedule_examscheduleid_seq OWNED BY examschedule."examscheduleID";


CREATE TABLE testing.expense (
    "expenseID" bigint NOT NULL,
    create_date date NOT NULL,
    date date NOT NULL,
    expense character varying(128) NOT NULL,
    amount character varying(11) NOT NULL,
    "userID" integer NOT NULL,
    uname character varying(60) NOT NULL,
    usertype character varying(20) NOT NULL,
    expenseyear integer NOT NULL,
    note text,
    created_at timestamp without time zone DEFAULT now(),
    "categoryID" integer NOT NULL,
    is_depreciation character(1)
);




CREATE SEQUENCE   testing."expense_categoryID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing."expense_categoryID_seq" OWNED BY expense."categoryID";


CREATE SEQUENCE   testing.expense_expenseid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE  testing.expense_expenseid_seq OWNED BY expense."expenseID";


CREATE TABLE testing.feetype (
    "feetypeID" bigint NOT NULL,
    feetype character varying(60) NOT NULL,
    note text,
    created_at timestamp without time zone DEFAULT now(),
    amount integer,
    is_repeative character varying(1),
    enddate date,
    startdate date,
    bank_account_id bigint
);



COMMENT ON COLUMN    testing.feetype.amount IS 'amount needed to be paid';


COMMENT ON COLUMN    testing.feetype.is_repeative IS '0=not repeative
1=repeative (this means this kind of fee will continue to appear)
';


COMMENT ON COLUMN    testing.feetype.enddate IS 'end date to which all students are required to pay for this kind of fee. ';


COMMENT ON COLUMN    testing.feetype.startdate IS 'time this fee is required to start to be paid.  if this fee is repeative, when this day is reached in the next year, system will automatically generate invoice for each member affected by this fee typei';

CREATE TABLE testing.feetype_category (
    "feetype_categoryID" integer NOT NULL,
    feetype_name character varying(500),
    created_date character varying(500),
    note character varying(500)
);




CREATE SEQUENCE   testing."feetype_category_feetype_categoryID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing."feetype_category_feetype_categoryID_seq" OWNED BY feetype_category."feetype_categoryID";


CREATE SEQUENCE   testing.feetype_feetypeid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




ALTER SEQUENCE  testing.feetype_feetypeid_seq OWNED BY feetype."feetypeID";

CREATE TABLE testing.financial_category (
    "categoryID" bigint NOT NULL,
    name character varying(500),
    create_date timestamp without time zone,
    "financialID" bigint,
    note character varying(500)
);



CREATE SEQUENCE   testing."financial_category_categoryID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




ALTER SEQUENCE  testing."financial_category_categoryID_seq" OWNED BY financial_category."categoryID";



CREATE TABLE testing.financial_statement (
    name character varying(200),
    create_date timestamp without time zone,
    "financialID" bigint NOT NULL,
    note character varying(500)
);



CREATE SEQUENCE   testing."financial_statement_financialID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing."financial_statement_financialID_seq" OWNED BY financial_statement."financialID";

CREATE TABLE testing.grade (
    "gradeID" bigint NOT NULL,
    grade character varying(60) NOT NULL,
    point character varying(11) NOT NULL,
    gradefrom integer NOT NULL,
    gradeupto integer NOT NULL,
    note text,
    created_at timestamp without time zone DEFAULT now(),
    classlevel_id integer,
    overall_note text
);




COMMENT ON COLUMN    testing.grade.classlevel_id IS 'this grade is for which class level';


CREATE SEQUENCE   testing.grade_gradeid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.grade_gradeid_seq OWNED BY grade."gradeID";



CREATE TABLE testing.hmember (
    "hmemberID" bigint NOT NULL,
    "hostelID" integer NOT NULL,
    "categoryID" integer NOT NULL,
    "studentID" integer NOT NULL,
    hbalance character varying(20),
    hjoindate date NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);



CREATE SEQUENCE   testing.hmember_hmemberid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




ALTER SEQUENCE  testing.hmember_hmemberid_seq OWNED BY hmember."hmemberID";


CREATE TABLE testing.hostel (
    "hostelID" bigint NOT NULL,
    name character varying(128) NOT NULL,
    htype character varying(11) NOT NULL,
    address character varying(200) NOT NULL,
    note text,
    created_at timestamp without time zone DEFAULT now()
);



CREATE SEQUENCE   testing.hostel_hostelid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE testing.hostel_hostelid_seq OWNER TO super;

ALTER SEQUENCE  testing.hostel_hostelid_seq OWNED BY hostel."hostelID";


CREATE TABLE testing.ini_config (
    "configID" integer NOT NULL,
    type character varying(255) NOT NULL,
    config_key character varying(255) NOT NULL,
    value character varying(255) NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);




CREATE SEQUENCE   testing.ini_config_configid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.ini_config_configid_seq OWNED BY ini_config."configID";


CREATE TABLE testing.invoice (
    "invoiceID" bigint NOT NULL,
    "classesID" integer NOT NULL,
    classes character varying(128) NOT NULL,
    "studentID" integer NOT NULL,
    student character varying(128) NOT NULL,
    roll character varying(128) NOT NULL,
    feetype character varying(128) NOT NULL,
   amount numeric(10,2) NOT NULL,
  paidamount numeric(10,2) DEFAULT 0,
    "userID" integer,
    usertype character varying(20),
    uname character varying(60),
    status integer NOT NULL,
    paymenttype character varying(128),
    date date NOT NULL,
    paiddate date,
    year integer NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    "invoiceNO" character varying(10),
    "feetypeID" integer
);



COMMENT ON COLUMN    testing.invoice."invoiceNO" IS 'This will be a unique number that identify certain payment';


COMMENT ON COLUMN    testing.invoice."feetypeID" IS 'reference feetypeID that identify this invoice reflect which feetype. If this field is null, then invoice amount will be unique otherwise invoice amount should reflect amount from this feetype';


CREATE SEQUENCE   testing.invoice_invoiceid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE testing.invoice_invoiceid_seq OWNER TO super;

ALTER SEQUENCE  testing.invoice_invoiceid_seq OWNED BY invoice."invoiceID";


CREATE TABLE testing.issue (
    "issueID" bigint NOT NULL,
    "lID" character varying(128) NOT NULL,
    "bookID" integer NOT NULL,
    book character varying(60) NOT NULL,
    author character varying(100) NOT NULL,
    serial_no character varying(40) NOT NULL,
    issue_date date NOT NULL,
    due_date date NOT NULL,
    return_date date,
    fine character varying(11),
    note text,
    created_at timestamp without time zone DEFAULT now()
);




CREATE SEQUENCE   testing.issue_issueid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE testing.issue_issueid_seq OWNER TO super;

ALTER SEQUENCE  testing.issue_issueid_seq OWNED BY issue."issueID";


CREATE TABLE  testing.item (
    item_id integer NOT NULL,
    name character varying(200),
    batch_number character varying(90),
    quantity integer,
    vendor_id integer,
    contact_person_name character varying(200),
    contact_person_number character varying(90),
    status integer,
    price real,
    created_at timestamp without time zone DEFAULT now(),
    comments text,
    depreciation real,
    current_price real,
    date_purchased timestamp without time zone
);




COMMENT ON TABLE    testing.item IS 'This table store all items in a school';


COMMENT ON COLUMN    testing.item.status IS '1=new, 2=used, 3=obsolete';


COMMENT ON COLUMN    testing.item.comments IS 'any item description';

COMMENT ON COLUMN   testing.item.current_price IS 'price after depreciation';


CREATE SEQUENCE   testing.item_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE testing.item_item_id_seq OWNER TO super;

ALTER SEQUENCE  testing.item_item_id_seq OWNED BY item.item_id;



CREATE TABLE  testing.lmember (
    "lmemberID" bigint NOT NULL,
    "lID" character varying(40),
    "studentID" integer NOT NULL,
    name character varying(60) DEFAULT NULL::character varying,
    email character varying(40),
    phone text,
    lbalance character varying(20),
    ljoindate date DEFAULT now(),
    created_at timestamp without time zone DEFAULT now(),
    teacher_id integer
);




CREATE SEQUENCE   testing.lmember_lmemberid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




ALTER SEQUENCE  testing.lmember_lmemberid_seq OWNED BY lmember."lmemberID";


CREATE TABLE  testing.mailandsms (
    "mailandsmsID" bigint NOT NULL,
    users character varying(15) NOT NULL,
    type character varying(10) NOT NULL,
    message text NOT NULL,
    create_date timestamp without time zone DEFAULT now() NOT NULL,
    year integer NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);



CREATE SEQUENCE   testing.mailandsms_mailandsmsid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.mailandsms_mailandsmsid_seq OWNED BY mailandsms."mailandsmsID";



CREATE TABLE  testing.mailandsmstemplate (
    "mailandsmstemplateID" bigint NOT NULL,
    name character varying(128) NOT NULL,
    "user" character varying(15) NOT NULL,
    type character varying(10) NOT NULL,
    template text NOT NULL,
    create_date timestamp without time zone DEFAULT now() NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);



CREATE SEQUENCE   testing.mailandsmstemplate_mailandsmstemplateid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE  testing.mailandsmstemplate_mailandsmstemplateid_seq OWNED BY mailandsmstemplate."mailandsmstemplateID";


CREATE TABLE  testing.mailandsmstemplatetag (
    "mailandsmstemplatetagID" bigint NOT NULL,
    "usersID" integer NOT NULL,
    name character varying(15) NOT NULL,
    tagname character varying(128) NOT NULL,
    mailandsmstemplatetag_extra character varying(255),
    create_date timestamp without time zone DEFAULT now() NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);



CREATE SEQUENCE   testing.mailandsmstemplatetag_mailandsmstemplatetagid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE  testing.mailandsmstemplatetag_mailandsmstemplatetagid_seq OWNED BY mailandsmstemplatetag."mailandsmstemplatetagID";



CREATE VIEW   testing.total_mark AS
    SELECT sum(mark.mark) AS total, mark."studentID", mark."examID", mark.year, mark."classesID" FROM testing.mark GROUP BY mark."studentID", mark."examID", mark."classesID", mark.year;



CREATE VIEW   testing.mark_grade AS
    SELECT total_mark."studentID", total_mark."classesID", total_mark.year, total_mark."examID", total_mark.total, row_number() OVER (ORDER BY total_mark.total DESC) AS rank FROM testing.total_mark;


CREATE SEQUENCE   testing.mark_markid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.mark_markid_seq OWNED BY mark."markID";


CREATE VIEW   testing.mark_ranking AS
    SELECT mark."studentID", mark.mark, rank() OVER (PARTITION BY mark."subjectID" ORDER BY mark.mark DESC) AS dense_rank, mark."examID", mark."subjectID" FROM testing.mark;



CREATE TABLE  testing.media (
    "mediaID" bigint NOT NULL,
    "userID" integer NOT NULL,
    usertype character varying(20) NOT NULL,
    "mcategoryID" integer DEFAULT 0 NOT NULL,
    file_name character varying(255) NOT NULL,
    file_name_display character varying(255) NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);


CREATE TABLE  testing.media_category (
    "mcategoryID" bigint NOT NULL,
    "userID" integer NOT NULL,
    usertype character varying(20) NOT NULL,
    folder_name character varying(255) NOT NULL,
    create_time timestamp without time zone DEFAULT now() NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);


CREATE SEQUENCE   testing.media_category_mcategoryid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE  testing.media_category_mcategoryid_seq OWNED BY media_category."mcategoryID";



CREATE SEQUENCE   testing.media_mediaid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.media_mediaid_seq OWNED BY media."mediaID";



CREATE TABLE  testing.media_share (
    "shareID" bigint NOT NULL,
    "classesID" integer NOT NULL,
    public integer NOT NULL,
    file_or_folder integer NOT NULL,
    item_id integer NOT NULL,
    create_time timestamp without time zone DEFAULT now() NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);


CREATE SEQUENCE   testing.media_share_shareid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE testing.media_share_shareid_seq OWNER TO super;

ALTER SEQUENCE  testing.media_share_shareid_seq OWNED BY media_share."shareID";

CREATE TABLE  testing.message (
    "messageID" bigint NOT NULL,
    email character varying(128) NOT NULL,
    "receiverID" integer NOT NULL,
    "receiverType" character varying(20) NOT NULL,
    subject character varying(255) NOT NULL,
    message text NOT NULL,
    attach text,
    attach_file_name text,
    "userID" integer NOT NULL,
    usertype character varying(20) NOT NULL,
    useremail character varying(40) NOT NULL,
    year integer NOT NULL,
    date date NOT NULL,
    create_date timestamp without time zone DEFAULT now() NOT NULL,
    read_status smallint NOT NULL,
    from_status integer NOT NULL,
    to_status integer NOT NULL,
    fav_status smallint NOT NULL,
    fav_status_sent smallint NOT NULL,
    reply_status integer NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);


CREATE SEQUENCE   testing.message_messageid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE  testing.message_messageid_seq OWNED BY message."messageID";



CREATE TABLE  testing.migrations (
    created_at timestamp without time zone DEFAULT now(),
    batch bigint,
    migration text,
    version integer NOT NULL
);



CREATE SEQUENCE   testing.migrations_version_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE  testing.migrations_version_seq OWNED BY migrations.version;



CREATE TABLE  testing.notice (
    "noticeID" bigint NOT NULL,
    title character varying(128) NOT NULL,
    notice text NOT NULL,
    year integer NOT NULL,
    date date NOT NULL,
    create_date timestamp without time zone DEFAULT now() NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);



CREATE SEQUENCE   testing.notice_noticeid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.notice_noticeid_seq OWNED BY notice."noticeID";


CREATE TABLE  testing.parent (
    "parentID" bigint NOT NULL,
    name character varying(60) NOT NULL,
    father_name character varying(60) NOT NULL,
    mother_name character varying(60) NOT NULL,
    father_profession character varying(40) NOT NULL,
    mother_profession character varying(40) NOT NULL,
    employer character varying(500),
    email character varying(40),
    phone text,
    address text,
    photo character varying(200) NOT NULL,
    username character varying(40) NOT NULL,
    password character varying(128) NOT NULL,
    usertype character varying(20) NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    other_phone character varying(25),
    status integer
);



COMMENT ON COLUMN      testing.parent.status IS '0=not active, 1=active';


CREATE SEQUENCE   testing.parent_parentid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE testing.parent_parentid_seq OWNER TO super;


ALTER SEQUENCE  testing.parent_parentid_seq OWNED BY parent."parentID";



CREATE TABLE  testing.payment (
    "paymentID" bigint NOT NULL,
    "invoiceID" integer NOT NULL,
    "studentID" integer NOT NULL,
    paymentamount character varying(20) NOT NULL,
    paymenttype character varying(128) NOT NULL,
    paymentdate date NOT NULL,
    paymentmonth character varying(10) NOT NULL,
    paymentyear integer NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    "transactionID" character varying(120),
    "userID" bigint,
    slipfile character varying(250),
    approved integer DEFAULT 0,
    approved_date timestamp without time zone,
    approved_user_id bigint
);



COMMENT ON COLUMN    testing.payment."transactionID" IS 'from bank, paygate, cheque no, etc';



COMMENT ON COLUMN    testing.payment."userID" IS 'someone who add payment';



COMMENT ON COLUMN    testing.payment.slipfile IS 'file name for this paying slip';



COMMENT ON COLUMN    testing.payment.approved IS '0= pending, 1=accepted, 2=rejected';



COMMENT ON COLUMN    testing.payment.approved_user_id IS 'which user ID approve this transaction';

CREATE VIEW   testing.payment_history AS
    SELECT a."invoiceNO", b."paymentID", a."invoiceID", b."transactionID", b.paymentdate, c.name AS student_name, d.name AS parent_name, b.paymentamount, b.paymenttype, b.slipfile, b.approved, e.feetype FROM ((((testing.invoice a JOIN testing.payment b ON ((b."invoiceID" = a."invoiceID"))) JOIN testing.student c ON ((c."studentID" = b."studentID"))) JOIN testing.parent d ON ((d."parentID" = c."parentID"))) JOIN testing.feetype e ON ((e."feetypeID" = a."feetypeID")));


CREATE SEQUENCE   testing.payment_paymentid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.payment_paymentid_seq OWNED BY payment."paymentID";



CREATE TABLE  testing.promotionsubject (
    "promotionSubjectID" bigint NOT NULL,
    "classesID" integer NOT NULL,
    "subjectID" integer NOT NULL,
    "subjectCode" text NOT NULL,
    "subjectMark" integer,
    created_at timestamp without time zone DEFAULT now()
);

CREATE SEQUENCE   testing.promotionsubject_promotionsubjectid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE  testing.promotionsubject_promotionsubjectid_seq OWNED BY promotionsubject."promotionSubjectID";


CREATE TABLE  testing.receipt (
    "receiptID" integer NOT NULL,
    "paymentID" bigint,
    code character varying(50),
    created_at timestamp without time zone DEFAULT now()
);

COMMENT ON TABLE    testing.receipt IS 'store receipt information on each payment done';



COMMENT ON COLUMN    testing.receipt.code IS 'Receipt code, unique, identify payment and returned in mobile sms';


CREATE SEQUENCE   testing."receipt_receiptID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE  testing."receipt_receiptID_seq" OWNED BY receipt."receiptID";


CREATE TABLE  testing.reply_msg (
    "replyID" bigint NOT NULL,
    "messageID" integer NOT NULL,
    reply_msg text NOT NULL,
    status integer NOT NULL,
    create_time timestamp without time zone DEFAULT now() NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);


CREATE SEQUENCE   testing.reply_msg_replyid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE  testing.reply_msg_replyid_seq OWNED BY reply_msg."replyID";


CREATE TABLE  testing.reset (
    "resetID" bigint NOT NULL,
    "keyID" character varying(128) NOT NULL,
    email character varying(60) NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);



CREATE SEQUENCE   testing.reset_resetid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.reset_resetid_seq OWNED BY reset."resetID";


CREATE TABLE  testing.routine (
    "routineID" bigint NOT NULL,
    "classesID" integer NOT NULL,
    "sectionID" integer NOT NULL,
    "subjectID" integer NOT NULL,
    day character varying(60) NOT NULL,
    start_time character varying(10) NOT NULL,
    end_time character varying(10) NOT NULL,
    room text NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);


ALTER TABLE testing.routine OWNER TO super;


CREATE SEQUENCE   testing.routine_routineid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE  testing.routine_routineid_seq OWNED BY routine."routineID";


CREATE TABLE  testing.school_sessions (
    session_id character varying(40) DEFAULT '0'::character varying NOT NULL,
    ip_address character varying(45) DEFAULT '0'::character varying NOT NULL,
    user_agent character varying(120) NOT NULL,
    last_activity bigint DEFAULT 0 NOT NULL,
    user_data text NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);



CREATE TABLE  testing.section (
    "sectionID" bigint NOT NULL,
    section character varying(60) NOT NULL,
    category character varying(128) NOT NULL,
    "classesID" integer NOT NULL,
    "teacherID" integer NOT NULL,
    note text,
    extra character varying(60),
    created_at timestamp without time zone DEFAULT now()
);


ALTER TABLE testing.section OWNER TO super;

CREATE SEQUENCE   testing.section_sectionid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE  testing.section_sectionid_seq OWNED BY section."sectionID";



CREATE TABLE  testing.setting (
    "settingID" bigint NOT NULL,
    sname text,
    name character varying(60),
    phone text,
    address text,
    email character varying(40),
    automation integer,
    currency_code character varying(11),
    currency_symbol text,
    footer text,
    photo character varying(128),
    username character varying(128),
    password character varying(128),
    usertype character varying(128),
    purchase_code character varying(255),
    created_at timestamp without time zone DEFAULT now(),
    api_key character varying(90),
    api_secret character varying(120),
    box character varying(90),
    payment_integrated integer DEFAULT 0,
    pass_mark integer,
    website character varying(250),
    motto character varying,
    academic_year_id integer
);



COMMENT ON COLUMN    testing.setting.api_key IS 'from karibusms';


COMMENT ON COLUMN    testing.setting.api_secret IS 'for karibusms';


COMMENT ON COLUMN    testing.setting.box IS 'school p.o box';



COMMENT ON COLUMN    testing.setting.payment_integrated IS 'check if payment is integrated or not';



COMMENT ON COLUMN    testing.setting.pass_mark IS 'specify the marking level which a school adopt such that, below this mark, a student is considered as failed';



CREATE SEQUENCE   testing.setting_settingid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.setting_settingid_seq OWNED BY setting."settingID";


CREATE TABLE  testing.sms (
    sms_id integer NOT NULL,
    body text,
    users_id integer DEFAULT 0,
    created_at timestamp without time zone DEFAULT now(),
    status integer DEFAULT 0,
    return_code character varying(90),
    phone_number character varying(20)
);


COMMENT ON COLUMN    testing.sms.status IS '0=pending, 1=sent, 2=failed';


CREATE SEQUENCE   testing.sms_sms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.sms_sms_id_seq OWNED BY sms.sms_id;


CREATE TABLE  testing.smssettings (
    "smssettingsID" bigint NOT NULL,
    types character varying(255),
    field_names character varying(255),
    field_values character varying(255),
    smssettings_extra character varying(255),
    created_at timestamp without time zone DEFAULT now()
);



CREATE SEQUENCE   testing.smssettings_smssettingsid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.smssettings_smssettingsid_seq OWNED BY smssettings."smssettingsID";


CREATE TABLE  testing.special_promotion (
    id integer NOT NULL,
    student_id bigint,
    from_academic_year_id bigint,
    to_academic_year_id bigint,
    pass_mark real,
    remark character varying,
    created_at timestamp without time zone DEFAULT now(),
    updated_at timestamp without time zone,
    status integer
);


COMMENT ON COLUMN    testing.special_promotion.status IS '1=promoted
2=not promoted
3=de promoted';


CREATE SEQUENCE   testing.special_promotion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE  testing.special_promotion_id_seq OWNED BY special_promotion.id;

CREATE TABLE  testing.student_archive (
    id integer NOT NULL,
    student_id bigint NOT NULL,
    academic_year_id smallint NOT NULL,
    section_id smallint NOT NULL,
    created_at timestamp(0) without time zone DEFAULT ('now'::text)::timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone
);


CREATE SEQUENCE   testing.student_archive_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE  testing.student_archive_id_seq OWNED BY student_archive.id;



CREATE SEQUENCE   testing.student_studentid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE  testing.student_studentid_seq OWNED BY student."studentID";



CREATE VIEW   testing.subject_count AS
    SELECT m.subject_id, m.student_id, s.subject, 'Option'::character varying AS subject_type, s.is_counted, s.is_penalty, s.pass_mark, s.grade_mark FROM (testing.subject_student m JOIN testing.subject s ON ((s."subjectID" = m.subject_id))) UNION SELECT a.subject_id, b."studentID" AS student_id, s.subject, 'Core'::character varying AS subject_type, s.is_counted, s.is_penalty, s.pass_mark, s.grade_mark FROM ((testing.subject_section a JOIN testing.student b ON ((b."sectionID" = a.section_id))) JOIN testing.subject s ON ((s."subjectID" = a.subject_id)));



CREATE SEQUENCE   testing.subject_section_subject_subject_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.subject_section_subject_subject_id_seq OWNED BY subject_section.subject_section_id;



CREATE SEQUENCE   testing.subject_student_subject_student_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.subject_student_subject_student_id_seq OWNED BY subject_student.subject_student_id;


CREATE SEQUENCE   testing.subject_subjectid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE  testing.subject_subjectid_seq OWNED BY subject."subjectID";


CREATE VIEW   testing.sum_rank AS
    SELECT a."examID", a."classesID", a."studentID", a."subjectID", c.is_counted, c.is_penalty, sum(a.mark) AS sum, rank() OVER (ORDER BY sum(a.mark) DESC) AS rank FROM (testing.mark a JOIN testing.subject c ON ((c."subjectID" = a."subjectID"))) WHERE (a.mark IS NOT NULL) GROUP BY a."examID", a."classesID", a."studentID", a."subjectID", c.is_counted, c.is_penalty;



CREATE TABLE  testing.tattendance (
    "tattendanceID" bigint NOT NULL,
    "teacherID" integer NOT NULL,
    usertype character varying(20) NOT NULL,
    monthyear character varying(10) NOT NULL,
    a1 character varying(3),
    a2 character varying(3),
    a3 character varying(3),
    a4 character varying(3),
    a5 character varying(3),
    a6 character varying(3),
    a7 character varying(3),
    a8 character varying(3),
    a9 character varying(3),
    a10 character varying(3),
    a11 character varying(3),
    a12 character varying(3),
    a13 character varying(3),
    a14 character varying(3),
    a15 character varying(3),
    a16 character varying(3),
    a17 character varying(3),
    a18 character varying(3),
    a19 character varying(3),
    a20 character varying(3),
    a21 character varying(3),
    a22 character varying(3),
    a23 character varying(3),
    a24 character varying(3),
    a25 character varying(3),
    a26 character varying(3),
    a27 character varying(3),
    a28 character varying(3),
    a29 character varying(3),
    a30 character varying(3),
    a31 character varying(3),
    created_at timestamp without time zone DEFAULT now()
);



CREATE SEQUENCE   testing.tattendance_tattendanceid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.tattendance_tattendanceid_seq OWNED BY tattendance."tattendanceID";


CREATE TABLE  testing.teacher (
    "teacherID" bigint NOT NULL,
    name character varying(60) NOT NULL,
    designation character varying(128) NOT NULL,
    dob date NOT NULL,
    sex character varying(10) NOT NULL,
    religion character varying(25),
    email character varying(40),
    phone text,
    address text,
    jod date NOT NULL,
    photo character varying(200),
    username character varying(40) NOT NULL,
    password character varying(128) NOT NULL,
    usertype character varying(20) NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    employment_type character varying(50),
    signature text,
    id_number character varying
);



COMMENT ON COLUMN    testing.teacher.signature IS 'store blob';



CREATE SEQUENCE   testing.teacher_teacherid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.teacher_teacherid_seq OWNED BY teacher."teacherID";


CREATE TABLE  testing.tmember (
    "tmemberID" bigint NOT NULL,
    "studentID" integer NOT NULL,
    "transportID" integer NOT NULL,
    name character varying(60) NOT NULL,
    email character varying(40),
    phone text,
    tbalance character varying(11),
    tjoindate date NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);



CREATE SEQUENCE   testing.tmember_tmemberid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE  testing.tmember_tmemberid_seq OWNED BY tmember."tmemberID";


CREATE TABLE  testing.transport (
    "transportID" bigint NOT NULL,
    route text NOT NULL,
    vehicle integer NOT NULL,
    fare character varying(11) NOT NULL,
    note text,
    created_at timestamp without time zone DEFAULT now()
);


CREATE SEQUENCE   testing.transport_transportid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE  testing.transport_transportid_seq OWNED BY transport."transportID";



CREATE TABLE  testing."user" (
    "userID" bigint NOT NULL,
    name character varying(60) NOT NULL,
    dob date NOT NULL,
    sex character varying(10) NOT NULL,
    religion character varying(25),
    email character varying(40),
    phone text,
    address text,
    jod date NOT NULL,
    photo character varying(200),
    username character varying(40) NOT NULL,
    password character varying(128) NOT NULL,
    usertype character varying(20) NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    signature text
);



COMMENT ON COLUMN    testing."user".signature IS 'store blob';



CREATE SEQUENCE   testing.user_userid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE  testing.user_userid_seq OWNED BY "user"."userID";


CREATE VIEW   testing.users AS
    (((SELECT a."userID" AS id, a.email, a.phone, a.username, a.usertype FROM testing."user" a UNION ALL SELECT t."teacherID" AS id, t.email, t.phone, t.username, 'teacher'::character varying AS usertype FROM testing.teacher t) UNION ALL SELECT s."studentID" AS id, s.email, s.phone, s.username, 'student'::character varying AS usertype FROM testing.student s) UNION ALL SELECT p."parentID" AS id, p.email, p.phone, p.username, 'parent'::character varying AS usertype FROM testing.parent p) UNION ALL SELECT b."settingID" AS id, b.email, b.phone, b.username, 'Admin'::character varying AS usertype FROM testing.setting b;



CREATE TABLE  testing.vendor (
    vendor_id integer NOT NULL,
    email character varying(150),
    name character varying(250),
    phone_number character varying(90),
    telephone_number character varying(90),
    country character varying(150),
    city character varying(150),
    location text,
    bank_name character varying(90),
    bank_branch character varying(150),
    account_number character varying(90),
    contact_person_name character varying(150),
    contact_person_phone character varying(90),
    contact_person_email character varying(90),
    contact_person_jobtitle character varying(90),
    service_product text,
    created_at timestamp without time zone DEFAULT now(),
    updated_at timestamp without time zone
);


CREATE TABLE testing.semister
(
  id serial NOT NULL,
  name character varying(100) NOT NULL,
  class_level_id smallint NOT NULL,
  created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone,
  updated_at timestamp(0) without time zone,
  academic_year_id integer, -- Lets know this semester is for which year
  start_date date,
  end_date date,
  CONSTRAINT semister_pkey PRIMARY KEY (id ),
  CONSTRAINT semister_class_level_id_fkey FOREIGN KEY (class_level_id)
      REFERENCES new.class_level (id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
);

ALTER TABLE testing.semister
  OWNER TO postgres;
COMMENT ON COLUMN testing.semister.academic_year_id IS 'Lets know this semester is for which year';

CREATE TABLE testing.role (
    id serial,
    name character varying NOT NULL,
    sys_role character(1) DEFAULT '0'::bpchar NOT NULL,
    is_super character(1) DEFAULT '0'::bpchar NOT NULL,
    created_at timestamp(0) without time zone DEFAULT ('now'::text)::timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone
);

CREATE TABLE testing.permission_group (
    id serial,
    name character varying NOT NULL,
    created_at timestamp(0) without time zone DEFAULT ('now'::text)::timestamp(0) without time zone NOT NULL
);

CREATE TABLE testing.permission (
    id serial,
    name character varying(80) NOT NULL,
    display_name character varying NOT NULL,
    description text NOT NULL,
    is_super character(1) DEFAULT '0'::bpchar NOT NULL,
    permission_group_id smallint NOT NULL,
    created_at timestamp(0) without time zone DEFAULT ('now'::text)::timestamp(0) without time zone NOT NULL
);


CREATE TABLE testing.role_permission (
    id serial,
    role_id smallint NOT NULL,
    permission_id smallint NOT NULL,
    created_at timestamp(0) without time zone DEFAULT ('now'::text)::timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone
);

CREATE TABLE testing.user_role (
    id serial,
    userID bigint NOT NULL,
    role_id smallint NOT NULL,
    created_at timestamp(0) without time zone DEFAULT ('now'::text)::timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone
);



--
-- Data for Name: testing.permission_group; Type: TABLE DATA; Schema: demo2; Owner: postgres
--

INSERT INTO testing.permission_group VALUES (1, 'Students', '2017-01-11 11:26:07');
INSERT INTO testing.permission_group VALUES (2, 'Teachers', '2017-01-11 11:26:31');
INSERT INTO testing.permission_group VALUES (3, 'Parents', '2017-01-11 11:27:03');
INSERT INTO testing.permission_group VALUES (4, 'Users', '2017-01-11 11:27:23');
INSERT INTO testing.permission_group VALUES (5, 'Class', '2017-01-11 11:27:30');
INSERT INTO testing.permission_group VALUES (6, 'Section', '2017-01-11 11:27:37');
INSERT INTO testing.permission_group VALUES (7, 'Library', '2017-01-11 11:27:52');
INSERT INTO testing.permission_group VALUES (8, 'Transport', '2017-01-11 11:28:01');
INSERT INTO testing.permission_group VALUES (9, 'Subject', '2017-01-11 11:28:21');
INSERT INTO testing.permission_group VALUES (10, 'Grade', '2017-01-11 11:28:29');
INSERT INTO testing.permission_group VALUES (11, 'Class level', '2017-01-11 11:28:38');
INSERT INTO testing.permission_group VALUES (12, 'Exam', '2017-01-11 11:28:47');
INSERT INTO testing.permission_group VALUES (13, 'Mark', '2017-01-11 11:29:06');
INSERT INTO testing.permission_group VALUES (14, 'Routine', '2017-01-11 11:29:14');
INSERT INTO testing.permission_group VALUES (15, 'Attendance', '2017-01-11 11:29:33');

SELECT pg_catalog.setval('testing.permission_group_id_seq', 15, true);

INSERT INTO testing.permission VALUES (2, 'add_student', 'Add student', '', '0', 1, '2017-01-11 11:50:16');
INSERT INTO testing.permission VALUES (3, 'edit_student', 'Edit student', '', '0', 1, '2017-01-11 11:50:33');
INSERT INTO testing.permission VALUES (4, 'delete_student', 'Delete student', '', '0', 1, '2017-01-11 11:54:07');
INSERT INTO testing.permission VALUES (6, 'print_student_info', 'Print student information', '', '0', 1, '2017-01-11 12:05:02');
INSERT INTO testing.permission VALUES (5, 'view_student_info', 'View student information', '', '0', 1, '2017-01-11 11:57:36');
INSERT INTO testing.permission VALUES (10, 'view_teachers', 'View teachers', '', '0', 2, '2017-01-11 12:17:01');
INSERT INTO testing.permission VALUES (11, 'add_teacher', 'Add teacher', '', '0', 2, '2017-01-11 12:17:22');
INSERT INTO testing.permission VALUES (12, 'edit_teacher', 'Edit teacher', '', '0', 2, '2017-01-11 12:17:44');
INSERT INTO testing.permission VALUES (13, 'delete_teacher', 'Delete teacher', '', '0', 2, '2017-01-11 12:19:50');
INSERT INTO testing.permission VALUES (14, 'view_teacher_info', 'View more one teacher information', '', '0', 2, '2017-01-11 12:49:23');
INSERT INTO testing.permission VALUES (16, 'print_teacher_id', 'Print teacher ID card', '', '0', 2, '2017-01-11 13:09:39');
INSERT INTO testing.permission VALUES (1, 'view_students', 'View Students', '', '0', 1, '2017-01-11 11:33:08');
INSERT INTO testing.permission VALUES (8, 'print_student_id', 'Print student ID card', '', '0', 1, '2017-01-11 12:10:44');
INSERT INTO testing.permission VALUES (7, 'student_pdf_preview', 'Student PDF Preview', '', '0', 1, '2017-01-11 12:09:40');
INSERT INTO testing.permission VALUES (82, 'view_teacher_attendance', 'View teachers attendance', '', '0', 15, '2017-01-11 16:59:24');
INSERT INTO testing.permission VALUES (15, 'teacher_pdf_preview', 'Teacher PDF Preview', '', '0', 2, '2017-01-11 13:03:34');
INSERT INTO testing.permission VALUES (18, 'view_parents', 'View list of parents', '', '0', 3, '2017-01-11 13:38:34');
INSERT INTO testing.permission VALUES (19, 'add_parent', 'Add parent', '', '0', 3, '2017-01-11 13:39:02');
INSERT INTO testing.permission VALUES (20, 'edit_parent', 'Edit parent', '', '0', 3, '2017-01-11 14:01:34');
INSERT INTO testing.permission VALUES (21, 'delete_parent', 'Delete parent', '', '0', 3, '2017-01-11 14:02:10');
INSERT INTO testing.permission VALUES (22, 'view_parent_info', 'View parent information', '', '0', 3, '2017-01-11 14:03:28');
INSERT INTO testing.permission VALUES (23, 'print_parent_info', 'Print parent information', '', '0', 3, '2017-01-11 14:04:10');
INSERT INTO testing.permission VALUES (24, 'parent_pdf_preview', 'Parent PDF preview', '', '0', 3, '2017-01-11 14:05:03');
INSERT INTO testing.permission VALUES (25, 'parent_pdf_email', 'Send Parent PDF to email', '', '0', 3, '2017-01-11 14:06:12');
INSERT INTO testing.permission VALUES (17, 'teacher_pdf_email', 'Send teacher PDF to email', '', '0', 2, '2017-01-11 13:23:53');
INSERT INTO testing.permission VALUES (9, 'student_pdf_email', 'Send Student PDF to email', '', '0', 1, '2017-01-11 12:15:39');
INSERT INTO testing.permission VALUES (26, 'view_users', 'View users ', '', '0', 4, '2017-01-11 14:34:07');
INSERT INTO testing.permission VALUES (27, 'add_user', 'Add new user', '', '0', 4, '2017-01-11 14:49:28');
INSERT INTO testing.permission VALUES (28, 'edit_user', 'Edit user', '', '0', 4, '2017-01-11 14:58:43');
INSERT INTO testing.permission VALUES (29, 'delete_user', 'Delete user', '', '0', 4, '2017-01-11 15:02:07');
INSERT INTO testing.permission VALUES (30, 'view_user_info', 'View user information', '', '0', 4, '2017-01-11 15:02:49');
INSERT INTO testing.permission VALUES (31, 'print_user_info', 'Print user information', '', '0', 4, '2017-01-11 15:06:13');
INSERT INTO testing.permission VALUES (32, 'print_user_id', 'Print user ID card', '', '0', 4, '2017-01-11 15:10:00');
INSERT INTO testing.permission VALUES (33, 'user_pdf_email', 'Send User PDF to email', '', '0', 4, '2017-01-11 15:12:00');
INSERT INTO testing.permission VALUES (34, 'download_user_info', 'Download User PDF information', '', '0', 4, '2017-01-11 15:14:28');
INSERT INTO testing.permission VALUES (35, 'add_class', 'Add class', '', '0', 5, '2017-01-11 15:15:44');
INSERT INTO testing.permission VALUES (36, 'edit_class', 'Edit class', '', '0', 5, '2017-01-11 15:16:02');
INSERT INTO testing.permission VALUES (37, 'delete_class', 'Delete class', '', '0', 5, '2017-01-11 15:16:21');
INSERT INTO testing.permission VALUES (38, 'add_section', 'Add section', '', '0', 6, '2017-01-11 15:29:19');
INSERT INTO testing.permission VALUES (39, 'print_exam_attendance', 'Print exam attendance', '', '0', 6, '2017-01-11 15:35:03');
INSERT INTO testing.permission VALUES (40, 'add_exam_attendance', 'Add exam attendance', '', '0', 6, '2017-01-11 15:38:03');
INSERT INTO testing.permission VALUES (41, 'view_library_members', 'View library members', '', '0', 7, '2017-01-11 15:40:26');
INSERT INTO testing.permission VALUES (42, 'add_library_member', 'Add library member', '', '0', 7, '2017-01-11 15:41:22');
INSERT INTO testing.permission VALUES (43, 'view_books', 'View books', '', '0', 7, '2017-01-11 15:47:27');
INSERT INTO testing.permission VALUES (44, 'add_book', 'Add book', '', '0', 7, '2017-01-11 15:48:11');
INSERT INTO testing.permission VALUES (45, 'edit_book', 'Edit book', '', '0', 7, '2017-01-11 15:49:34');
INSERT INTO testing.permission VALUES (46, 'delete_book', 'Delete book ', '', '0', 7, '2017-01-11 15:51:12');
INSERT INTO testing.permission VALUES (47, 'search_library_member', 'Search library member', '', '0', 7, '2017-01-11 15:52:34');
INSERT INTO testing.permission VALUES (48, 'view_issue_user', 'View issue user', '', '0', 7, '2017-01-11 15:54:30');
INSERT INTO testing.permission VALUES (49, 'return_book', 'Return a book', '', '0', 7, '2017-01-11 15:55:00');
INSERT INTO testing.permission VALUES (50, 'add_transport_routes', 'Add transport routes', '', '0', 8, '2017-01-11 15:57:06');
INSERT INTO testing.permission VALUES (51, 'add_transport_fee', 'Add transport fee', '', '0', 8, '2017-01-11 15:57:47');
INSERT INTO testing.permission VALUES (52, 'delete_transport', 'Delete transport', '', '0', 8, '2017-01-11 15:58:53');
INSERT INTO testing.permission VALUES (53, 'edit_transport', 'Edit transport', '', '0', 8, '2017-01-11 16:00:25');
INSERT INTO testing.permission VALUES (54, 'add_transport_member', 'Add transport member', '', '0', 8, '2017-01-11 16:01:42');
INSERT INTO testing.permission VALUES (55, 'delete_transport_member', 'Delete transport member', '', '0', 8, '2017-01-11 16:02:11');
INSERT INTO testing.permission VALUES (56, 'edit_transport_member', 'Edit transport member', '', '0', 8, '2017-01-11 16:02:51');
INSERT INTO testing.permission VALUES (57, 'edit_section', 'Edit section', '', '0', 6, '2017-01-11 16:04:24');
INSERT INTO testing.permission VALUES (58, 'delete_section', 'Delete section', '', '0', 6, '2017-01-11 16:04:48');
INSERT INTO testing.permission VALUES (59, 'add_subject', 'Add Subject', '', '0', 9, '2017-01-11 16:15:16');
INSERT INTO testing.permission VALUES (60, 'edit_subject', 'Edit Subject', '', '0', 9, '2017-01-11 16:16:21');
INSERT INTO testing.permission VALUES (61, 'delete_subject', 'Delete Subject', '', '0', 9, '2017-01-11 16:17:05');
INSERT INTO testing.permission VALUES (62, 'add_grade', 'Add Grade', '', '0', 10, '2017-01-11 16:18:17');
INSERT INTO testing.permission VALUES (63, 'edit_grade', 'Edit Grade', '', '0', 10, '2017-01-11 16:18:44');
INSERT INTO testing.permission VALUES (64, 'delete_grade', 'Delete Grade', '', '0', 10, '2017-01-11 16:19:19');
INSERT INTO testing.permission VALUES (65, 'add_class_level', 'Add Class level', '', '0', 11, '2017-01-11 16:23:16');
INSERT INTO testing.permission VALUES (66, 'edit_class_level', 'Edit Class Level', '', '0', 11, '2017-01-11 16:23:53');
INSERT INTO testing.permission VALUES (67, 'delete_class_level', 'Delete Class Level', '', '0', 11, '2017-01-11 16:26:26');
INSERT INTO testing.permission VALUES (68, 'add_exam', 'Add Exam', '', '0', 12, '2017-01-11 16:29:51');
INSERT INTO testing.permission VALUES (69, 'edit_exam', 'Edit Exam', '', '0', 12, '2017-01-11 16:30:27');
INSERT INTO testing.permission VALUES (70, 'delete_exam', 'Delete Exam', '', '0', 12, '2017-01-11 16:30:49');
INSERT INTO testing.permission VALUES (71, 'add_exam_schedule', 'Add Exam Schedule', '', '0', 12, '2017-01-11 16:31:29');
INSERT INTO testing.permission VALUES (72, 'edit_exam_schedule', 'Edit Exam Schedule', '', '0', 12, '2017-01-11 16:32:10');
INSERT INTO testing.permission VALUES (73, 'delete_exam_schedule', 'Delete Exam Schedule', '', '0', 12, '2017-01-11 16:35:15');
INSERT INTO testing.permission VALUES (74, 'view_student_all_report', 'View Student All Reports', '', '0', 12, '2017-01-11 16:38:41');
INSERT INTO testing.permission VALUES (75, 'view_student_single_report', 'View Student Single Report', '', '0', 12, '2017-01-11 16:43:19');
INSERT INTO testing.permission VALUES (76, 'create_official_report', 'Create Official Report', '', '0', 12, '2017-01-11 16:44:17');
INSERT INTO testing.permission VALUES (77, 'view_official_reports', 'View official Reports', '', '0', 12, '2017-01-11 16:44:59');
INSERT INTO testing.permission VALUES (78, 'add_mark', 'Add Mark', '', '0', 13, '2017-01-11 16:45:38');
INSERT INTO testing.permission VALUES (79, 'view_mark', 'View Mark', '', '0', 13, '2017-01-11 16:46:06');
INSERT INTO testing.permission VALUES (80, 'add_routine', 'Add routine', '', '0', 14, '2017-01-11 16:53:54');
INSERT INTO testing.permission VALUES (81, 'view_routine', 'View routine', '', '0', 14, '2017-01-11 16:54:33');
INSERT INTO testing.permission VALUES (83, 'print_teacher_attendance', 'Print teacher attendance', '', '0', 15, '2017-01-11 17:00:23');
INSERT INTO testing.permission VALUES (84, 'add_teacher_attendance', 'Add teachers attendance', '', '0', 15, '2017-01-11 17:01:05');
INSERT INTO testing.permission VALUES (85, 'view_student_attendance', 'View students attendance', '', '0', 15, '2017-01-11 17:01:59');
INSERT INTO testing.permission VALUES (86, 'add_student_attendance', 'Add students attendance', '', '0', 15, '2017-01-11 17:03:01');
INSERT INTO testing.permission VALUES (87, 'view_exam_attendance', 'View exam attendance', '', '0', 15, '2017-01-11 17:03:36');

SELECT pg_catalog.setval('testing.permission_id_seq', 87, true);

INSERT INTO testing.role VALUES (1, 'Admin', '0', '0', '2017-01-11 18:31:24', NULL);
INSERT INTO testing.role VALUES (2, 'Teacher', '0', '0', '2017-01-11 18:31:59', NULL);
INSERT INTO testing.role VALUES (3, 'Accountant', '0', '0', '2017-01-11 18:32:44', NULL);
INSERT INTO testing.role VALUES (4, 'Librarian', '0', '0', '2017-01-11 18:32:54', NULL);
INSERT INTO testing.role VALUES (5, 'Secretary', '0', '0', '2017-01-11 18:33:24', NULL);

SELECT pg_catalog.setval('testing.role_id_seq', 5, true);

COMMENT ON TABLE    testing.vendor IS 'store all vendor information for those who supply materials to a certain school';



COMMENT ON COLUMN    testing.vendor.name IS 'company name or business name';


COMMENT ON COLUMN    testing.vendor.phone_number IS 'vendor mobile number';


COMMENT ON COLUMN    testing.vendor.service_product IS 'List of materials this supplier supply';



CREATE SEQUENCE   testing.vendor_vendor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE  testing.vendor_vendor_id_seq OWNED BY vendor.vendor_id;


ALTER TABLE ONLY    testing.academic_year ALTER COLUMN id SET DEFAULT nextval('testing.accademic_year_id_seq'::regclass);

ALTER TABLE ONLY    testing.alert ALTER COLUMN "alertID" SET DEFAULT nextval('testing.alert_alertid_seq'::regclass);



ALTER TABLE ONLY    testing.attendance ALTER COLUMN "attendanceID" SET DEFAULT nextval('testing.attendance_attendanceid_seq'::regclass);


ALTER TABLE ONLY    testing.automation_rec ALTER COLUMN "automation_recID" SET DEFAULT nextval('testing.automation_rec_automation_recid_seq'::regclass);


ALTER TABLE ONLY    testing.automation_shudulu ALTER COLUMN "automation_shuduluID" SET DEFAULT nextval('testing.automation_shudulu_automation_shuduluid_seq'::regclass);



ALTER TABLE ONLY    testing.bank_account ALTER COLUMN bank_account_id SET DEFAULT nextval('testing.bank_account_bank_account_id_seq'::regclass);


ALTER TABLE ONLY    testing.book ALTER COLUMN "bookID" SET DEFAULT nextval('testing.book_bookid_seq'::regclass);


ALTER TABLE ONLY    testing.book_class ALTER COLUMN book_class_id SET DEFAULT nextval('testing.book_class_book_class_id_seq'::regclass);


ALTER TABLE ONLY    testing.category ALTER COLUMN "categoryID" SET DEFAULT nextval('testing.category_categoryid_seq'::regclass);


ALTER TABLE ONLY    testing.class_exam ALTER COLUMN class_exam_id SET DEFAULT nextval('testing.class_exam_class_exam_id_seq'::regclass);

ALTER TABLE ONLY    testing.classes ALTER COLUMN "classesID" SET DEFAULT nextval('testing.classes_classesid_seq'::regclass);



ALTER TABLE ONLY    testing.classlevel ALTER COLUMN classlevel_id SET DEFAULT nextval('testing.class_level_class_level_id_seq'::regclass);


ALTER TABLE ONLY    testing.eattendance ALTER COLUMN "eattendanceID" SET DEFAULT nextval('testing.eattendance_eattendanceid_seq'::regclass);



ALTER TABLE ONLY    testing.email ALTER COLUMN email_id SET DEFAULT nextval('testing.email_email_id_seq'::regclass);


ALTER TABLE ONLY    testing.exam ALTER COLUMN "examID" SET DEFAULT nextval('testing.exam_examid_seq'::regclass);


ALTER TABLE ONLY    testing.exam_report ALTER COLUMN exam_report_id SET DEFAULT nextval('testing.exam_report_exam_report_id_seq'::regclass);


ALTER TABLE ONLY    testing.examschedule ALTER COLUMN "examscheduleID" SET DEFAULT nextval('testing.examschedule_examscheduleid_seq'::regclass);


ALTER TABLE ONLY    testing.expense ALTER COLUMN "expenseID" SET DEFAULT nextval('testing.expense_expenseid_seq'::regclass);

ALTER TABLE ONLY    testing.expense ALTER COLUMN "categoryID" SET DEFAULT nextval('testing."expense_categoryID_seq"'::regclass);


ALTER TABLE ONLY    testing.feetype ALTER COLUMN "feetypeID" SET DEFAULT nextval('testing.feetype_feetypeid_seq'::regclass);

ALTER TABLE ONLY    testing.feetype_category ALTER COLUMN "feetype_categoryID" SET DEFAULT nextval('testing."feetype_category_feetype_categoryID_seq"'::regclass);


ALTER TABLE ONLY    testing.financial_category ALTER COLUMN "categoryID" SET DEFAULT nextval('testing."financial_category_categoryID_seq"'::regclass);

ALTER TABLE ONLY    testing.financial_statement ALTER COLUMN "financialID" SET DEFAULT nextval('testing."financial_statement_financialID_seq"'::regclass);



ALTER TABLE ONLY    testing.grade ALTER COLUMN "gradeID" SET DEFAULT nextval('testing.grade_gradeid_seq'::regclass);


ALTER TABLE ONLY    testing.hmember ALTER COLUMN "hmemberID" SET DEFAULT nextval('testing.hmember_hmemberid_seq'::regclass);


ALTER TABLE ONLY    testing.hostel ALTER COLUMN "hostelID" SET DEFAULT nextval('testing.hostel_hostelid_seq'::regclass);


ALTER TABLE ONLY    testing.ini_config ALTER COLUMN "configID" SET DEFAULT nextval('testing.ini_config_configid_seq'::regclass);


ALTER TABLE ONLY    testing.invoice ALTER COLUMN "invoiceID" SET DEFAULT nextval('testing.invoice_invoiceid_seq'::regclass);


ALTER TABLE ONLY    testing.issue ALTER COLUMN "issueID" SET DEFAULT nextval('testing.issue_issueid_seq'::regclass);


ALTER TABLE ONLY    testing.item ALTER COLUMN item_id SET DEFAULT nextval('testing.item_item_id_seq'::regclass);


ALTER TABLE ONLY    testing.lmember ALTER COLUMN "lmemberID" SET DEFAULT nextval('testing.lmember_lmemberid_seq'::regclass);


ALTER TABLE ONLY    testing.mailandsms ALTER COLUMN "mailandsmsID" SET DEFAULT nextval('testing.mailandsms_mailandsmsid_seq'::regclass);


ALTER TABLE ONLY    testing.mailandsmstemplate ALTER COLUMN "mailandsmstemplateID" SET DEFAULT nextval('testing.mailandsmstemplate_mailandsmstemplateid_seq'::regclass);

ALTER TABLE ONLY    testing.mailandsmstemplatetag ALTER COLUMN "mailandsmstemplatetagID" SET DEFAULT nextval('testing.mailandsmstemplatetag_mailandsmstemplatetagid_seq'::regclass);

ALTER TABLE ONLY    testing.mark ALTER COLUMN "markID" SET DEFAULT nextval('testing.mark_markid_seq'::regclass);

ALTER TABLE ONLY    testing.media ALTER COLUMN "mediaID" SET DEFAULT nextval('testing.media_mediaid_seq'::regclass);


ALTER TABLE ONLY    testing.media_category ALTER COLUMN "mcategoryID" SET DEFAULT nextval('testing.media_category_mcategoryid_seq'::regclass);

ALTER TABLE ONLY    testing.media_share ALTER COLUMN "shareID" SET DEFAULT nextval('testing.media_share_shareid_seq'::regclass);


ALTER TABLE ONLY    testing.message ALTER COLUMN "messageID" SET DEFAULT nextval('testing.message_messageid_seq'::regclass);


ALTER TABLE ONLY    testing.migrations ALTER COLUMN version SET DEFAULT nextval('testing.migrations_version_seq'::regclass);


ALTER TABLE ONLY    testing.notice ALTER COLUMN "noticeID" SET DEFAULT nextval('testing.notice_noticeid_seq'::regclass);


ALTER TABLE ONLY    testing.parent ALTER COLUMN "parentID" SET DEFAULT nextval('testing.parent_parentid_seq'::regclass);


ALTER TABLE ONLY    testing.payment ALTER COLUMN "paymentID" SET DEFAULT nextval('testing.payment_paymentid_seq'::regclass);


ALTER TABLE ONLY    testing.promotionsubject ALTER COLUMN "promotionSubjectID" SET DEFAULT nextval('testing.promotionsubject_promotionsubjectid_seq'::regclass);



ALTER TABLE ONLY    testing.receipt ALTER COLUMN "receiptID" SET DEFAULT nextval('testing."receipt_receiptID_seq"'::regclass);


ALTER TABLE ONLY    testing.reply_msg ALTER COLUMN "replyID" SET DEFAULT nextval('testing.reply_msg_replyid_seq'::regclass);


ALTER TABLE ONLY    testing.reset ALTER COLUMN "resetID" SET DEFAULT nextval('testing.reset_resetid_seq'::regclass);

ALTER TABLE ONLY    testing.routine ALTER COLUMN "routineID" SET DEFAULT nextval('testing.routine_routineid_seq'::regclass);


ALTER TABLE ONLY    testing.section ALTER COLUMN "sectionID" SET DEFAULT nextval('testing.section_sectionid_seq'::regclass);


ALTER TABLE ONLY    testing.setting ALTER COLUMN "settingID" SET DEFAULT nextval('testing.setting_settingid_seq'::regclass);


ALTER TABLE ONLY    testing.sms ALTER COLUMN sms_id SET DEFAULT nextval('testing.sms_sms_id_seq'::regclass);



ALTER TABLE ONLY    testing.smssettings ALTER COLUMN "smssettingsID" SET DEFAULT nextval('testing.smssettings_smssettingsid_seq'::regclass);



ALTER TABLE ONLY    testing.special_promotion ALTER COLUMN id SET DEFAULT nextval('testing.special_promotion_id_seq'::regclass);



ALTER TABLE ONLY    testing.student ALTER COLUMN "studentID" SET DEFAULT nextval('testing.student_studentid_seq'::regclass);


ALTER TABLE ONLY    testing.student_archive ALTER COLUMN id SET DEFAULT nextval('testing.student_archive_id_seq'::regclass);


ALTER TABLE ONLY    testing.subject ALTER COLUMN "subjectID" SET DEFAULT nextval('testing.subject_subjectid_seq'::regclass);


ALTER TABLE ONLY    testing.subject_section ALTER COLUMN subject_section_id SET DEFAULT nextval('testing.subject_section_subject_subject_id_seq'::regclass);

ALTER TABLE ONLY    testing.subject_student ALTER COLUMN subject_student_id SET DEFAULT nextval('testing.subject_student_subject_student_id_seq'::regclass);


ALTER TABLE ONLY    testing.tattendance ALTER COLUMN "tattendanceID" SET DEFAULT nextval('testing.tattendance_tattendanceid_seq'::regclass);


ALTER TABLE ONLY    testing.teacher ALTER COLUMN "teacherID" SET DEFAULT nextval('testing.teacher_teacherid_seq'::regclass);



ALTER TABLE ONLY    testing.tmember ALTER COLUMN "tmemberID" SET DEFAULT nextval('testing.tmember_tmemberid_seq'::regclass);


ALTER TABLE ONLY    testing.transport ALTER COLUMN "transportID" SET DEFAULT nextval('testing.transport_transportid_seq'::regclass);


ALTER TABLE ONLY    testing."user" ALTER COLUMN "userID" SET DEFAULT nextval('testing.user_userid_seq'::regclass);


ALTER TABLE ONLY    testing.vendor ALTER COLUMN vendor_id SET DEFAULT nextval('testing.vendor_vendor_id_seq'::regclass);

ALTER TABLE ONLY    testing.academic_year
    ADD CONSTRAINT accademic_year_name_key UNIQUE (name);


ALTER TABLE ONLY    testing.academic_year
    ADD CONSTRAINT accademic_year_pkey PRIMARY KEY (id);



ALTER TABLE ONLY    testing.class_exam
    ADD CONSTRAINT class_exam_pkey PRIMARY KEY (class_exam_id);



ALTER TABLE ONLY    testing.financial_category
    ADD CONSTRAINT financial_category_pkey PRIMARY KEY ("categoryID");

ALTER TABLE ONLY    testing.financial_statement
    ADD CONSTRAINT financial_statement_pkey PRIMARY KEY ("financialID");



ALTER TABLE ONLY    testing.bank_account
    ADD CONSTRAINT pk_account_number PRIMARY KEY (bank_account_id);


ALTER TABLE ONLY    testing.book
    ADD CONSTRAINT pk_bok_id PRIMARY KEY ("bookID");



ALTER TABLE ONLY    testing.book_class
    ADD CONSTRAINT pk_book_class_id PRIMARY KEY (book_class_id);



ALTER TABLE ONLY    testing.classes
    ADD CONSTRAINT pk_classes_id PRIMARY KEY ("classesID");


ALTER TABLE ONLY    testing.email
    ADD CONSTRAINT pk_email_id PRIMARY KEY (email_id);

ALTER TABLE ONLY    testing.exam
    ADD CONSTRAINT pk_exam_id PRIMARY KEY ("examID");



ALTER TABLE ONLY    testing.exam_report
    ADD CONSTRAINT pk_exam_report_id PRIMARY KEY (exam_report_id);



ALTER TABLE ONLY    testing.feetype
    ADD CONSTRAINT pk_feetype_id PRIMARY KEY ("feetypeID");


ALTER TABLE ONLY    testing.grade
    ADD CONSTRAINT "pk_gradeID" PRIMARY KEY ("gradeID");


ALTER TABLE ONLY    testing.item
    ADD CONSTRAINT pk_item_id PRIMARY KEY (item_id);


ALTER TABLE ONLY    testing.classlevel
    ADD CONSTRAINT pk_level_id PRIMARY KEY (classlevel_id);


ALTER TABLE ONLY    testing.receipt
    ADD CONSTRAINT pk_receipt_id PRIMARY KEY ("receiptID");


ALTER TABLE ONLY    testing.sms
    ADD CONSTRAINT pk_sms_id PRIMARY KEY (sms_id);



ALTER TABLE ONLY    testing.special_promotion
    ADD CONSTRAINT pk_special_promotion PRIMARY KEY (id);



ALTER TABLE ONLY    testing.subject_section
    ADD CONSTRAINT pk_subject_section PRIMARY KEY (subject_section_id);


ALTER TABLE ONLY    testing.subject_student
    ADD CONSTRAINT pk_subject_student PRIMARY KEY (subject_student_id);


ALTER TABLE ONLY    testing.vendor
    ADD CONSTRAINT pk_vendor_id PRIMARY KEY (vendor_id);


ALTER TABLE ONLY    testing.invoice
    ADD CONSTRAINT "primary key16" PRIMARY KEY ("invoiceID");


ALTER TABLE ONLY    testing.issue
    ADD CONSTRAINT "primary key17" PRIMARY KEY ("issueID");


ALTER TABLE ONLY    testing.lmember
    ADD CONSTRAINT "primary key18" PRIMARY KEY ("lmemberID");



ALTER TABLE ONLY    testing.mailandsms
    ADD CONSTRAINT "primary key19" PRIMARY KEY ("mailandsmsID");



ALTER TABLE ONLY    testing.mailandsmstemplate
    ADD CONSTRAINT "primary key20" PRIMARY KEY ("mailandsmstemplateID");


ALTER TABLE ONLY    testing.mailandsmstemplatetag
    ADD CONSTRAINT "primary key21" PRIMARY KEY ("mailandsmstemplatetagID");


ALTER TABLE ONLY    testing.mark
    ADD CONSTRAINT "primary key22" PRIMARY KEY ("markID");


ALTER TABLE ONLY    testing.media
    ADD CONSTRAINT "primary key23" PRIMARY KEY ("mediaID");


ALTER TABLE ONLY    testing.media_category
    ADD CONSTRAINT "primary key24" PRIMARY KEY ("mcategoryID");

ALTER TABLE ONLY    testing.media_share
    ADD CONSTRAINT "primary key25" PRIMARY KEY ("shareID");

ALTER TABLE ONLY    testing.message
    ADD CONSTRAINT "primary key26" PRIMARY KEY ("messageID");

ALTER TABLE ONLY    testing.notice
    ADD CONSTRAINT "primary key27" PRIMARY KEY ("noticeID");


ALTER TABLE ONLY    testing.parent
    ADD CONSTRAINT "primary key28" PRIMARY KEY ("parentID");


ALTER TABLE ONLY    testing.payment
    ADD CONSTRAINT "primary key29" PRIMARY KEY ("paymentID");

ALTER TABLE ONLY    testing.promotionsubject
    ADD CONSTRAINT "primary key30" PRIMARY KEY ("promotionSubjectID");

ALTER TABLE ONLY    testing.reply_msg
    ADD CONSTRAINT "primary key31" PRIMARY KEY ("replyID");


ALTER TABLE ONLY    testing.reset
    ADD CONSTRAINT "primary key32" PRIMARY KEY ("resetID");


ALTER TABLE ONLY    testing.routine
    ADD CONSTRAINT "primary key33" PRIMARY KEY ("routineID");


ALTER TABLE ONLY    testing.school_sessions
    ADD CONSTRAINT "primary key34" PRIMARY KEY (session_id);

ALTER TABLE ONLY    testing.section
    ADD CONSTRAINT "primary key35" PRIMARY KEY ("sectionID");

ALTER TABLE ONLY    testing.setting
    ADD CONSTRAINT "primary key36" PRIMARY KEY ("settingID");

ALTER TABLE ONLY    testing.smssettings
    ADD CONSTRAINT "primary key37" PRIMARY KEY ("smssettingsID");

ALTER TABLE ONLY    testing.student
    ADD CONSTRAINT "primary key38" PRIMARY KEY ("studentID");


ALTER TABLE ONLY    testing.subject
    ADD CONSTRAINT "primary key39" PRIMARY KEY ("subjectID");

ALTER TABLE ONLY    testing.tattendance
    ADD CONSTRAINT "primary key40" PRIMARY KEY ("tattendanceID");


ALTER TABLE ONLY    testing.teacher
    ADD CONSTRAINT "primary key41" PRIMARY KEY ("teacherID");


ALTER TABLE ONLY    testing.tmember
    ADD CONSTRAINT "primary key42" PRIMARY KEY ("tmemberID");


ALTER TABLE ONLY    testing.transport
    ADD CONSTRAINT "primary key43" PRIMARY KEY ("transportID");

ALTER TABLE ONLY    testing."user"
    ADD CONSTRAINT "primary key44" PRIMARY KEY ("userID");

ALTER TABLE ONLY    testing.student_archive
    ADD CONSTRAINT student_archive_pkey PRIMARY KEY (id);


CREATE UNIQUE INDEX "fki_alertID_pk" ON alert USING btree ("alertID" NULLS FIRST);


COMMENT ON INDEX "fki_alertID_pk" IS 'primary key';



CREATE INDEX last_activity_idx ON school_sessions USING btree (last_activity);


ALTER TABLE ONLY    testing.academic_year
    ADD CONSTRAINT accademic_year_class_level_id_fkey FOREIGN KEY (class_level_id) REFERENCES classlevel(classlevel_id) ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE ONLY    testing.financial_category
    ADD CONSTRAINT "financial_category_financialID_fkey" FOREIGN KEY ("financialID") REFERENCES financial_statement("financialID");



ALTER TABLE ONLY    testing.book_class
    ADD CONSTRAINT fk_bookid FOREIGN KEY (book_id) REFERENCES book("bookID") ON UPDATE CASCADE ON DELETE CASCADE;


ALTER TABLE ONLY    testing.class_exam
    ADD CONSTRAINT fk_class_exam_class_id FOREIGN KEY (class_id) REFERENCES classes("classesID") ON UPDATE CASCADE ON DELETE CASCADE;



ALTER TABLE ONLY    testing.class_exam
    ADD CONSTRAINT fk_class_exam_exam_id FOREIGN KEY (exam_id) REFERENCES exam("examID") ON UPDATE CASCADE ON DELETE CASCADE;



ALTER TABLE ONLY    testing.book_class
    ADD CONSTRAINT fk_classes_id FOREIGN KEY (classes_id) REFERENCES classes("classesID") ON UPDATE CASCADE ON DELETE CASCADE;



ALTER TABLE ONLY    testing.subject_section
    ADD CONSTRAINT fk_section_id FOREIGN KEY (section_id) REFERENCES section("sectionID") ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE ONLY    testing.special_promotion
    ADD CONSTRAINT fk_special_promotion_ac FOREIGN KEY (id) REFERENCES academic_year(id) ON UPDATE CASCADE ON DELETE SET NULL;


ALTER TABLE ONLY    testing.subject_section
    ADD CONSTRAINT fk_subject_id FOREIGN KEY (subject_id) REFERENCES subject("subjectID") ON UPDATE CASCADE ON DELETE CASCADE;



ALTER TABLE ONLY    testing.student_archive
    ADD CONSTRAINT student_archive_accademic_year_id_fkey FOREIGN KEY (academic_year_id) REFERENCES academic_year(id) ON UPDATE RESTRICT ON DELETE RESTRICT;



ALTER TABLE ONLY    testing.student_archive
    ADD CONSTRAINT student_archive_section_id_fkey FOREIGN KEY (section_id) REFERENCES section("sectionID") ON UPDATE RESTRICT ON DELETE RESTRICT;



ALTER TABLE ONLY   testing.student_archive
    ADD CONSTRAINT student_archive_student_id_fkey FOREIGN KEY (student_id) REFERENCES student("studentID") ON UPDATE RESTRICT ON DELETE RESTRICT;

CREATE OR REPLACE VIEW testing.student_info AS 
 SELECT *
   FROM testing.student
  WHERE student.status = 1;
