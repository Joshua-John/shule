
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-user"></i> <?=$this->lang->line('panel_title')?></h3>
       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_parent')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
   <?php
                    $usertype = $this->session->userdata("usertype");
                    if($usertype == "Admin" || $usertype=='Secretary') {
                ?>
                    <h5 class="page-header">
                        <a class="btn btn-success" href="<?php echo base_url('parentes/add') ?>">
                            <i class="fa fa-plus"></i>
                            <?=$this->lang->line('add_title')?>
                        </a>
                    </h5>
                <?php } ?>



                <div class="col-sm-6 col-sm-offset-3 list-group">
                    <div class="list-group-item list-group-item-warning">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="col-sm-2 col-sm-offset-2 control-label">
                                    <?=$this->lang->line("student_classes")?>
                                </label>
                                <div class="col-sm-6">
                                    <?php
                                        $array = array("0" => $this->lang->line("student_select_class"));
					$array[date('Y')]='All';
                                        foreach ($classes as $classa) {
                                            $array[$classa->classesID] = $classa->classes;
                                        }
                                        echo form_dropdown("classesID", $array, set_value("classesID", $set), "id='classesID' class='form-control'");
                                    ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
		
		
                <?php 
                    $usertype = $this->session->userdata("usertype");
                    if($usertype == "Admin") {
                ?>
                    <h5 class="page-header">
<!--                        <a class="btn btn-success" href="<?php //echo base_url('parentes/add') ?>">
                            <i class="fa fa-plus"></i> 
                            <?=$this->lang->line('add_title')?>
                        </a>-->
				<a href="<?=  base_url('parentes/export')?>" style="float: right;" class="btn btn-success btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Export"><i class="fa fa-cloud-download"></i></a>
                    </h5>
		
                <?php } ?>


                <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th class="col-sm-2"><?=$this->lang->line('slno')?></th>
                                <th class="col-sm-2"><?=$this->lang->line('parentes_photo')?></th>
                                <th class="col-sm-2"><?=$this->lang->line('parentes_name')?></th>
                                <th class="col-sm-2"><?=$this->lang->line('parentes_email')?></th>
				 <th class="col-sm-2"><?=$this->lang->line('parentes_phone')?></th>
                                <th class="col-sm-2"><?=$this->lang->line('action')?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(count($parentes)) {$i = 1; foreach($parentes as $parent) { ?>
                                <tr>
                                    <td data-title="<?=$this->lang->line('slno')?>">
                                        <?php echo $i; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('parentes_photo')?>">
                                        <?php $array = array(
                                                "src" => base_url('uploads/images/'.$parent->photo),
                                                'width' => '35px',
                                                'height' => '35px',
                                                'class' => 'img-rounded'

                                            );
                                            echo img($array); 
                                        ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('parentes_name')?>">
                                        <?php echo $parent->name; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('parentes_email')?>">
                                        <?=$parent->email; ?>
                                    </td>
				     <td data-title="<?=$this->lang->line('parentes_phone')?>">
                                        <?php echo $parent->phone; ?>
                                    </td>
                                    <td data-title="<?=$this->lang->line('action')?>">
                                        <?php echo btn_view('parentes/view/'.$parent->parentID, $this->lang->line('view')) ?>
                                        <?php echo btn_edit('parentes/edit/'.$parent->parentID, $this->lang->line('edit')) ?>
                                        <?php echo btn_delete('parentes/delete/'.$parent->parentID, $this->lang->line('delete')) ?>
                                    </td>
                                </tr>
                            <?php $i++; }} ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#classesID').change(function() {
        var classesID = $(this).val();
        if(classesID == 0) {
            $('#hide-table').hide();
            $('.nav-tabs-custom').hide();
        } else {
            $.ajax({
                type: 'POST',
                url: "<?=base_url('parentes/parentes_list')?>",
                data: "id=" + classesID,
                dataType: "html",
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });
</script>