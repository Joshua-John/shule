<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-puzzle-piece"></i> <?= $this->lang->line('panel_title') ?></h3>

        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li class="active"><?= $this->lang->line('menu_mark') ?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="col-sm-6 col-sm-offset-3 list-group">
                    <div class="list-group-item list-group-item-warning">
                        <form style="" class="form-horizontal" role="form" method="post">  
                            <div class="form-group">              
                                <label for="studentID" class="col-sm-2 col-sm-offset-2 control-label">
				    <?= $this->lang->line("mark_student") ?>
                                </label>
                                <div class="col-sm-6">
				    <?php
				    $array = array("0" => $this->lang->line("mark_select_student"));
				    if ($allstudents) {
					foreach ($allstudents as $allstudent) {
					    $array[$allstudent->studentID] = $allstudent->name;
					}
				    }
				    echo form_dropdown("studentID", $array, set_value("studentID", $set), "id='studentID' class='form-control'");
				    ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-sm-12" id="load_information">
                    <section class="panel">
                        <div class="profile-view-head">
                            <a href="#">
				<?= img(base_url('uploads/images/' . $student->photo)) ?>
                            </a>

                            <h1><?= $student->name ?></h1>
                            <p><?= $this->lang->line("student_classes") . " " . $classes->classes ?></p>

                        </div>
			<p align="right"><a class="btn btn-small btn-info" onclick="printDiv('load_information')"><i class="fa fa-print"></i> print </a></p>
                        <div class="panel-body profile-view-dis">
                            <h1><?= $this->lang->line("personal_information") ?></h1>
                            <div class="row">
                                <div class="profile-view-tab">
                                    <p><span><?= $this->lang->line("mark_roll") ?> </span>: <?= $student->roll ?></p>
                                </div>
                                <div class="profile-view-tab">
                                    <p><span><?= $this->lang->line("menu_section") ?> </span>: <?php
					if (count($section)) {
					    echo $section->section;
					} else {
					    echo $student->section;
					}
					?></p>
                                </div>
                                <div class="profile-view-tab">
                                    <p><span><?= $this->lang->line("mark_dob") ?> </span>: <?= date("d M Y", strtotime($student->dob)) ?></p>
                                </div>
                                <div class="profile-view-tab">
                                    <p><span><?= $this->lang->line("mark_sex") ?> </span>: <?= $student->sex ?></p>
                                </div>
                                <div class="profile-view-tab">
                                    <p><span><?= $this->lang->line("mark_religion") ?> </span>: <?= $student->religion ?></p>
                                </div>
                                <div class="profile-view-tab">
                                    <p><span><?= $this->lang->line("mark_email") ?> </span>: <?= $student->email ?></p>
                                </div>
                                <div class="profile-view-tab">
                                    <p><span><?= $this->lang->line("mark_phone") ?> </span>: <?= $student->phone ?></p>
                                </div>
                                <div class="profile-view-tab">
                                    <p><span><?= $this->lang->line("mark_address") ?> </span>: <?= $student->address ?></p>
                                </div>
                            </div>

                            <h1><?= $this->lang->line("report_information") ?></h1>
			    <h3>Single Exams Reports</h3>
			    <br/>
			    <table class="table table-striped table-bordered">
				<thead>
				    <tr>
					<th>Exam Name</th>
					<th>Exam Date</th>
					<th>Action</th>
				    </tr>
				</thead>
				<tbody>
				    <?php foreach ($exams as $exam) {
					
					?>
    				    <tr>
    					<td><?= $exam->exam ?></td>
    					<td><?= date('d M Y', strtotime($exam->date)) ?></td>
					<td><a href="<?= base_url("exam/singlereport/" . $student->studentID . "?exam=".$exam->examID) ?>" class="btn btn-small btn-info"> <i class="fa fa-file"></i> View Report</a></td>
    				    </tr>
				    <?php } ?>
				</tbody>
			    </table>

			    <br/><h1>School Official Reports</h1>
			    <table class="table table-striped table-bordered">
				<thead>
				    <tr>
					<th>Exam Name</th>
					<th>Exam Date</th>
					<th>Action</th>
				    </tr>
				</thead>
				<tbody>
				    <?php foreach ($exams_report as $rep) { ?>
    				    <tr>
    					<td><?= $rep->name ?></td>
    					<td><?= date('d M Y', strtotime($rep->created_at)) ?></td>
					<td><a href="<?= base_url("exam/singlecombined/" . $student->studentID . "?rank=".$rep->rank."&exam=" . $rep->combined_exams) ?>" class="btn btn-small btn-info"> <i class="fa fa-file"></i> View Report</a></td>
    				    </tr>
				    <?php } ?>
    				</tbody>
    			    </table>
			   </div>
                    </section>
                </div>



            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#studentID').change(function () {
	var studentID = $(this).val();
	if (studentID == 0) {
	    $('#hide-table').hide();
	} else {
	    $.ajax({
		type: 'POST',
		url: "<?= base_url('mark/student_list') ?>",
		data: "id=" + studentID,
		dataType: "html",
		success: function (data) {
		    window.location.href = data;
		}
	    });
	}
    });
    function printDiv(divID) {
	//Get the HTML of div
	var divElements = document.getElementById(divID).innerHTML;
	//Get the HTML of whole page
	var oldPage = document.body.innerHTML;

	//Reset the page's HTML with div's HTML only
	document.body.innerHTML =
		"<html><head><title></title></head><body>" +
		divElements + "</body>";

	//Print Page
	window.print();

	//Restore orignal HTML
	document.body.innerHTML = oldPage;
    }
</script>