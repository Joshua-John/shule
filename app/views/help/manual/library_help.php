<?php
$user = empty($usertype) ? 'Administrator' : $usertype;
?>

<!DOCTYPE html>
<html>
 <head>
    <title>ShuleSoft Manual</title>
    <?php
    $css = base_url() . "/assets/manual_assets/";
    $js = base_url() . "/assets/manual_assets/";
    ?>
    <meta charset="utf-8">
    <!--        <link rel="canonical" href="http://www.templatemonster.com/help/quick-start-guide/website-templates/responsive-website-templates-v1-1/index_en.html"/> -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?= $css ?>css/bootstrap.css" media="screen">
    <link rel="stylesheet" href="<?= $css ?>css/responsive.css" media="screen">
    <link rel="stylesheet" href="<?= $css ?>css/prettify.css"  media="screen">
    <link rel="stylesheet" href="<?= $css ?>css/prettyPhoto.css"  media="screen">
    <link rel="stylesheet" href="<?= $css ?>css/idea.css"  media="screen">
    <link rel="stylesheet" href="<?= $css ?>css/style.css" media="screen">
    <link rel="stylesheet" href="<?= $css ?>css/contact-form.css" media="screen">
    <script src="<?= $js ?>js/jquery.js"></script>
    <script src="<?= $js ?>js/jquery.scrollTo.js"></script>
    <script src="<?= $js ?>js/jquery-migrate-1.1.0.js"></script>
    <script src="<?= $js ?>js/prettify.js"></script>
    <script src="<?= $js ?>js/bootstrap-affix.js"></script>
    <script src="<?= $js ?>js/jquery.prettyPhoto.js"></script>

    <script src="<?= $js ?>js/TMForm.js"></script>
    <script src="<?= $js ?>js/modal.js"></script>
    <script src="<?= $js ?>js/bootstrap-filestyle.js"></script>

    <script src="<?= $js ?>js/sForm.js"></script>
    <script src="<?= $js ?>js/jquery.countdown.min.js"></script>
    <script src="<?php echo $js ?>js/scripts.js"></script>
    <!--[if IE 8]>
    <link rel="stylesheet" href="css/ie.css" />
    <![endif]-->

    <!--[if lt IE 9]>
    <div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/img/upgrade.jpg"border="0"alt=""/></a></div>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <![endif]-->
</head>

<body>

<!-- header
================================================== -->
<header class="header">
    <div class="container">
        <div class="row">
            <article class="span4"><a href="<?= base_url() ?>" target="_blank">Home</a></article>
            <article class="span6">
                <h1>
                    <i class="fa fa-book"></i>
                    ShuleSoft System Manual
                </h1>
            </article>
            <article class="span2">

                <div id="versions" class="select-menu pull-right">
                    <div class="select-menu_icon"><b>v1-0</b><i class="icon-angle-down"></i></div>
                </div>
            </article>
        </div>
    </div>
</header>
<div id="content">
<div class="bg-content-top">
<div class="container">

<!-- Docs nav
    ================================================== -->
<div class="row">
<div class="span3">

    <div id="nav_container">
        <a href="javascript:;" id="affect_all">
            <span class="expand">
                <i class="icon-angle-down"></i>
            </span>
            <span class="close">
                <i class="icon-angle-up"></i>
            </span>
        </a>
        <div class="clearfix"></div>
        <ul class="nav nav-list bs-docs-sidenav"  id="nav">

            <li class="nav-item item1">
                <dl class="slide-down">
                    <dt><a href="#introduction" class="icon-info-sign">Introduction</a></dt>
                    <dd></dd>
                </dl>
            </li>
            <li class="nav-item item2">
                <dl class="slide-down">
                    <dt><a href="#modules" class="icon-check">Librarian Modules</a></dt>
                    <dd></dd>
                </dl>
            </li>
            <li class="nav-item item3">
                <dl class="slide-down">
                    <dt><a href="#general-information" class="icon-play">Features</a></dt>
                    <dd></dd>
                </dl>
            </li>
            <li class="nav-item item4">
                <dl class="slide-down">
                    <dt><a href="#subject" class="icon-book">Subject</a></dt>
                    <dd></dd>
                </dl>
            </li>
            <li class="nav-item item5">
                <dl class="slide-down">
                    <dt><a href="#member" class="icon-plus-sign">Member</a></dt>
                    <dd></dd>
                </dl>
            </li>
            <li class="nav-item item6">
                <dl class="slide-down">
                    <dt><a href="#books" class="icon-plus-sign">Books</a></dt>
                    <dd></dd>
                </dl>
            </li>

            <li class="nav-item item7">
                <dl class="slide-down">
                    <dt><a href="#issue" class="icon-book">Issue Book</a></dt>
                    <dd></dd>
                </dl>
            </li>

            <li class="nav-item item8">
                <dl class="slide-down">
                    <dt><a href="#fine" class="icon-play">Fine</a></dt>
                    <dd></dd>
                </dl>
            </li>
            <li class="nav-item item9">
                <dl class="slide-down">
                    <dt><a href="#message" class="icon-home">Message</a></dt>
                    <dd></dd>
                </dl>
            </li>


            <li class="nav-item item10">
                <dl class="slide-down">
                    <dt><a href="#notice" class="icon-play">Notice</a></dt>
                    <dd></dd>
                </dl>
            </li>

            <li class="nav-item item11">
                <dl class="slide-down">
                    <dt><a href="#transport" class="icon-bar-chart">Transport</a></dt>
                    <dd></dd>
                </dl>
            </li>
            <li class="nav-item item12">
                <dl class="slide-down">
                    <dt><a href="#hostel" class="icon-home">Hostel</a></dt>
                    <dd></dd>
                </dl>
            </li>






            <li class="nav-item item13">
                <dl class="slide-down">
                    <dt><a href="#conclusion" class="icon-question-sign">Conclusion</a></dt>
                    <dd></dd>
                </dl>
            </li>
        </ul>
    </div>
</div>


<div class="span9">

<!-- box-content
================================================== -->
<div class="box-content">
<!-- block-started
================================================== -->
<section class="block-started"  id="introduction">
    <h2 class="item1"><i class="icon-info-sign"></i> Introduction <small>
            <i>What is ShuleSoft ?</i></small></h2>
    <p>
        ShuleSoft is Cloud based management system for schools that brings efficiency in school management and interactions among students, teachers and parents. ShuleSoft School management system is used for managing education based organizations like schools, colleges, universities etc.
    <p>

</section>

<section class="block-templates" id="modules">
    <h2 class="item2"><i class="icon-check"></i>Librarian Modules Available</h2>

    <p>ShuleSoft has number of modules available.  Librarian can access the following modules</p>
    <ol>
       <li>Library</li>
    </ol>
    <p>As <?= $user ?>, you can view and manage all modules for school operations.</p>
</section>

<section class="block-templates" id="general-information">
    <h2 class="item3"><i class="icon-play"></i>ShuleSoft Features </h2>


    <p>The following are some of the features available in shulesoft software. </p>

    <section id="features">
    <pre class="prettyprint lang-plain linenums">

Multiple Modules that link each other to bring more efficiency in school management
Cloud based solution. This means, the software will be accessed via the internet to enable each and every user to interact together and also to enable interlinking of system with payment systems
Linked with SMS platform to send SMS and Email notification on each major action
System support multiple accounts creation such that, each user (admin, teacher, student, parent etc) will have his/her own account and view his/her own information only 
Allow creation of Custom classes and sections.
Allow admin to create subjects and grading point as per country  or custom regulations
Allow creation of exams and exam schedule and also, teacher can mark a subject via the system and the system will help/her to generate all required reports
Allow registration of students in library, hostel and transport routes
Allow attendances (teacher, students and examination) to be taken and generate attendance reports
Allow simple creation of routines (classes and exams)
Accounting modules help a school to easily create invoices, accept electronic payments and manage all school accounting
Each user in the system have a unique profile to enable him/her to view and perform required task. These users include accountants, administrators, teachers, librarians, parents, students etc 

    </pre>
    </section>
</section>


<section class="block-templates" id="get-started">
    <h2 class="item4"><i class="icon-cog"></i>How to Get Started</h2>
    <p>This is the step by step guide on how to get started to use ShuleSoft in your school</p>
    <section id="how_to_start">
        <h4 id="add-teacher">View Teacher</h4>

					<pre class="prettyprint">
Login as Librarian
Select user menu in sidebar
Click  Teacher
Then List of teacher will be displayed
					</pre>
        <p>
            <img src="<?= base_url() ?>assets/images/librarian/view_teacher.png" width="1000" style="padding: 0px 20px;"/>
        </p>

        <h4 id="subject">View Subject</h4>

					<pre class="prettyprint">
Login as Librarian
Click subject menu in sidebar
Select class in the dropdown
Then you will see student's subject
					</pre>
        <p>
            <img src="<?= base_url() ?>assets/images/librarian/view_subject.png" width="1000" style="padding: 0px 20px;"/>
        </p>

        <p>
            <img src="<?= base_url() ?>assets/images/librarian/view_subject.png" width="1000" style="padding: 0px 20px;"/>
        </p>

        <h4 id="member">View Member</h4>
					<pre class="prettyprint">
Login as Librarian
Click Librarian menu in sidebar
Then Select student to view his/her marks
					</pre>
        <p>
            <img src="<?= base_url() ?>assets/images/librarian/view_member1.png" width="1000" style="padding: 0px 20px;"/>
        </p>
        <h4>List of Library Member</h4>
        <p>
            <img src="<?= base_url() ?>assets/images/librarian/view_member.png" width="1000" style="padding: 0px 20px;"/>
        </p>

    </section>

</section>

<section class="block-templates" id="attendance">
    <h2 class="item5"><i class="icon-play-circle"></i> How to View Student Attendance</h2>
    <section id="attendance">


        <section id="books">
            <h4>View Books</h4>
					<pre class="prettyprint">
Login as Librarian
Select Books menu in sidebar
Then list of books will be displayed

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/librarian/view_books.png" width="1000" style="padding: 0px 20px;"/>
            </p>
            <h4>Add Book</h4>
					<pre class="prettyprint">
Login as Librarian
Click Add Book button
Form will be displayed and fill the form and click Add Book

            <p>
                <img src="<?= base_url() ?>assets/images/librarian/view_member.png" width="1000" style="padding: 0px 20px;"/>
            </p>
        </section>

        <section id="issue">
            <h4>Issue Books</h4>
					<pre class="prettyprint">
Login as Librarian
Select Issue on sidebar menu
Enter libraryID and then the issued book will be displayed

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/librarian/issue_book.png" width="1000" style="padding: 0px 20px;"/>
            </p>
        </section>

        <section id="fine">
            <h4>Fine</h4>
					<pre class="prettyprint">
Login as Librarian
Select Fine on sidebar menu
Then select day,select month,select year and click Fine


					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/librarian/fine.png" width="1000" style="padding: 0px 20px;"/>
            </p>
        </section>


        <section id="message">
            <h4>View Message</h4>
					<pre class="prettyprint">
Login as Librarian
Select Message menu in sidebar
Then message platform will be displayed and then you can compose and see the message

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/librarian/view_message.png" width="1000" style="padding: 0px 20px;"/>
            </p>
        </section>

        <section id="notice">

            <h4>View Notice</h4>
					<pre class="prettyprint">
Login as Librarian
Select Notice menu in sidebar
Then list of notices will be displayed

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/librarian/view_notice.png" width="1000" style="padding: 0px 20px;"/>
            </p>
        </section>

        <section id="transport">

            <h4>View Transport</h4>
					<pre class="prettyprint">
Login as Librarian
Select Transport menu in sidebar
Then list of transport route will be displayed

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/librarian/view_transport_route.png" width="1000" style="padding: 0px 20px;"/>
            </p>
        </section>
        <section id="hostel">

            <h4>View Hostels</h4>
					<pre class="prettyprint">
Login as Librarian
Select Hostel menu in sidebar
Then list of hostels will be displayed

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/librarian/view_hostels.png" width="1000" style="padding: 0px 20px;"/>
            </p>
            <h4>View Hostel Category</h4>
            					<pre class="prettyprint">
Login as Librarian
Select Hostel menu in sidebar
Then click category to view hostel category

					</pre>
            <p>
                <img src="<?= base_url() ?>assets/images/librarian/view_hostel_category.png" width="1000" style="padding: 0px 20px;"/>
            </p>
        </section>

        <section id="transport">

        </section>
    </section>


</div>
</div>
</div>

</div>
<footer id="conclusion">
    <div class="container">
        @<a href="http://www.inetstz.com" target="_blank">INETS Company Limited </a>
    </div>
</footer>
</div>
</div>
<!-- Footer
 ================================================== -->


<div id="back-top">
    <a href="#"><i class="icon-double-angle-up"></i></a>
</div>
<!-- <script src="js/bootstrap-scrollspy.js"></script>  -->
<!--        <div class="language-modal">
        <div class="modal_bg"></div>
        <div class="modal">
        <div class="modal-header">
            <span class="modal_remove pull-right"><i class="icon-remove"></i></span>
            <h3>Choose language</h3>
        </div>
        <div class="modal-body">
            <ul id="modal_languages" class="nav nav-list bs-docs-sidenav"></ul>
        </div>
        </div>
    </div>-->
</body>
</html>
