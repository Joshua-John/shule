
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-wrench"></i> <?= $this->lang->line('panel_title') ?></h3>


        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li class="active"><?= $this->lang->line('menu_smssettings') ?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

                <div class="col-sm-12">

                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="<?php if ($clickatell == 1) echo 'active'; ?>"><a data-toggle="tab" href="#clickatell" aria-expanded="true">karibuSMS Summary</a></li>
                        </ul>

                        <div class="tab-content">
                           
                                
                                <div class="row">
                                    <div class="col-sm-12">
					<div  class="alert alert-info text-white">Hello, You have been registered to use karibuSMS application to easily send SMS. For more information, visit <a href="http://www.karibusms.com/api" target="_blank" class="text-white"> www.karibusms.com.</a></div>

				    </div>
				    

				</div>
			    <br><br>
			
				<div class="row">
					<div class="col-lg-3 col-xs-6">
					    <div class="small-box ">
						<a class="small-box-footer" href="">
						    <div class="icon bg-aqua" style="padding: 9.5px 18px 8px 18px;">
							<i class="fa icon-student"></i>
						    </div>
						    <div class="inner ">
							<h3>
							    120
							</h3>
							<p>
								Available SMS
							</p>
						    </div>
						</a>
					    </div>
					</div>

					<div class="col-lg-3 col-xs-6">
					    <div class="small-box ">
						<a class="small-box-footer" href="">
						    <div class="icon bg-red" style="padding: 9.5px 18px 8px 18px;">
							<i class="fa icon-teacher"></i>
						    </div>
						    <div class="inner ">
							<h3>
							    450
							</h3>
							<p>
							    Sent SMS
							</p>
						    </div>
						</a>
					    </div>
					</div>

					<div class="col-lg-3 col-xs-6">
					    <div class="small-box ">
						<a class="small-box-footer" href="">
						    <div class="icon bg-yellow">
							<i class="fa fa-user"></i>
						    </div>
						    <div class="inner ">
							<h3>
							   20
							</h3>
							<p>
							    Failed SMS
							</p>
						    </div>
						</a>
					    </div>
					</div>

					<div class="col-lg-3 col-xs-6">
					    <div class="small-box ">
						<a class="small-box-footer" href="">
						    <div class="icon bg-blue" style="padding: 9.5px 18px 8px 18px;">
							<i class="fa icon-attendance"></i>
						    </div>
						    <div class="inner ">
							<h3>
							  4
							</h3>
							<p>
							   SMS buying requests
							</p>
						    </div>
						</a>
					    </div>
					</div>
				    </div>
			    

			</div>
		    </div> <!-- nav-tabs-custom -->
		</div>


	    </div> <!-- col-sm-12 -->

	</div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->