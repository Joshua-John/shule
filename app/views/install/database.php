
<div class="panel panel-default">
    <div class="panel-heading-install">
	<ul class="nav nav-pills">
	    <li><a href="<?= base_url('install/index') ?>"><span class="fa fa-check"></span> Checklist</a></li>

	    <li class="active"><a href="<?= base_url('install/database') ?>">Database</a></li>
	    <li><a href="#">School Config</a></li>
	    <li><a href="#">Done!</a></li>
	</ul>
    </div>
    <div class="panel-body ins-bg-col">
	<h4>Database installation progress</h4>

	<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">


	    <div class="form-group">
		<div class="row">
		    <div class="col-sm-10 col-sm-offset-1">

			<div class="x_content">

			    <div class="bs-glyphicons" data-example-id="simple-jumbotron">
				<div class="jumbotron">
				    <h1><a href="#/check"><i class="fa fa-check"></i></a> Success!</h1>
				    <p>Database installed successfully.</p>
				</div>
			    </div>

			</div>

		    </div>
		</div>
	    </div>


	    <div class="form-group">
		<div class="row">
		    <div class="col-sm-4 col-sm-offset-1">
			<a href="<?= base_url('install/index') ?>" class="btn btn-default pull-right">Previous Step</a>
		    </div>
		    <div class="col-sm-4 col-sm-offset-3">
			<a href="<?= base_url('install/site') ?>" class="btn btn-success" >Next Step </a>
		    </div>
		</div>
	    </div>
	</form>
    </div>
</div>