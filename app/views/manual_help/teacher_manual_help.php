<!--/**-->
<!--* ------------------------------------------->
<!--*-->
<!--* ******* Address****************-->
<!--* INETS COMPANY LIMITED-->
<!--* P.O BOX 32258, DAR ES SALAAM-->
<!--* TANZANIA-->
<!--*-->
<!--*
            <img class="img-responsive" src="http://localhost:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/TeacherDashboard.png"/>
-->
<!--* *******Office Location *********-->
<!--* 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam-->
<!--*-->
<!--*-->
<!--* ********Contacts***************-->
<!--* Email: <info@inetstz.com>-->
<!--* Website: <www.inetstz.com>-->
<!--    * Mobile: <+255 655 406 004>-->
<!--    * Tel:    <+255 22 278 0228>-->
<!--    * ------------------------------------------->
<!--    */-->

<?php $this->load->view("components/page_header"); ?>
<?php $this->load->view("components/page_topbar"); ?>
<?php $this->load->view("components/page_menu"); ?>


<div class="container-fluid">
    <div class="row">

        <div class="col-sm-4"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div
            <div class="col-sm-12">
                <h1>SHULESOFT USER HELP</h1>
                <p>This is the user help for helping a teacher to navigate through the system
                    according to his level of access.</p>
            </div>
            <div class="col-sm-2"></div>

        </div>

        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div>
            <div class="col-sm-12">
                <h3>TEACHER MAIN DASHBOARD</h3>
                <p>This is the main part of teacher dashboard where the navigation starts after logging in
                    to the system.Check navigation items in the left side of the dashboard.</p>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/TeacherDashboard.png"/>

            </div>

            <div class="col-sm-2"></div>
            <div></div>
        </div>

        <br/>
        <hr>

        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div>
            <div class="col-sm-12"
                 <h3>CHECKING LIST OF STUDENT</h3>
                <p>By clicking User->Student->Select Class;You can view list of student 
                    according to the class example form one,form two,form three and ....</p>
                <br/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/StudentList.png"/>

            </div>
            <div class="col-sm-2"></div>
        </div>




        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div>
            <div class="col-sm-12"
                 <h3>CHECKING TEACHER LIST</h3>
                <p>By clicking User->Teacher;you will see the list of teacher with some of their details.</p>
                <br/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/Teacherlist.png"/>

            </div>
            <div class="col-sm-2"></div>
        </div>


        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div>
            <div class="col-sm-12"
                 <h3>CHECKING SUBJECT FOR SPECIFIC CLASS </h3>
                <p>By clicking "Subject" then "Select class"in drop down menu</p>
                <br/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/SubjectList.png"/>

            </div>
            <div class="col-sm-2"></div>
        </div>


        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div>
            <div class="col-sm-12"
                 <h3>CHECKING EXAM SCHEDULE</h3>
                <p>By clicking "Exam Schedule"->"Select Class" in the drop down menu you will see classes and then you ca select a class 
                    which you want to see the exam schedule</p>
                <br/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/ExamSchedule.png"/>

            </div>
            <div class="col-sm-2"></div>
        </div>


        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div>
            <div class="col-sm-12"
                 <h3>VIEW AND ADDING OF STUDENT MARKS</h3>
                <p>By Clicking Mark you can view student marks by selecting class in the drop down menu and Adding marks for the student is by clicking "Add Mark "
                    you will see a form with drop down menus where you will have to select according to your exam,class and subject</p>
                <br/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/StudentMarks.png"/>

            </div>
            <div class="col-sm-2"></div>
        </div>



        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div>
            <div class="col-sm-12"
                 <h3>VIEW ROUTINE OF THE SPECIFIC CLASS</h3>
                <p>By Clicking routine and then Select class in the drop down menu to select a routine of the specific class</p>
                <br/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/classRoutine.png"/>

            </div>
            <div class="col-sm-2"></div>
        </div>



        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div>
            <div class="col-sm-12"
                 <h3>VIEW AND ADD STUDENT ,TEACHER AND EXAM ATTENDANCE  </h3>
                <p>By Clicking Attendance then Student or Teacher or Exam ,you can select class to view their attendance
                    and by clicking "Add student attendance" you can add attendance of student for the specified date and also
                    you can view teacher attendance by clicking Teacher and viewing exam attendance by clicking Exam ->Select Class from the drop down menu</p>
                <br/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/studentAttendance.png"/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/AddAttendance.png"/>
            </div>
            <div class="col-sm-2"></div>
        </div>





        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div>
            <div class="col-sm-12"
                 <h3>VIEW BOOKS IN LIBRARY</h3>
                <p>By Clicking Books ,you can view available books in the library</p>
                <br/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/ViewBooks.png"/>

            </div>
            <div class="col-sm-2"></div>
        </div>


        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div>
            <div class="col-sm-12"
                 <h3>VIEW TRANSPORT DETAILS</h3>
                <p>By Clicking Transport ,you can view transport route with their fares</p>
                <br/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/Transport.png"/>

            </div>
            <div class="col-sm-2"></div>
        </div>

        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div>
            <div class="col-sm-12"
                 <h3>VIEW HOSTEL</h3>
                <p>By Clicking Hostels ,you can view all hostel and their categories 
                    the select hostel to know the number of hostels and category to view hostel category</p>
                <br/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/Hostels.png"/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/HostelCat.png"/>
            </div>
            <div class="col-sm-2"></div>
        </div>


        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div>
            <div class="col-sm-12"
                 <h3>STUDENT PROMOTION</h3>
                <p>By Clicking Promotion ,then you can select a class ,you will be directed to the
                    new page for adding marks of the subject in order to find how many student
                    are promoted to the next class </p>
                <br/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/StudentPromotion.png"/>

            </div>
            <div class="col-sm-2"></div>
        </div>

        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div>
            <div class="col-sm-12"
                 <h3>MEDIA</h3>
                <p>By Clicking Media you can add materials for reading like exercises,notes and other good stuffs</p>
                <br/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/filesMedia.png"/>

            </div>
            <div class="col-sm-2"></div>
        </div>


        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div>
            <div class="col-sm-12"
                 <h3>MESSAGE</h3>
                <p>By Clicking Message,you can view inbox messages and you can compose a message to the other stuff member  </p>
                <br/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/msg.png"/>

            </div>
            <div class="col-sm-2"></div>
        </div>


        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div>
            <div class="col-sm-12"
                 <h3>VIEW NOTICE</h3>
                <p>By Clicking Notice,here you can see if there is new notification from the administrator </p>
                <br/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/Notice.png"/>

            </div>
            <div class="col-sm-2"></div>
        </div>

    </div>
</div>
<script>



</script>

<?php $this->load->view("components/page_footer"); ?>

