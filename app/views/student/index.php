
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-student"></i> <?= $this->lang->line('panel_title') ?></h3>


        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li class="active"><?= $this->lang->line('menu_student') ?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

		<?php
		$usertype = $this->session->userdata("usertype");
		if ($usertype == "Admin" || $usertype=='Secretary') {
		    ?>
    		<h5 class="page-header">
    		    <a class="btn btn-success" href="<?php echo base_url('student/add') ?>">
    			<i class="fa fa-plus"></i>
			    <?= $this->lang->line('add_title') ?>
    		    </a>
    		</h5>
		    <?php
//		    if (!empty($students)) {
//			$stud = $students;
//			$student_status = array_shift($stud);
//			if ($student_status->academic_year_id != $current_academic_year_id) {
//			    ?>
	    		<!--<div align="center" class="alert alert-warning center-align">These students are not yet promoted to the current academic year. You need to promote these students into the current academic year</div>-->
			    <?php
//			}
//		    }
		}
		?>



                <div class="col-sm-6 col-sm-offset-3 list-group">
                    <div class="list-group-item list-group-item-warning">
                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="col-sm-2 col-sm-offset-2 control-label">
				    <?= $this->lang->line("student_classes") ?>
                                </label>
                                <div class="col-sm-6">
				    <?php
				    $array = array("0" => $this->lang->line("student_select_class"));
				    foreach ($classes as $classa) {
					$array[$classa->classesID] = $classa->classes;
				    }
				    echo form_dropdown("classesID", $array, set_value("classesID", $set), "id='classesID' class='form-control'");
				    ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

		<?php if (!empty($students)) { ?>

    		<div class="col-sm-12">

    		    <div class="nav-tabs-custom">
    			<ul class="nav nav-tabs">
    			    <li class="active"><a data-toggle="tab" href="#all" aria-expanded="true"><?= $this->lang->line("student_all_students") ?> (<?= count($students) ?>)</a></li>
				<?php
				foreach ($sections as $key => $section) {
				    echo '<li class=""><a data-toggle="tab" href="#' . $section->sectionID . '" aria-expanded="false">' . $this->lang->line("student_section") . " " . $section->section . " ( " . $section->category . " )" . '</a></li>';
				}
				?>

    			    <a href="<?= base_url('student/export/' . $set) ?>" style="float: right;" class="btn btn-success btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Export"><i class="fa fa-cloud-download"></i></a>
    			</ul>


    			<div class="tab-content">
    			    <div id="all" class="tab-pane active">
    				<div id="hide-table">
    				    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
    					<thead>
    					    <tr>
    						<th class="col-sm-2"><?= $this->lang->line('slno') ?></th>
    						<th class="col-sm-2"><?= $this->lang->line('student_photo') ?></th>
    						<th class="col-sm-2"><?= $this->lang->line('student_name') ?></th>
    						<th class="col-sm-2"><?= $this->lang->line('student_roll') ?></th>
    						<th class="col-sm-2"><?= $this->lang->line('student_dob') ?></th>
    						<th class="col-sm-2"><?= $this->lang->line('action') ?></th>
    					    </tr>
    					</thead>
    					<tbody>
						<?php
						if (!empty($students)) {
						    $i = 1;
						    foreach ($students as $student) {
							?>
	    					    <tr>
	    						<td data-title="<?= $this->lang->line('slno') ?>">
	    <?php echo $i; ?>
	    						</td>

	    						<td data-title="<?= $this->lang->line('student_photo') ?>">
								<?php
								$array = array(
								    "src" => base_url('uploads/images/' . $student->photo),
								    'width' => '35px',
								    'height' => '35px',
								    'class' => 'img-rounded'
								);
								echo img($array);
								?>
	    						</td>
	    						<td data-title="<?= $this->lang->line('student_name') ?>">
								<?php echo $student->name; ?>
	    						</td>
	    						<td data-title="<?= $this->lang->line('student_roll') ?>">
								<?php echo $student->roll; ?>
	    						</td>
	    						<td data-title="<?= $this->lang->line('student_phone') ?>">
								<?php echo date('d M Y', strtotime($student->dob)); ?>
	    						</td>
	    						<td data-title="<?= $this->lang->line('action') ?>">
								<?php
								if ($usertype == "Admin") {
								    echo btn_view('student/view/' . $student->studentID . "/" . $set, $this->lang->line('view'));
								    echo btn_edit('student/edit/' . $student->studentID . "/" . $set, $this->lang->line('edit'));
								    //echo btn_delete('student/delete/'.$student->studentID."/".$set, $this->lang->line('delete'),'modal','#delete-student');
								    echo anchor('student/deleteView/' . $student->studentID . "/" . $set, "<i class='fa fa-trash-o'></i>", "class='btn btn-danger btn-xs mrg' data-placement='top' data-toggle='tooltip' data-original-title='" . $this->lang->line('delete') . "'");
								} elseif ($usertype == "Teacher") {
								    echo btn_view('student/view/' . $student->studentID . "/" . $set, $this->lang->line('view'));
								}
								?>
	    						</td>
	    					    </tr>
							<?php
							$i++;
						    }
						}
						?>
    					</tbody>
    				    </table>
    				</div>

    			    </div>

    <?php foreach ($sections as $key => $section) { ?>
				    <div id="<?= $section->sectionID ?>" class="tab-pane">
					<div id="hide-table">
					    <table class="table table-striped table-bordered table-hover dataTable no-footer">
						<thead>
						    <tr>
							<th class="col-sm-2"><?= $this->lang->line('slno') ?></th>
							<th class="col-sm-2"><?= $this->lang->line('student_photo') ?></th>
							<th class="col-sm-2"><?= $this->lang->line('student_name') ?></th>
							<th class="col-sm-2"><?= $this->lang->line('student_roll') ?></th>
							<th class="col-sm-2"><?= $this->lang->line('student_dob') ?></th>
							<th class="col-sm-2"><?= $this->lang->line('action') ?></th>
						    </tr>
						</thead>
						<tbody>
							    <?php
							    if (count($allsection[$section->section])) {
								$i = 1;
								foreach ($allsection[$section->section] as $student) {
								    ?>
							    <tr>
								<td data-title="<?= $this->lang->line('slno') ?>">
								    <?php echo $i; ?>
								</td>

								<td data-title="<?= $this->lang->line('student_photo') ?>">
								    <?php
								    $array = array(
									"src" => base_url('uploads/images/' . $student->photo),
									'width' => '35px',
									'height' => '35px',
									'class' => 'img-rounded'
								    );
								    echo img($array);
								    ?>
								</td>
								<td data-title="<?= $this->lang->line('student_name') ?>">
								    <?php echo $student->name; ?>
								</td>
								<td data-title="<?= $this->lang->line('student_roll') ?>">
								    <?php echo $student->roll; ?>
								</td>
								<td data-title="<?= $this->lang->line('student_phone') ?>">
								    <?php echo date('d M Y', strtotime($student->dob)); ?>
								</td>
								<td data-title="<?= $this->lang->line('action') ?>">
								    <?php
								    if ($usertype == "Admin") {
									echo btn_view('student/view/' . $student->studentID . "/" . $set, $this->lang->line('view'));
									echo btn_edit('student/edit/' . $student->studentID . "/" . $set, $this->lang->line('edit'));

									echo anchor('student/deleteView/' . $student->studentID . "/" . $set, "<i class='fa fa-trash-o'></i>", "class='btn btn-danger btn-xs mrg' data-placement='top' data-toggle='tooltip' data-original-title='" . $this->lang->line('delete') . "'");
								    } elseif ($usertype == "Teacher") {
									echo btn_view('student/view/' . $student->studentID . "/" . $set, $this->lang->line('view'));
								    }
								    ?>
								</td>
							    </tr>
					    <?php
					    $i++;
					}
				    }
				    ?>
						</tbody>
					    </table>
					</div>
				    </div>
    <?php } ?>
    			</div>

    		    </div> <!-- nav-tabs-custom -->
    		</div> <!-- col-sm-12 for tab -->

<?php } else { ?>
    		<div class="col-sm-12">

    		    <div class="nav-tabs-custom">
    			<ul class="nav nav-tabs">
    			    <li class="active"><a data-toggle="tab" href="#all" aria-expanded="true"><?= $this->lang->line("student_all_students") ?></a></li>
    			</ul>


    			<div class="tab-content">
    			    <div id="all" class="tab-pane active">
    				<div id="hide-table">
    				    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
    					<thead>
    					    <tr>
    						<th class="col-sm-2"><?= $this->lang->line('slno') ?></th>
    						<th class="col-sm-2"><?= $this->lang->line('student_photo') ?></th>
    						<th class="col-sm-2"><?= $this->lang->line('student_name') ?></th>
    						<th class="col-sm-2"><?= $this->lang->line('student_roll') ?></th>
    						<th class="col-sm-2"><?= $this->lang->line('student_phone') ?></th>
    						<th class="col-sm-2"><?= $this->lang->line('action') ?></th>
    					    </tr>
    					</thead>
    					<tbody>
							<?php
							if (count($students)) {
							    $i = 1;
							    foreach ($students as $student) {
								?>
	    					    <tr>
	    						<td data-title="<?= $this->lang->line('slno') ?>">
								<?php echo $i; ?>
	    						</td>

	    						<td data-title="<?= $this->lang->line('student_photo') ?>">
								<?php
								$array = array(
								    "src" => base_url('uploads/images/' . $student->photo),
								    'width' => '35px',
								    'height' => '35px',
								    'class' => 'img-rounded'
								);
								echo img($array);
								?>
	    						</td>
	    						<td data-title="<?= $this->lang->line('student_name') ?>">
								<?php echo $student->name; ?>
	    						</td>
	    						<td data-title="<?= $this->lang->line('student_roll') ?>">
								<?php echo $student->roll; ?>
	    						</td>
	    						<td data-title="<?= $this->lang->line('student_phone') ?>">
								<?php echo $student->phone; ?>
	    						</td>
	    						<td data-title="<?= $this->lang->line('action') ?>">
	    <?php
	    if ($usertype == "Admin") {
		echo btn_view('student/view/' . $student->studentID . "/" . $set, $this->lang->line('view'));
		echo btn_edit('student/edit/' . $student->studentID . "/" . $set, $this->lang->line('edit'));

		echo anchor('student/deleteView/' . $student->studentID . "/" . $set, "<i class='fa fa-trash-o'></i>", "class='btn btn-danger btn-xs mrg' data-placement='top' data-toggle='tooltip' data-original-title='" . $this->lang->line('delete') . "'");
	    } elseif ($usertype == "Teacher") {
		echo btn_view('student/view/' . $student->studentID . "/" . $set, $this->lang->line('view'));
	    }
	    ?>
	    						</td>
	    					    </tr>
			    <?php
			    $i++;
			}
		    }
		    ?>
    					</tbody>
    				    </table>
    				</div>

    			    </div>
    			</div>
    		    </div> <!-- nav-tabs-custom -->
    		</div>
<?php } ?>

            </div> <!-- col-sm-12 -->

        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->


<!-- email modal starts here -->
<form class="form-horizontal" role="form" action="<?= base_url('student/send_mail'); ?>" method="post">
    <div class="modal fade" id="delete-student">
	<div class="modal-dialog">
	    <div class="modal-content">
		<div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		    <h4 class="modal-title"><?= $this->lang->line('mail') ?></h4>
		</div>
		<div class="modal-body">

<?php
if (form_error('to'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
                    <label for="to" class="col-sm-2 control-label">
		<?= $this->lang->line("to") ?>
                    </label>
                    <div class="col-sm-6">
                        <input type="email" class="form-control" id="to" name="to" value="<?= set_value('to') ?>" >
                    </div>
                    <span class="col-sm-4 control-label" id="to_error">
                    </span>
                </div>

<?php
if (form_error('subject'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
		<label for="subject" class="col-sm-2 control-label">
	    <?= $this->lang->line("subject") ?>
		</label>
		<div class="col-sm-6">
		    <input type="text" class="form-control" id="subject" name="subject" value="<?= set_value('subject') ?>" >
		</div>
		<span class="col-sm-4 control-label" id="subject_error">
		</span>

	    </div>

<?php
if (form_error('message'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
	    <label for="message" class="col-sm-2 control-label">
<?= $this->lang->line("message") ?>
	    </label>
	    <div class="col-sm-6">
		<textarea class="form-control" id="message" style="resize: vertical;" name="message" value="<?= set_value('message') ?>" ></textarea>
	    </div>
	</div>


    </div>
    <div class="modal-footer">
	<button type="button" class="btn btn-default" style="margin-bottom:0px;" data-dismiss="modal"><?= $this->lang->line('close') ?></button>
	<input type="button" id="send_pdf" class="btn btn-success" value="<?= $this->lang->line("send") ?>" />
    </div>
</div>
</div>
</div>
</form>

<script type="text/javascript">
    $('#classesID').change(function () {
	var classesID = $(this).val();
	if (classesID == 0) {
	    $('#hide-table').hide();
	    $('.nav-tabs-custom').hide();
	} else {
	    $.ajax({
		type: 'POST',
		url: "<?= base_url('student/student_list') ?>",
		data: "id=" + classesID,
		dataType: "html",
		success: function (data) {
		    window.location.href = data;
		}
	    });
	}
    });
</script>