<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
    <!-- jQuery Smart Wizard -->
    <script src="<?=  base_url()?>/assets/js/jquery.smartWizard.js"></script>

        <!-- page content --> 
        <div class="">

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Student <small>Admission</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                      <form class="form-horizontal"  role="form" enctype="multipart/form-data" action="<?= current_url()?>"  method="POST">
                    <!-- Smart Wizard -->
                    <p>Students Information registration</p>
                    <div id="wizard" class="form_wizard wizard_horizontal">
                      <ul class="wizard_steps">
                        <li>
                          <a href="#step-1">
                            <span class="step_no">1</span>
                            <span class="step_descr">
                                              <small>Step 1 </small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-2">
                            <span class="step_no">2</span>
                            <span class="step_descr">
                                              <small>Step 2 </small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-3">
                            <span class="step_no">3</span>
                            <span class="step_descr">
                                              <small>Step 3 </small>
                                          </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-4">
                            <span class="step_no">4</span>
                            <span class="step_descr">
                                              <small>Step 4 </small>
                                          </span>
                          </a>
                        </li>
                      </ul>
                       
                      <div id="step-1">              
                   <h2 class="StepTitle">Step 2 Content</h2>      
           <?php
                        if(form_error('name')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="name_id" class="col-sm-2 control-label">
                            <?=$this->lang->line("student_name")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="name_id" name="name" value="<?=set_value('name', $student->name)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('name'); ?>
                        </span>
                    </div>

	    
	       <?php
	    if (form_error('nationality'))
		echo "<div class='form-group has-error' >";
	    else
		echo "<div class='form-group' >";
	    ?>
	    <label for="name_id" class="col-sm-2 control-label">
		<?= $this->lang->line("student_nationality") ?> *
	    </label>
	    <div class="col-sm-6">
		    <?php
	$array = array('255' =>'Tanzania (United Republic of)');
	$countries=  getCountries();
	foreach ($countries as $key=>$country) {
		$array[$country] = $country;
	}

    echo form_dropdown("nationality", $array, set_value("nationality"), "id='nationality_id' class='form-control guargianID'");
    ?>
	    </div>
	    <span class="col-sm-4 control-label">
		<?php echo form_error('nationality'); ?>
	    </span>
	</div> 
	
                    <?php 
                        if(form_error('guargianID')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="guargianID" class="col-sm-2 control-label">
                            <?=$this->lang->line("student_guargian")?>
                        </label>
                        <div class="col-sm-6">
                            <div class="select2-wrapper">
                                <?php
                                    $array = array('' => '');
                                    foreach ($parents as $parent) {
                                        $array[$parent->parentID] = $parent->name." (" . $parent->email ." )";
                                    }
                                    echo form_dropdown("guargianID", $array, set_value("guargianID", $student->parentID), "id='guargianID' class='form-control guargianID'");
                                ?>
                            </div>
                        </div>
                        <span class="col-sm-4 control-label">
                        <?php echo form_error('guargianID');?>
                        </span>
                    </div>
            <?php
		    if (form_error('nationality'))
			echo "<div class='form-group has-error' >";
		    else
			echo "<div class='form-group' >";
		    ?>
		    <label for="name_id" class="col-sm-2 control-label">
			<?= $this->lang->line("student_nationality") ?> *
		    </label>
		    <div class="col-sm-6">
			<input type="text" class="form-control" id="national_id" name="nationality" value="<?= set_value('nationality',$student->nationality) ?>">
		    </div>
		    <span class="col-sm-4 control-label">
			<?php echo form_error('nationality'); ?>
		    </span>
	    </div> 

                    <?php 
                        if(form_error('dob')) 
                            echo "<div class='form-group has-error'>";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="dob" class="col-sm-2 control-label">
                            <?=$this->lang->line("student_dob")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="dob" name="dob" value="<?=set_value('dob', date("m-d-Y", strtotime($student->dob)))?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('dob'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('sex')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="sex" class="col-sm-2 control-label">
                            <?=$this->lang->line("student_sex")?>
                        </label>
                        <div class="col-sm-6">
                            <?php 
                                echo form_dropdown("sex", array($this->lang->line('student_sex_male') => $this->lang->line('student_sex_male'), $this->lang->line('student_sex_female') => $this->lang->line('student_sex_female')), set_value("sex", $student->sex), "id='sex' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('sex'); ?>
                        </span>

                    </div>

                   
                    <?php 
                        if(form_error('religion')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="religion" class="col-sm-2 control-label">
                            <?=$this->lang->line("student_religion")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="religion" name="religion" value="<?=set_value('religion', $student->religion)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('religion'); ?>
                        </span>
                    </div>


                    <?php 
                        if(form_error('classesID')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="classesID" class="col-sm-2 control-label">
                            <?=$this->lang->line("student_classes")?>
                        </label>
                        <div class="col-sm-6">
                           <?php
                                $array = array(0 => $this->lang->line("student_select_class"));
                                foreach ($classes as $classa) {
                                    $array[$classa->classesID] = $classa->classes;
                                }
                                echo form_dropdown("classesID", $array, set_value("classesID", $student->classesID), "id='classesID' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('classesID'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('sectionID')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="sectionID" class="col-sm-2 control-label">
                            <?=$this->lang->line("student_section")?>
                        </label>
                        <div class="col-sm-6">
                           <?php
                                $array = array(0 => $this->lang->line("student_select_section"));
                                foreach ($sections as $section) {
                                    $array[$section->sectionID] = $section->section;
                                }
                                echo form_dropdown("sectionID", $array, set_value("sectionID", $student->sectionID), "id='sectionID' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('sectionID'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('roll')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="roll" class="col-sm-2 control-label">
                            <?=$this->lang->line("student_roll")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="roll" name="roll" value="<?=set_value('roll', $student->roll)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('roll'); ?>
                        </span>
                    </div>

                    <?php 
                        if(isset($image)) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="photo" class="col-sm-2 control-label col-xs-8 col-md-2">
                            <?=$this->lang->line("student_photo")?>
                        </label>
                        <div class="col-sm-4 col-xs-6 col-md-4">
                            <input class="form-control"  id="uploadFile" placeholder="Choose File" disabled />  
                        </div>

                        <div class="col-sm-2 col-xs-6 col-md-2">
                            <div class="fileUpload btn btn-success form-control">
                                <span class="fa fa-repeat"></span>
                                <span><?=$this->lang->line("upload")?></span>
                                <input id="uploadBtn" type="file" class="upload" name="image" />
                            </div>
                        </div>
                         <span class="col-sm-4 control-label col-xs-6 col-md-4">
                           
                            <?php if(isset($image)) echo $image; ?>
                        </span>
                    </div>                
                          
                          
                         
        
         </div>
<!--end step one-->
                            
                      <div id="step-2">
                        <h2 class="StepTitle">Step 2 Content</h2>
<?php
		if (form_error('health'))
			echo "<div class='form-group has-error' >";
		else
			echo "<div class='form-group' >";
		?>
		<label for="health" class="col-sm-2 control-label">
			<?= $this->lang->line("student_health") ?>
		</label>
	  <div class="col-sm-6">
                            <input type="text" class="form-control" id="health" name="health" value="<?=set_value('religion', $student->health)?>" >
                        </div>
	<span class="col-sm-4 control-label">
	    <?php echo form_error('health'); ?>
	</span>
	</div>
 	<?php
		if (form_error('health_other'))
			echo "<div class='form-group has-error hidden' id='health_other' >";
		else
			echo "<div class='form-group' >";
		?>
		<label for="health" class="col-sm-2 control-label">
			<?= $this->lang->line("other_health_problem") ?>
		</label>
		<div class="col-sm-6">
	
                    <textarea type="text"  class="form-control" id="health_other" name="health_other"  value="<?= set_value('health_other', $student->health_other)?>"><?= set_value('health_other', $student->health_other)?></textarea>
		</div>
	<span class="col-sm-4 control-label">
	    <?php echo form_error('health_other'); ?>
	</span>
	</div>


                    <?php 
                        if(form_error('email')) 
                            echo "<div class='form-group has-error'>";
                        else     
                            echo "<div class='form-group'>";
                    ?>
                        <label for="email" class="col-sm-2 control-label">
                            <?=$this->lang->line("student_email")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="email" name="email" value="<?=set_value('email', $student->email)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('email');?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('phone')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="phone" class="col-sm-2 control-label">
                            <?=$this->lang->line("student_phone")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="phone" name="phone" value="<?=set_value('phone', $student->phone)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('phone'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('address')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="address" class="col-sm-2 control-label">
                            <?=$this->lang->line("student_address")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="address" name="address" value="<?=set_value('address', $student->address)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('address'); ?>
                        </span>
                    </div>
                     
                     
                      </div>
                      <div id="step-3">
                        <h2 class="StepTitle">Step 3 Content</h2>
                         
<?php
if (form_error('admitted_from'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="email" class="col-sm-2 control-label">
    <?= $this->lang->line("student_admitted_from") ?>
</label>
<div class="col-sm-6">
    <input type="text" class="form-control" id="admitted_from" name="admitted_from" value="<?= set_value('admitted_from',$student->admitted_from) ?>" >
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('admitted_from'); ?>
</span>
</div>
                      
                      <?php
if (form_error('year_finished'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="email" class="col-sm-2 control-label">
    <?= $this->lang->line("student_year_finished") ?>
</label>
<div class="col-sm-6">
    <input type="text" class="form-control" id="year_finished" name="year_finished" value="<?= set_value('year_finished',$student->year_finished) ?>" >
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('year_finished'); ?>
</span>
</div>

<?php
if (form_error('region'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="phone" class="col-sm-2 control-label">
    <?= $this->lang->line("student_region") ?>
</label>
<div class="col-sm-6">
    <input type="text" class="form-control" id="region" name="region" value="<?= set_value('region',$student->region) ?>" >
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('region'); ?>
</span>
</div>

<?php
if (form_error('reg_number'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="address" class="col-sm-2 control-label">
    <?= $this->lang->line("student_reg_number") ?> *
</label>
<div class="col-sm-6">
    <input type="text" class="form-control" id="reg_number" name="reg_number" value="<?= set_value('reg_number',$student->reg_number) ?>" >
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('reg_number'); ?> *
</span>
    </div>
                      </div>
                      <div id="step-4">
                        <h2 class="StepTitle">Step 4 Content</h2>
                      

                          <?php
		    if (form_error('division'))
			echo "<div class='form-group has-error' >";
		    else
			echo "<div class='form-group' >";
		    ?>
		    <label for="name_id" class="col-sm-2 control-label">
			<?= $this->lang->line("student_division") ?> *
		    </label>
		    <div class="col-sm-6">
			<input type="text" class="form-control" id="division" name="division" value="<?= set_value('division',$student->division) ?>" >
		    </div>
		    <span class="col-sm-4 control-label">
			<?php echo form_error('division'); ?>
		    </span>
	    </div>
		    <?php
		    if (form_error('point'))
			echo "<div class='form-group has-error' >";
		    else
			echo "<div class='form-group' >";
		    ?>
		    <label for="name_id" class="col-sm-2 control-label">
			<?= $this->lang->line("student_point") ?> *
		    </label>
		    <div class="col-sm-6">
			<input type="text" class="form-control" id="national_id" name="point" value="<?= set_value('point',$student->point) ?>">
		    </div>
		    <span class="col-sm-4 control-label">
			<?php echo form_error('point'); ?>
		    </span>
	    </div>
                     	<?php
		if (form_error('math'))
			echo "<div class='form-group has-error' >";
		else
			echo "<div class='form-group' >";
		?>
		<label for="dob" class="col-sm-2 control-label">
			<?= $this->lang->line("student_math") ?>
		</label>
		<div class="col-sm-6">
	<input type="text" class="form-control" id="math" name="math" value="<?= set_value('math',$student->math) ?>" >
		</div>
	<span class="col-sm-4 control-label">
	    <?php echo form_error('math'); ?>
	</span>
	</div> 
        
                          <?php
		    if (form_error('physics'))
			echo "<div class='form-group has-error' >";
		    else
			echo "<div class='form-group' >";
		    ?>
		    <label for="name_id" class="col-sm-2 control-label">
			<?= $this->lang->line("student_physics") ?> *
		    </label>
		    <div class="col-sm-6">
			<input type="text" class="form-control" id="physics" name="physics" value="<?= set_value('physics',$student->physics) ?>" >
		    </div>
		    <span class="col-sm-4 control-label">
			<?php echo form_error('physics'); ?>
		    </span>
	    </div>
		    <?php
		    if (form_error('chemistry'))
			echo "<div class='form-group has-error' >";
		    else
			echo "<div class='form-group' >";
		    ?>
		    <label for="name_id" class="col-sm-2 control-label">
			<?= $this->lang->line("student_chemistry") ?> *
		    </label>
		    <div class="col-sm-6">
			<input type="text" class="form-control" id="chemistry" name="chemistry" value="<?= set_value('nationality',$student->chemistry) ?>">
		    </div>
		    <span class="col-sm-4 control-label">
			<?php echo form_error('chemistry'); ?>
		    </span>
	    </div>
                     	<?php
		if (form_error('biology'))
			echo "<div class='form-group has-error' >";
		else
			echo "<div class='form-group' >";
		?>
		<label for="biology" class="col-sm-2 control-label">
			<?= $this->lang->line("student_biology") ?>
		</label>
		<div class="col-sm-6">
	<input type="text" class="form-control" id="biology" name="biology" value="<?= set_value('biology',$student->biology) ?>" >
		</div>
	<span class="col-sm-4 control-label">
	    <?php echo form_error('biology'); ?>
	</span>
	</div> 
               	<?php
		if (form_error('geograph'))
			echo "<div class='form-group has-error' >";
		else
			echo "<div class='form-group' >";
		?>
		<label for="biology" class="col-sm-2 control-label">
			<?= $this->lang->line("student_geograph") ?>
		</label>
		<div class="col-sm-6">
	<input type="text" class="form-control" id="geograph" name="geograph" value="<?= set_value('geograph',$student->geography) ?>" >
		</div>
	<span class="col-sm-4 control-label">
	    <?php echo form_error('geograph'); ?>
	</span>
	</div> 
               	<?php
		if (form_error('history'))
			echo "<div class='form-group has-error' >";
		else
			echo "<div class='form-group' >";
		?>
		<label for="biology" class="col-sm-2 control-label">
			<?= $this->lang->line("student_history") ?>
		</label>
		<div class="col-sm-6">
	<input type="text" class="form-control" id="history" name="history" value="<?= set_value('history',$student->history) ?>" >
		</div>
	<span class="col-sm-4 control-label">
	    <?php echo form_error('history'); ?>
	</span>
	</div> 
               	<?php
		if (form_error('kiswahili'))
			echo "<div class='form-group has-error' >";
		else
			echo "<div class='form-group' >";
		?>
		<label for="kiswahili" class="col-sm-2 control-label">
			<?= $this->lang->line("student_kiswahili") ?>
		</label>
		<div class="col-sm-6">
	<input type="text" class="form-control" id="biology" name="kiswahili" value="<?= set_value('kiswahili',$student->kiswahili) ?>" >
		</div>
	<span class="col-sm-4 control-label">
	    <?php echo form_error('kiswahili'); ?>
	</span>
	</div> 
               	<?php
		if (form_error('english'))
			echo "<div class='form-group has-error' >";
		else
			echo "<div class='form-group' >";
		?>
		<label for="english" class="col-sm-2 control-label">
			<?= $this->lang->line("student_english") ?>
		</label>
		<div class="col-sm-6">
	<input type="text" class="form-control" id="english" name="english" value="<?= set_value('english',$student->english) ?>" >
		</div>
	<span class="col-sm-4 control-label">
	    <?php echo form_error('english'); ?>
	</span>
	</div> 
               	<?php
		if (form_error('civics'))
			echo "<div class='form-group has-error' >";
		else
			echo "<div class='form-group' >";
		?>
		<label for="civics" class="col-sm-2 control-label">
			<?= $this->lang->line("student_civics") ?>
		</label>
		<div class="col-sm-6">
	<input type="text" class="form-control" id="civics" name="civics" value="<?= set_value('civics',$student->civics) ?>" >
		</div>
	<span class="col-sm-4 control-label">
	    <?php echo form_error('civics'); ?>
	</span>
	</div> 
               	<?php
		if (form_error('bk'))
			echo "<div class='form-group has-error' >";
		else
			echo "<div class='form-group' >";
		?>
		<label for="biology" class="col-sm-2 control-label">
			<?= $this->lang->line("student_bk") ?>
		</label>
		<div class="col-sm-6">
	<input type="text" class="form-control" id="bk" name="bk" value="<?= set_value('bk',$student->book_keeping) ?>" >
		</div>
	<span class="col-sm-4 control-label">
	    <?php echo form_error('bk'); ?>
	</span>
	</div> 
               	<?php
		if (form_error('commerce'))
			echo "<div class='form-group has-error' >";
		else
			echo "<div class='form-group' >";
		?>
		<label for="biology" class="col-sm-2 control-label">
			<?= $this->lang->line("student_commerce") ?>
		</label>
		<div class="col-sm-6">
	<input type="text" class="form-control" id="commerce" name="commerce" value="<?= set_value('commerce',$student->commerce) ?>" >
		</div>
	<span class="col-sm-4 control-label">
	    <?php echo form_error('commerce'); ?>
	</span>
	</div> 
               	<?php
		if (form_error('other_subject'))
			echo "<div class='form-group has-error' >";
		else
			echo "<div class='form-group' >";
		?>
		<label for="biology" class="col-sm-2 control-label">
			<?= $this->lang->line("student_other") ?>
		</label>
		<div class="col-sm-6">
                    <textarea type="text" class="form-control" id="other_subject" name="other_subject" value="<?= set_value('other_subject',$student->other_subject) ?>"></textarea>
		</div>
	<span class="col-sm-4 control-label">
	    <?php echo form_error('other_subject'); ?>
	</span>
	</div> 
                      
                      </div>
                             
                    </div>
                    <!-- End SmartWizard Content -->
                     </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <!-- /page content -->

            <!-- jQuery Smart Wizard -->
    <script>
    
       
      $(document).ready(function() {
        $('#wizard').smartWizard();
        $('.buttonNext').addClass('btn btn-success');
        $('.buttonPrevious').addClass('btn btn-primary');
        $('.buttonFinish').addClass('btn btn-success');  
      });
     

    $('#classesID').change(function (event) {
	var classesID = $(this).val();
	if (classesID === '0') {
	    $('#classesID').val(0);
	} else {
	    $.ajax({
		type: 'POST',
		url: "<?= base_url('student/sectioncall') ?>",
		data: "id=" + classesID,
		dataType: "html",
		success: function (data) {
		    $('#sectionID').html(data);
		}
	    });
	}
    });

    $(document).ready(function () {
	$('#show_password').click(function () {
	    $('#password').attr('type', $(this).is(':checked') ? 'text' : 'password');
	});

    });
    
	$('#health').change(function () {
	    var status= $('#health').val();
            if(status==='other'){
                ('#health_other').show();
            }else{
                ('#health_other').hide();  
            }
            
	});
          $("#dob").datepicker();

   
//    $(document).ready(function(){
//
//        $("#dob").click(function(){
//            $("#dob").pickadate({
//
//                selectYears: 50,
//                selectMonths: true,
//                max:new Date("2018")
//            });
//        });
//
//    });


    </script>
    <!-- /jQuery Smart Wizard -
