
<div class="box">
    <div class="box-header">
	<h3 class="box-title"><i class="fa icon-bar"></i> <?= $this->lang->line('add_title') ?></h3>

	<ol class="breadcrumb">
	    <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
	    <li><a href="<?= base_url("inventory/index") ?>"><?= $this->lang->line('menu_inventory') ?></a></li>
	    <li class="active"><?= $this->lang->line('inventory_title') ?></li>
	</ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
	<div class="row">
	    <div class="col-sm-8">
		<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">

		    <?php
		    if (form_error('name'))
			echo "<div class='form-group has-error' >";
		    else
			echo "<div class='form-group' >";
		    ?>
		    <label for="name" class="col-sm-2 control-label">
			<?= $this->lang->line("name") ?> *
		    </label>
		    <div class="col-sm-6">
			<input type="text" class="form-control" id="name" name="name" value="<?= set_value('name',$item->name) ?>" >
		    </div>
		    <span class="col-sm-4 control-label">
			<?php echo form_error('name'); ?>
		    </span>
	    </div>


	    <?php
	    if (form_error('vendor_name'))
		echo "<div class='form-group has-error' >";
	    else
		echo "<div class='form-group' >";
	    ?>
	    <label for="vendor_name" class="col-sm-2 control-label">
		<?= $this->lang->line("vendor_name") ?> *
	    </label>
	    <div class="col-sm-6">
		<div class="select2-wrapper">
		    <?php
		    $array = array('' => '');
		    foreach ($vendors as $vendor) {
			$array[$vendor->vendor_id] = $vendor->name . " (" . $vendor->email . " )";
		    }
		    echo form_dropdown("vendor_id", $array, set_value("vendor_id",$item->vendor_id), "id='vendor_name' class='form-control vendor_name'");
		    ?>
		</div>
	    </div>
	    <span class="col-sm-4 control-label">
		<?php echo form_error('vendor_name'); ?>
	    </span>
	</div>



	<?php
	if (form_error('batch_number'))
	    echo "<div class='form-group has-error' >";
	else
	    echo "<div class='form-group' >";
	?>
	<label for="batch_number" class="col-sm-2 control-label">
	    <?= $this->lang->line("batch_number") ?> *
	</label>
	<div class="col-sm-6">
	    <input type="text" class="form-control" id="batch_number" name="batch_number" value="<?= set_value('batch_number',$item->batch_number) ?>" >
	</div>
	<span class="col-sm-4 control-label">
	    <?php echo form_error('batch_number'); ?>
	</span>
    </div>

    <?php
    if (form_error('status'))
	echo "<div class='form-group has-error' >";
    else
	echo "<div class='form-group' >";
    ?>
    <label for="status" class="col-sm-2 control-label">
	<?= $this->lang->line("status") ?> *
    </label>
    <div class="col-sm-6">

	<select name="status" id="status" class="form-control">
	    <option value="1">New</option>
	    <option value="2">Used</option>
	    <option value="3">Obsolete</option>
	</select>

    </div>
    <span class="col-sm-4 control-label">
	<?php echo form_error('status'); ?>
    </span>
</div>

<?php
if (form_error('date_purchased'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="date_purchased" class="col-sm-2 control-label">
    <?= $this->lang->line("date_purchased") ?>
</label>
<div class="col-sm-6">
    <input type="date" class="form-control" id="date_purchased" name="date_purchased" value="<?= set_value('date_purchased',date("m/d/Y", strtotime($student->dob))) ?>" >
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('date_purchased'); ?>
</span>
</div>

<?php
if (form_error('price'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="price" class="col-sm-2 control-label">
    <?= $this->lang->line("price") ?>
</label>
<div class="col-sm-6">
    <input type="text" class="form-control" id="price" name="price" value="<?= set_value('price',$item->price) ?>" >
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('price'); ?>
</span>
</div>

<?php
if (form_error('quantity'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="quantity" class="col-sm-2 control-label">
    <?= $this->lang->line("quantity") ?>
</label>
<div class="col-sm-6">
    <input type="number" class="form-control" id="quantity" name="quantity" value="<?= set_value('quantity',$item->quantity) ?>" >
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('quantity'); ?>
</span>
</div>

<?php
if (form_error('contact_person_number'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="contact_person_number" class="col-sm-2 control-label">
    <?= $this->lang->line("contact_person_number") ?>
</label>
<div class="col-sm-6">
    <input type="text" class="form-control" id="contact_person_number" name="contact_person_number" value="<?= set_value('contact_person_number',$item->contact_person_number) ?>" >
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('contact_person_number'); ?>
</span>
</div>

<?php
if (form_error('contact_person_name'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="contact_person_name" class="col-sm-2 control-label">
    <?= $this->lang->line("contact_person_name") ?>
</label>
<div class="col-sm-6">
    <input type="text" class="form-control" id="contact_person_number" name="contact_person_name" value="<?= set_value('contact_person_name',$item->contact_person_name) ?>" >
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('contact_person_name'); ?>
</span>
</div>

<?php
if (form_error('depreciation'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="depreciation" class="col-sm-2 control-label">
    <?= $this->lang->line("depreciation") ?> *
</label>
<div class="col-sm-6">

    <select name="depreciation" id="status" class="form-control">
	<option value="0.125">Class three -12.5%</option>
	<option value="0.25">Class two -25%</option>
	<option value="0.375">Class One - 37.5%</option>
    </select>
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('depreciation'); ?>
</span>
</div>

<?php
if (form_error('comments'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="comments" class="col-sm-2 control-label">
    <?= $this->lang->line("comments") ?>
</label>
<div class="col-sm-6">
    <textarea class="form-control" id="comments" name="comments"><?= set_value('comments',$item->comments) ?></textarea>
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('comments'); ?>
</span>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
	<input type="submit" class="btn btn-success" value="<?= $this->lang->line("update_item") ?>" >
    </div>
</div>
</form>
</div> <!-- col-sm-8 -->

</div><!-- row -->
</div><!-- Body -->
</div><!-- /.box -->

<script type="text/javascript">

    $(document).ready(function () {
	$('#show_password').click(function () {
	    $('#password').attr('type', $(this).is(':checked') ? 'text' : 'password');
	});

    });

    $(document).ready(function(){

        $("#date_purchased").click(function(){
            $("#date_purchased").pickadate({
                selectYears: 50,
                selectMonths: true,
                max:new Date()
            });
        });

    });
</script>
