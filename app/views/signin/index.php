<div class="form-box" id="login-box">
    <form method="post" class="form-horizontal">

        <!-- style="margin-top:40px;" -->

        <div class="body white-bg">
	    
	    <?php
	    if ($form_validation == "No") {
		
	    } else {
		if (count($form_validation)) {
		    echo "<div class=\"alert alert-danger alert-dismissable\">
                        <i class=\"fa fa-ban\"></i>
                        <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                        $form_validation
                    </div>";
		}
	    }
	    if ($this->session->flashdata('reset_success')) {
		$message = $this->session->flashdata('reset_success');
		echo "<div class=\"alert alert-success alert-dismissable\">
                    <i class=\"fa fa-ban\"></i>
                    <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                    $message
                </div>";
	    }
	    ?>
	    <div class="form-group">
		<label for="inputName" class="col-md-3 control-label">
		    <?=$this->lang->line('username')?>:</label>
		<div class="col-md-9">
		    <div class="input-icon right">
			<i class="fa fa-user"></i>
			<input id="inputName" type="text" placeholder="Username" name="username" class="form-control" autofocus value="<?= set_value('username') ?>"/></div>
		</div>
	    </div>
	    <div class="form-group">
		<label for="inputPassword" class="col-md-3 control-label">
		    Password:</label>
		<div class="col-md-9">
		    <div class="input-icon right">
			<i class="fa fa-lock"></i>
			<input id="inputPassword" placeholder="Password"  name="password" type="password" type="text"  class="form-control" /></div>
		</div>
	    </div>


	    <div class="form-group mbn">
		<div class="col-lg-12">
		    <div class="form-group mbn">
			<div class="col-lg-3">
			    <label>
				<input type="checkbox" value="Remember Me" name="remember">
				<span> &nbsp; Remember Me</span>
			    </label>
			</div>
			<div class="col-lg-9">
			    <button type="submit" class="btn btn-default sign-btn btn-block" value="SIGN IN" >
				Sign In</button>
			</div>

		    </div>
		   
		</div>
	    </div>

        </div>
    </form>
</div>

