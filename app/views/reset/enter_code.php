<?php

/**
 * Description of enter_code
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
?>
<form role="form" method="post" class="form-horizontal" action="<?=  base_url('reset/addcodes')?>">
			<h1 class="text-center">Complete</h1>


			<?php
			if (isset($reset_send)) {
			    $message =$reset_send;
			    echo "<div class=\"alert alert-success alert-dismissable\">
                        <i class=\"fa fa-warning\"></i>
                        <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                        $message
                    </div>";
			} else {
			    if (isset($reset_error)) {
				$message = $reset_error;
				echo "<div class=\"alert alert-danger alert-dismissable\">
	                        <i class=\"fa fa-warning\"></i>
	                        <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
	                        $message
	                    </div>";
			    }
			}
			?>
			<div class="input-field">
			    <i class="material-icons prefix"></i>
			    <input type="text" name="codes" id="inputEmail"  class="validate"/>
			    <label for="inputEmail">Enter Verification Codes</label>
			</div>

			<div>


			    <button class="btn btn-default  btn-block text-uppercase" type="submit" >Send</button>

			</div>
		    </form>