<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
	<div id="sms_parent" class="tab-pane <?php if ($sms_parent == 2) echo 'active'; ?>">
	    <br>
	    <div class="row">
		<div class="col-sm-12">
		    <form class="form-horizontal" role="form" method="post">
			<?php echo form_hidden('type', 'sms'); ?> 
			<?php
			if (form_error('sms_user'))
			    echo "<div class='form-group has-error' >";
			else
			    echo "<div class='form-group' >";
			?>
			<label for="sms_user" class="col-sm-1 control-label">
			    <?= $this->lang->line("mailandsms_users") ?>
			</label>
			<div class="col-sm-4">

			    <?php
			    $array = array('0' => $this->lang->line("mailandsms_select_user"));
			    foreach ($usertypes as $user) {
				$array[strtolower($user->type)] = $user->type;
			    }
			    echo form_dropdown("sms_user", $array, set_value("sms_user"), "id='sms_user' class='form-control'");
			    ?>
			</div>
			<span class="col-sm-4 control-label">
			    <?php echo form_error('sms_user'); ?>
			</span>
		</div>

		<?php
		if (form_error('sms_template'))
		    echo "<div class='form-group has-error' >";
		else
		    echo "<div class='form-group' >";
		?>
		<label for="sms_template" class="col-sm-1 control-label">
		    <?= $this->lang->line("mailandsms_template") ?>
		</label>
		<div class="col-sm-4" >

		    <?php
		    $array = array(
			'select' => $this->lang->line('mailandsms_select_template'),
		    );

		    if ($sms_user != "select") {
			foreach ($sms_templates as $stemplate) {
			    $array[$stemplate->mailandsmstemplateID] = $stemplate->name;
			}
		    }

		    $smID = "";
		    if ($sms_templateID == 'select') {
			$smID = 'select';
		    } else {
			$smID = $sms_templateID;
		    }

		    echo form_dropdown("sms_template", $array, set_value("sms_template", $smID), "id='sms_template' class='form-control'");
		    ?>

		</div>
		<span class="col-sm-3 control-label">
		    <?php echo form_error('sms_template'); ?>
		</span>
	    </div>

	    <input type="hidden" name="sms_getway" id="sms_getway" value="karibusms"/>
	    <?php
	    if (form_error('sms_message'))
		echo "<div class='form-group has-error' >";
	    else
		echo "<div class='form-group' >";
	    ?>
	    <label for="sms_message" class="col-sm-1 control-label">
		<?= $this->lang->line("mailandsms_message") ?>
	    </label>
	    <div class="col-sm-8">
		<textarea class="form-control" style="resize:vertical" id="sms_message" name="sms_message" ><?= set_value('sms_message') ?></textarea>
	    </div>
	    <span class="col-sm-3 control-label">
		<?php echo form_error('sms_message'); ?>
	    </span>
	</div> 
