
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-lbooks"></i> <?= $this->lang->line('panel_title') ?></h3>


        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li><a href="<?= base_url("book/index") ?>"><?= $this->lang->line('menu_books') ?></a></li>
            <li class="active"><?= $this->lang->line('menu_add') ?> <?= $this->lang->line('menu_books') ?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">
		<form class="form-horizontal" role="form" method="post">
		    <?php
		    if (form_error('book'))
			echo "<div class='form-group has-error' >";
		    else
			echo "<div class='form-group' >";
		    ?>
		    <label for="book" class="col-sm-2 control-label">
			<?= $this->lang->line("book_name") ?>
		    </label>
		    <div class="col-sm-6">
			<input type="text" class="form-control" id="book" name="book" value="<?= set_value('book') ?>" >
		    </div>
		    <span class="col-sm-4 control-label">
			<?php echo form_error('book'); ?>
		    </span>
	    </div>

	    <?php
	    if (form_error('author'))
		echo "<div class='form-group has-error' >";
	    else
		echo "<div class='form-group' >";
	    ?>
	    <label for="author" class="col-sm-2 control-label">
		<?= $this->lang->line("book_author") ?>
	    </label>
	    <div class="col-sm-6">
		<input type="text" class="form-control" id="author" name="author" value="<?= set_value('author') ?>" >
	    </div>
	    <span class="col-sm-4 control-label">
		<?php echo form_error('author'); ?>
	    </span>
	</div>


	<?php
	if (form_error('book_edition'))
	    echo "<div class='form-group has-error' >";
	else
	    echo "<div class='form-group' >";
	?>
	<label for="price" class="col-sm-2 control-label">
	    <?= $this->lang->line("book_edition") ?>
	</label>
	<div class="col-sm-6">
	    <input type="text" class="form-control" id="price" name="edition" value="<?= set_value('edition') ?>" >
	</div>
	<span class="col-sm-4 control-label">
	    <?php echo form_error('book_edition'); ?>
	</span>
    </div>
    <?php
    if (form_error('book_class'))
	echo "<div class='form-group has-error' >";
    else
	echo "<div class='form-group' >";
    ?>
    <label for="book_class" class="col-sm-2 control-label">
	<?= $this->lang->line("book_class") ?>
    </label>
    <div class="col-sm-6">
	<!--<select class="form-control"  multiple="multiple" name="classes[]">-->
	<?php foreach ($classes as $class) { ?>
    	<input type="checkbox" name="classes[]" value="<?= $class->classesID ?>"><?= $class->classes ?>
	<?php } ?>
	<!--</select>-->
    </div>
    <span class="col-sm-4 control-label">
	<?php echo form_error('book_class'); ?>
    </span>
</div>


<?php
if (form_error('book_subject'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="book_subject" class="col-sm-2 control-label">
    <?= $this->lang->line("book_subject") ?>
</label>
<div class="col-sm-6">
    <select name="subject" class="form-control" >

	<?php foreach ($subjects as $subject) {
	    ?>
    	<option value="<?= $subject->subject ?>" ><?= ucwords($subject->subject) ?></option>
	<?php } ?>
	<option value="<?= $subject->subject ?>" ><?= ucwords($subject->subject) ?></option>
    </select>
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('book_subject'); ?>
</span>
</div>

<?php
if (form_error('book_for'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="book_subject" class="col-sm-2 control-label">
    <?= $this->lang->line("book_for") ?>
</label>
<div class="col-sm-6">
    <select name="book_for" class="form-control" >
	<option value="teachers" >Teachers</option>
	<option value="students" >Students</option>
	<option value="both" >Both</option>
    </select>
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('book_for'); ?>
</span>
</div>

<?php
if (form_error('quantity'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="quantity" class="col-sm-2 control-label">
    <?= $this->lang->line("book_quantity") ?>
</label>
<div class="col-sm-6">
    <input type="text" class="form-control" id="quantity" name="quantity" value="<?= set_value('quantity') ?>" >
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('quantity'); ?>
</span>
</div>

<?php
if (form_error('rack'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="rack" class="col-sm-2 control-label">
    <?= $this->lang->line("book_rack_no") ?>
</label>
<div class="col-sm-6">
    <input type="text" class="form-control" id="rack" name="rack" value="<?= set_value('rack') ?>" >
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('rack'); ?>
</span>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
	<input type="submit" class="btn btn-success" value="<?= $this->lang->line("add_book") ?>" >
    </div>
</div>
</form>

</div>
<div class="col-sm-4"> 

    <div class="x_panel">
	<div class="x_title">
	    <h2>Upload Books From Excel file</h2>
	    
	    <div class="clearfix"></div>
	
	</div>
	<p>Download <a href="<?=base_url('uploads/sample/sample_book_upload.xlsx')?>">Sample File here</a></p>
	<form id="demo-form2" action="<?= base_url('book/uploadByFile') ?>" class="form-horizontal" method="POST" enctype="multipart/form-data">

	    <div class="form-group">

		<div class="col-md-6 col-sm-6 col-xs-12">
		    <input id="file" name="file"  type="file" required="required" accept=".xls,.xlsx,.csv,.odt">
		</div>
	    </div>
	    <div class="ln_solid"></div>
	    <div class="form-group">
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
		    <button type="submit" class="btn btn-success">Submit</button>
		</div>
	    </div>

	</form>
    </div>
</div>
</div>
</div>
</div>
