<?php
/**
 * @author Ephraim Swilla <ephraim@inetstz.com>
 */
$school = $this->session->CI->data['siteinfos'];
$usertype = $this->session->userdata['usertype'];
$sitephoto = $this->session->userdata['photo'];
?>
   <div class="well">
      <div class="row">

	    	<div class="col-sm-6">
	    	    <button class="btn-default btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> <?= $this->lang->line('print') ?> </button>
	
		  
	    	</div>

	    	<div class="col-sm-6">
	    	    <ol class="breadcrumb">
	    		<li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
	    		<li><a href="<?= base_url("invoice/payment_history") ?>"><?= $this->lang->line('payment') ?></a></li>
	    		<li class="active"><?= $this->lang->line('view') ?></li>
	    	    </ol>
	    	</div>
	        </div>
	    </div>

	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

	
   
    <div id="printablediv">  
        <?php 
           foreach ($receipt as $value) {?>
	<section class="content invoice" >
	    <div class="row text-center">
		<style type="text/css">

	    TABLE {

		padding:5px;

		font-family:Arial, Helvetica, sans-serif;

		font-size:11px;

		height:auto;

		padding-bottom:20px;

	    }

	    TABLE LABEL {

		display:block;

		width:250px;

		float:left

	    }

	    TABLE LABEL SPAN {

		font-style:italic;

	    }

	    TABLE SPAN {

		display:block;

		vertical-align:text-bottom

	    }


	    #Table_01{
		margin: 0 auto;
	    }
	</style>
      
     
            
     

		  <table id="Table_01" height="700" cellpadding="0" cellspacing="0">

			<tr>

			    <td width="600"  height="100" valign="top" align="right">

				<table width="700" height="70" border="0" cellpadding="0" cellspacing="0">
				    <tr>
					<td width="200">
					    <img src="<?= base_url('uploads/images/' . $school->photo) ?>" alt="" width="180" height="140" />
					</td>

					<td align="right">
					    <div style="font-family:tahoma; font-size:10px; display:block; padding-left:60px">

						<b><?= $school->sname ?></b><br>
						<b><?= $school->address ?></b><br>
						<b><?= $school->box ?></b>.<br>

						<b>Simu:</b> <?= $school->phone ?>.&nbsp;&nbsp;<br>

						<b>Barua pepe:</b> <?= $school->email ?><br>

						<b>Tovuti:</b> www.<?= strtolower($school->sname) ?>.shulesoft.com

					    </div></td>



				    </tr>
				</table>
			    </td>
			</tr>
			<tr>
			    <td width="600"  colspan="2" valign="top" height="80" align="right"><br/> 
				Receipt No: <b><?= $value->receiptID ?></b><br/>
				Reference Number:  <b><?= $value->code ?></b></td>
			</tr>
			<tr>

			    <td width="600"  colspan="2" valign="top" align="left">



				<table width="500" ><tr>

					<td colspan="3">

					    <label>

						NIMEPOKEA KWA<BR>

						<span>Received from</span>

					    </label>

					    <span style="padding-left:200px; font-weight:bold; text-transform:uppercase">
						<?= $value->student_name ?></span>

					    <span>..................................................................................................................................................................................................................................



					    </span>

					</td></tr>

				    <tr><td colspan="3">



					    <label>

						KIASI CHA SHILINGI(KWA MANENO)<BR>

						<span>Sum of shillings(words)</span>

					    </label>

					    <span style="padding-left:200px; font-weight:bold; text-transform:uppercase">
						<?= number_to_words($value->paymentamount) ?>  SHILLINGS ONLY</span>

					    <span>..................................................................................................................................................................................................................................

					    </span>



					</td></tr>

				    <tr><td colspan="3">



					    <label>

						KWA MALIPO YA <BR>

						<span>In respect of</span>

					    </label>

					    <span style="padding-left:200px; font-weight:bold; text-transform:uppercase">
						<?= $value->feetype ?></span>

					    <span>..................................................................................................................................................................................................................................

					    </span>



					</td></tr>

				    <tr><td colspan="3">



					    <label>

						KWA FEDHA TASLIMU/HUNDI NAMBA <BR>

						<span>By cash/cheque No.</span>

					    </label>

					    <span style="padding-left:200px; font-weight:bold; text-transform:uppercase">
						<?= $value->paymenttype . ' - ' . $value->transactionID ?></span>

					    <span>..................................................................................................................................................................................................................................

					    </span>
					</td></tr>


				    <tr><td width="300">


					</td>

					<td width="200">

					    CHEO<br>

					    <font style="font-style:italic">Title</font>

					    <span style="text-decoration:underline; text-transform:uppercase; font-weight:bold; padding-top:10px; display:block">ACCOUNTANT</span>

					</td>

					<td width="200">

					    TAREHE<br>

					    <font style="font-style:italic">Date</font>

					    <span style="text-decoration:underline; text-transform:uppercase; font-weight:bold; display:block; padding-top:10px"><?=date('d M Y h:m',  strtotime($value->created_at)) ?></span>

					</td></tr>



				</table>



			    </td>

			</tr>

			<tr>

			    <td colspan="2">
			    </td>

			</tr>

		    </table>
		 
	    </div><!-- /.row -->

	    <!-- this row will not appear when printing -->
	</section><!-- /.content -->
        <br><br>
         <?php }?>
    </div>

<script type="text/javascript">
     function printDiv(divID) {
		//Get the HTML of div
		var divElements = document.getElementById(divID).innerHTML;
		//Get the HTML of whole page
		var oldPage = document.body.innerHTML;

		//Reset the page's HTML with div's HTML only
		document.body.innerHTML =
			"<html><head><title></title></head><body>" +
			divElements + "</body>";

		//Print Page
		window.print();

		//Restore orignal HTML
		document.body.innerHTML = oldPage;
	    }
	    function closeWindow() {
		location.reload();
	    }
</script>