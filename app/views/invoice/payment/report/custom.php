<div class="page-header">
    <h3>Payment Custom Report</h3><small>
	Select what payment report do want to display here</small>
</div>
<form id="form" role="form" novalidate="novalidate"  enctype="multipart/form-data" autocomplete="off">
    <div class='row'>
        <div class='col-sm-3'>
            <div class="form-group">
                <div class='col-sm-12'>
                    <label for="form-field-select-3">
			Select payment method
                    </label>
                    <select id="form-field-select-3" class="form-control search-select bn_report" onchange="setCriteria(this.value);" name = "criteria">
                        <option value="">&nbsp;</option>
                        <option value="1">CRDB</option>
                        <option value="2">NMB</option>
                        <option value="3">Tigo Pesa</option>
                        <option value="4">M-pesa</option>
                        <option value="5">Airtel-Money</option>
			<option value="6">BANK Card</option>
                    </select>
                </div>
            </div>
        </div>
        <div class='col-sm-9'>
            <div class="row">
                <div id="type" class="col-sm-3" style ='display:none'>
                    <div class='form-group'>
                        <div class='col-sm-12'>
                            <label for="type-input">
                                Type
                            </label>
                            <select id="type-input" class = "form-control search-select bn_report" name = "sub_type">
                                <option value= "">&nbsp;</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div id = "start_date" class="col-sm-3">
                    <div class='form-group'>
                        <div class='col-sm-12'>
                            <label for="lower_date-input">
                                Start Date
                            </label>
                            <input  data-date-viewmode="years" class="form-control date-picker" required="" type='text' class='form-control' name='start_date'>
                        </div>
                    </div>
                </div>

                <div id = "upper_age" class="col-sm-3">
                    <div class='form-group'>
                        <div class='col-sm-12'>
                            <label for="end_date-input">
                                End Date
                            </label>
                            <input  data-date-viewmode="years" class="form-control date-picker" required="" type='text' class='form-control' name='end_date'>
                        </div>
                    </div>
                </div>

                <div id = 'button' class="col-sm-3">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="search-button">&nbsp;
                            </label>
                            <button  id="search-button" type = "submit" class="btn btn-squared btn-blue btn-block">
                                Search <i class="fa fa-arrow-circle-right"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<hr/>
<div id = "report"></div>

<script src="<?= site_url() ?>assets/js/validator-default.js"></script>

<script type='text/javascript'>
			validatordefault.init();/* peform validations for this view*/
			var s = formatDate(new Date(1961, 0, 1)); /*01/01/1961*/
			var e = formatDate(new Date());
			$('.date-picker').datepicker({
			    autoclose: true,
			    format: 'dd/mm/yyyy',
			    startDate: s
			});
			$(".bn_report").select2({
			    placeholder: "Select Criteria",
			    allowClear: true
			});
			function setCriteria(value) {
			    switch (value) {
				case '':
				    document.getElementById('region').style.display = "none";
				    break;
				case '1':
				    /*CRDB bank*/
				    var items = [
					{'name': 'POS', 'value': 'POS'},
					{'name': 'Sim Banking', 'value': 'SIM_BANKING'},
					{'name': 'Fahari Huduma', 'value': 'FAHARI_HUDUMA'},
					{'name': 'All', 'value': 'ALL'}
				    ];
				    $('#type-input').empty();
				    $.each(items, function (i, item) {
					$('#type-input').append($('<option>', {
					    value: item.value,
					    text: item.name
					}));
				    });
				    $('#type').show();
				    break;
				case '2':
				    /*NMB bank*/
				    var nmbitems = [
					{'name': 'POS', 'value': 'POS'},
					{'name': 'NMB Mobile', 'value': 'NMB_MOBILE'},
					{'name': 'NMB Wakala', 'value': 'NMB_WAKALA'},
					{'name': 'All', 'value': 'ALL'},
				    ];
				    $('#type').show();
				    $('#type-input').empty();
				    $.each(nmbitems, function (i, item) {
					$('#type-input').append($('<option>', {
					    value: item.value,
					    text: item.name
					}));
				    });
				    break;
				case '3':
				    $('#type').hide();
				    break;
				case '4':
				    $('#type').hide();
				    break;
				case '5':
				    $('#type').hide();
				    break;
				case '6':
				    $('#type').hide();
				    break;
			    }
			}

			var div = '#master';
			var el = $(div);
			var form = $('#form');
			form.validate({
			    rules: {
				criteria: {
				    required: true
				},
				region: {
				    required: true
				}
			    },
			    messages: {
				criteria: {
				    required: 'Select report criteria'
				},
				region: {
				    required: 'Choose Region'
				}
			    },
			    submitHandler: function (form) {

				el.block({overlayCSS: {backgroundColor: '#fff'}, message: '<i class="fa fa-refresh fa-spin"></i>', css: {border: 'none', color: '#333', background: 'none'}});
				var options = {
				    /*target:        '#output2',   target element(s) to be updated with server response*/
				    success: function (data, textStatus, XMLHttpRequest) {
					el.unblock();
					$('#report').off();  /*Calling .off() with no arguments removes all handlers attached to the elements.*/
					$('#report').empty();  /*clearing the content of the div*/
					$('#report').html(data);
				    },
				    error: function (xhr, textStatus, errorThrown) {

					if (xhr.responseText === undefined) {
					    $.gritter.add({
						/* (string | mandatory) the heading of the notification*/
						title: 'Connection timed out',
						/* (string | mandatory) the text inside the notification                                                 text: 'There was a problem on connecting to the network. Check your internet connection settings.',

						 (string | optional) the class name you want to apply to that specific message*/
						class_name: 'gritter-black'
					    });
					} else {
					    var myWindow = window.open("Error", "MsgWindow", "width=900, height=400");
					    myWindow.document.write(xhr.responseText);
					}
					el.unblock();
					/*clear controls that do not need to keep its previous info*/

				    },
				    url: home + 'report/custom_payment_report', /* override for form's 'action' attribute*/
				    data: {},
				    /*enctype: 'multipart/form-data',*/
				    type: 'post', /* 'get' or 'post', override for form's 'method' attribute*/
				    dataType: 'html', /* 'xml', 'script', 'html' or 'json' (expected server response type)*/
				    beforeSend: function () {

				    },
				    uploadProgress: function (event, position, total, percentComplete) {

				    },
				    complete: function () {

				    }
				    /*clearForm: true         clear all form fields after successful submit
				    resetForm: true         reset the form after successful submit
				    timeout:   3000*/
				};
				/*submit form via ajax*/
				$('#form').ajaxSubmit(options);
			    },
			    invalidHandler: function (event, validator) { /*display error alert on form submit
				errorHandler.show();*/
			    }
			});
</script>


