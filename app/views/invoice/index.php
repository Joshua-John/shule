
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-invoice"></i> <?= $this->lang->line('panel_title') ?></h3>


        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li class="active"><?= $this->lang->line('menu_invoice') ?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

		<?php
		$usertype = $this->session->userdata("usertype");
		if ($usertype == "Admin" || $usertype == "Accountant") {
		    ?>
    		<h5 class="page-header">
    		    <a class="btn btn-success" href="<?php echo base_url('invoice/add') ?>">
    			<i class="fa fa-plus"></i> 
			    <?= $this->lang->line('add_title') ?>
    		    </a>
    		</h5>
		<?php } ?>
		<div class="col-sm-6 col-sm-offset-3 list-group">
                    <div class="list-group-item list-group-item-warning">
                        <form style="" class="form-horizontal" role="form" method="post">  
                            <div class="form-group">              
                                <label for="classesID" class="col-sm-2 col-sm-offset-2 control-label">
				    <?= $this->lang->line("invoice_select_classes") ?>
                                </label>
                                <div class="col-sm-6">
				    <?php
				    $array = array("0" => $this->lang->line("invoice_select_classes"));
				    foreach ($classes as $classa) {
					$array[$classa->classesID] = $classa->classes;
				    }
				    echo form_dropdown("classesID", $array, set_value("classesID", $set), "id='classesID' class='form-control'");
				    ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th><?= $this->lang->line('slno') ?></th>
                                <th><?= $this->lang->line('invoice_feetype') ?></th>
                                <th><?= $this->lang->line('invoice_date') ?></th>
                                <th><?= $this->lang->line('invoice_status') ?></th>
				<th><?= $this->lang->line('invoice_number') ?></th>
                                <th><?= $this->lang->line('invoice_student') ?></th>
                                <th><?= $this->lang->line('invoice_paymentmethod') ?></th>
                                <th><?= $this->lang->line('invoice_amount') ?></th>
                                <th><?= $this->lang->line('invoice_due') ?></th>
                                <th><?= $this->lang->line('action') ?></th>
                            </tr>
                        </thead>
                        <tbody>
			    <?php
			    if (count($invoices)) {
				$i = 1;
				$total_paid=0;
				$total_unpaid=0;
				foreach ($invoices as $invoice) {
				    ?>
				    <tr>
					<td data-title="<?= $this->lang->line('slno') ?>">
					    <?php echo $i; ?>
					    <!--<input type="checkbox"/>-->
					</td>
					<td data-title="<?= $this->lang->line('invoice_feetype') ?>">
					    <?php echo $invoice->feetype; ?>
					</td>

					<td data-title="<?= $this->lang->line('invoice_date') ?>">
					    <?php echo $invoice->date; ?>
					</td>

					<td data-title="<?= $this->lang->line('invoice_status') ?>">
					    <?php
					    $status = $invoice->status;
					    $setstatus = '';
					    $btn_class = 'success';
					    if ($status == 0) {
						$status = $this->lang->line('invoice_notpaid');
						$btn_class = 'warning';
					    } elseif ($status == 1) {
						$status = $this->lang->line('invoice_partially_paid');
						$btn_class = 'danger';
					    } elseif ($status == 2) {
						$status = $this->lang->line('invoice_fully_paid');
						$btn_class = 'success';
					    } elseif ($status == 3) {
						$status = $this->lang->line('invoice_pending_approved');
						$btn_class = 'info';
					    }

					    echo "<button class='btn btn-" . $btn_class . " btn-xs'>" . $status . "</button>";
					    ?>
					</td>
					<td data-title="<?= $this->lang->line('invoice_number') ?>">
					    <?php echo $invoice->invoiceNO; ?>
					</td>
					<td data-title="<?= $this->lang->line('invoice_student') ?>">
					    <?php echo $invoice->student; ?>
					</td>

					<td data-title="<?= $this->lang->line('invoice_paymentmethod') ?>">
					    <?php echo $invoice->paymenttype; ?>
					</td>

					<td data-title="<?= $this->lang->line('invoice_amount') ?>">
					    <?php
					    echo $siteinfos->currency_symbol . number_format($invoice->amount,2);
					   $invoice->status== 2 ? $total_paid +=(int) $invoice->amount :
							    $total_unpaid +=(int) $invoice->amount;
					    ?>
					</td>

					<td data-title="<?= $this->lang->line('invoice_due') ?>">
					    <?php echo $siteinfos->currency_symbol . number_format(($invoice->amount - $invoice->paidamount),2); ?>
					</td>


					<td data-title="<?= $this->lang->line('action') ?>">
					    <?php echo btn_view('invoice/view/' . $invoice->invoiceID, $this->lang->line('view')) ?>
					    <?php if ($usertype == "Admin" || $usertype == "Accountant") { ?>
						<?php echo btn_edit('invoice/edit/' . $invoice->invoiceID, $this->lang->line('edit')) ?>
						<?php echo btn_delete('invoice/delete/' . $invoice->invoiceID, $this->lang->line('delete')) ?>
					    <?php } ?>
					</td>
				    </tr>
				    <?php
				    $i++;
				}
			    }
			    ?>
                        </tbody>
                    </table>

                </div>
		<?php if (count($invoices) > 0) { ?>
    		<h3 class="header">Payment Summary</h3>
    		<table class="table table-striped table-bordered table-hover dataTable no-footer">
    		    <thead>
    			<tr>
    			    <th>Total Invoices</th>
    			    <th>Total Amount Unpaid</th>
    			    <th>Total Amount paid</th>
    			</tr>
    		    </thead>
    		    <tbody>
    			<tr>
    			    <td><?= count($invoices) ?></td>
    			    <td><?= $siteinfos->currency_symbol.' '.number_format($total_unpaid) ?></td>
    			    <td><?= $siteinfos->currency_symbol.' '.number_format($total_paid) ?></td>
    			</tr>
    		    </tbody>
    		</table>
		<?php } ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#classesID').change(function () {
	var classesID = $(this).val();
	if (classesID == 0) {
	    $('#hide-table').hide();
	} else {
	    $.ajax({
		type: 'POST',
		url: "<?= base_url('/invoice/invoice_list') ?>",
		data: "id=" + classesID,
		success: function (data) {
		    window.location.href = data;
		}
	    });
	}
    });
</script>
