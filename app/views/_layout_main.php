<div id="loader-content">
    <div id="loader-wrapper">
        <div id="loader"></div>
    </div>
</div>
<?php $this->load->view("components/page_header"); ?>

<?php $this->load->view("components/page_menu");

?>

        <div class="right_col" role="main">
            <section class="content">
                <div class="row small_screen">
                    <div class="col-xs-12">
                        <?php $this->load->view($subview); ?> 
                    </div>
                </div>
            </section>
        </div>
                            


<!-- ClickDesk Live Chat Service for websites -->
<script type='text/javascript'>
//var _glc =_glc || []; _glc.push('all_ag9zfmNsaWNrZGVza2NoYXRyEgsSBXVzZXJzGICAgNL_5sUJDA');
//var glcpath = (('https:' == document.location.protocol) ? 'https://my.clickdesk.com/clickdesk-ui/browser/' : 
//'http://my.clickdesk.com/clickdesk-ui/browser/');
//var glcp = (('https:' == document.location.protocol) ? 'https://' : 'http://');
//var glcspt = document.createElement('script'); glcspt.type = 'text/javascript'; 
//glcspt.async = true; glcspt.src = glcpath + 'livechat-new.js';
//var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(glcspt, s);


</script>
<!-- End of ClickDesk -->
<?php $this->load->view("components/page_footer"); ?>
<script type="text/javascript">
    jQuery(window).load(function () {
        jQuery('#loader-content').fadeOut('slow');
    });
</script>

