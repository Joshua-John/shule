
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-invoice"></i> <?= $this->lang->line('panel_title') ?></h3>


        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li class="active"><?= $this->lang->line('menu_invoice') ?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

		<?php
		$usertype = $this->session->userdata("usertype");
		if ($usertype == "Admin" || $usertype == "Accountant") {
		    ?>
    		<h5 class="page-header">
    		    <a class="btn btn-success" href="<?php echo base_url('invoice/bulk_add') ?>">
    			<i class="fa fa-plus"></i> 
			    <?= $this->lang->line('add_title') ?>
    		    </a>
    		</h5>
		<?php } ?>
		<div class="col-sm-6 col-sm-offset-3 list-group">
                    <div class="list-group-item list-group-item-warning">
                        <form style="" class="form-horizontal" role="form" method="post">  
                            <div class="form-group">              
                                <label for="classesID" class="col-sm-2 col-sm-offset-2 control-label">
				    <?= $this->lang->line("invoice_select_classes") ?>
                                </label>
                                <div class="col-sm-6">
				    <?php
				    $array = array("0" => $this->lang->line("invoice_select_classes"));
				    foreach ($classes as $classa) {
					$array[$classa->classesID] = $classa->classes;
				    }
				    echo form_dropdown("classesID", $array, set_value("classesID", $set), "id='classesID' class='form-control'");
				    ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
                            <tr>
                                <th><?= $this->lang->line('slno') ?></th>
                                <th>Fee type</th>
                                <th>Fee Amount</th>
                                <th>Generate Invoices</th>
                                 <th>Generate Receipts</th>
                            </tr>
                        </thead>
                        <tbody>
			    <?php
			    if (count($fees)) {
				$i = 1;
				foreach ($fees as $fee) {
				    ?>
				    <tr>
					<td data-title="<?= $this->lang->line('slno') ?>">
					    <?php echo $i; ?>
					    <!--<input type="checkbox"/>-->
					</td>
					<td data-title="<?= $this->lang->line('invoice_feetype') ?>">
					    <?php echo $fee->feetype; ?>
					</td>

					<td data-title="<?= $this->lang->line('invoice_amount') ?>">
					    <?php
					    echo $siteinfos->currency_symbol . number_format($fee->amount);
					    ?>
					</td>


					<td data-title="<?= $this->lang->line('action') ?>">
		
					    <?php if ($usertype == "Admin" || $usertype == "Accountant") { ?>
				
                                                <?php echo btn_view('invoice/bulk_view/' . $classesID.'/'.$fee->feetypeID, 'Generate Invoice') ?>
					    <?php } ?>
					</td>
                                        
                                       <td data-title="<?= $this->lang->line('action') ?>">
                                       <?php
                                       
                                  
 echo btn_add_pdf('invoice/receipt/' .$fee->feetypeID. "/" . $classesID, 'Receipt');
							
	    			
				   
				 ?>     
                                            
                                        </td>  
                                        
				    </tr>
				    <?php
				    $i++;
				}
			    }
			    ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#classesID').change(function () {
	var classesID = $(this).val();
	if (classesID == 0) {
	    $('#hide-table').hide();
	} else {
	    $.ajax({
		type: 'POST',
		url: "<?= base_url('/invoice/bulk_list') ?>",
		data: "id=" + classesID,
		success: function (data) {
		    window.location.href = data;
		}
	    });
	}
    });
</script>
