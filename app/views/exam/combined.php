<?php
/**
 * Description of report
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
?>

<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-book"></i> <?= $this->lang->line('panel_title') ?></h3>


        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li class="active"><?= $this->lang->line('menu_report') ?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

		<?php
		$usertype = $this->session->userdata("usertype");
		?>

                <div class="col-sm-6 col-sm-offset-3 list-group">
                    <div class="list-group-item list-group-item-warning">
                        <form class="form-horizontal" role="form" method="post">


			    <?php
			    if (form_error('classesID'))
				echo "<div class='form-group has-error' >";
			    else
				echo "<div class='form-group' >";
			    ?>
			    <label for="classesID" class="col-sm-2 col-sm-offset-2 control-label">
				<?= $this->lang->line('eattendance_classes') ?>
			    </label>
			    <div class="col-sm-6">
				<?php
				$array = array("0" => $this->lang->line("eattendance_select_classes"));
				foreach ($classes as $classa) {
				    $array[$classa->classesID] = $classa->classes;
				}
				echo form_dropdown("classesID", $array, set_value("classesID"), "id='classesID' class='form-control'");
				?>
			    </div>
		    </div>
		     <?php
		    if (form_error('academic_year_id'))
			echo "<div class='form-group has-error' >";
		    else
			echo "<div class='form-group' >";
		    ?>
		    <label for="sectionID" class="col-sm-2 col-sm-offset-2 control-label">
			<?= $this->lang->line('exam_academic_year') ?>
		    </label>
		    <div class="col-sm-6">
			<?php
			$array = array("0" => $this->lang->line("exam_select_year"));
			if (!empty($academic_years)) {
			    foreach ($academic_years as $academic) {
				$array[$academic->id] = $academic->name;
			    }
			}

			echo form_dropdown("academic_year_id", $array, set_value("academic_year_id"), "id='academic_year_id' class='form-control'");
			?>
		    </div>
		</div>
                    <div id="examID"></div>
		    <div class="form-group">
			<div class="col-sm-offset-4 col-sm-8">
			    <input type="submit" class="btn btn-success" style="margin-bottom:0px" value="<?= $this->lang->line("view_combined_report") ?>" >
			</div>
		    </div>
		    </form>
		</div>
	    </div>

	    <?php if (count($students) > 0) { ?>

    	    <div class="col-sm-12">

    		<div class="nav-tabs-custom">
    		    <ul class="nav nav-tabs">
    			<li class="active"><a data-toggle="tab" href="#all" aria-expanded="true"><?= $this->lang->line("eattendance_all_students") ?></a></li>
			    <?php
			    foreach ($sections as $key => $section) {
				echo '<li class=""><a data-toggle="tab" href="#' . $section->sectionID . '" aria-expanded="false">' . $this->lang->line("student_section") . " " . $section->section . " ( " . $section->category . " )" . '</a></li>';
			    }
			    ?>
    		    </ul>
			<?php if ($report_sent == 0) { ?><p>
				<button class="btn btn-danger" id="send_report"><i class="fa fa-book"></i>Create Official Report</button>
			    </p>
			<?php } ?>
    		    <div class="tab-content">
    			<div id="all" class="tab-pane active">
    			    <div id="hide-table">
				    <?php
				    /*
				     * ------------------------------------------------------------------
				     * Here we load report for the whole class
				     * 
				     * ------------------------------------------------------------------
				     * 
				     */
				    ?>
    				<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
    				    <thead>
    					<tr>
    					    <th class="col-sm-2"><?= $this->lang->line('slno') ?></th>
    					    <th class="col-sm-2"><?= $this->lang->line('eattendance_name') ?></th>
    					    <th class="col-sm-2"><?= $this->lang->line('eattendance_roll') ?></th>
						<?php
						if (count($exams_title) > 0) {
						    foreach ($exams_title as $exam) {
							echo '<th class="col-sm-2">' . $exam->exam . '</th>';
						    }
						}
						?>

    					    <th class="col-sm-2">Total Marks</th>
    					    <th class="col-sm-2">Average</th>
    					    <th class="col-sm-2">Rank</th>
    					    <th class="col-sm-2"><?= $this->lang->line('action') ?></th>
    					</tr>
    				    </thead>
    				    <tbody>
					    <?php
					    if (count($combine_exams)) {
						$i = 1;
						foreach ($combine_exams as $combine) {
						    ?>
	    					<tr>
	    					    <td data-title="<?= $this->lang->line('slno') ?>">
							    <?php echo $i ?>
	    					    </td>

	    					    <td data-title="<?= $this->lang->line('eattendance_name') ?>">
							    <?php echo $combine['name']; ?>
	    					    </td>
	    					    <td data-title="<?= $this->lang->line('eattendance_roll') ?>">
							    <?php echo $combine['roll']; ?>
	    					    </td>
							<?php
							if (count($exams_title) > 0) {
							    $url_ids = '';
							    foreach ($exams_title as $exam) {

								$name = preg_replace('/[^a-z0-9]/', '', strtolower($exam->exam));
								$url_ids.=$exam->examID . '_';
								echo '<td>' . $combine[$name] . '</td>';
							    }
							}
							?>

	    					    <td data-title="<?= $this->lang->line('eattendance_roll') ?>">
							    <?= $combine['total'] ?>
	    					    </td>
	    					    <td data-title="<?= $this->lang->line('eattendance_roll') ?>">
							    <?= $combine['average'] ?>
	    					    </td>
	    					    <td data-title="<?= $this->lang->line('eattendance_phone') ?>">
							    <?= $combine['rank'] ?>
	    					    </td>
	    					    <td data-title="<?= $this->lang->line('action') ?>">
	    						<a href="<?= base_url('exam/singlecombined/' . $combine['studentID'] . '?rank=' . $combine['rank'] . '&exam=' . rtrim($url_ids, '_')) ?>&tag=all" class="btn btn-success btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Download Report"><i class="fa fa-download"></i></a>
	    					    </td>
	    					</tr>
						    <?php
						    $i++;
						}
					    }
					    ?>
    				    </tbody>
    				</table>
    			    </div>

    			</div>

			    <?php foreach ($sections as $key => $section) { ?>
				<div id="<?= $section->sectionID ?>" class="tab-pane">
				    <div id="hide-table">
					<?php
					/**
					 * ----------------------------------------------------------------
					 * 
					 * Here we load report for specific section only
					 * ---------------------------------------------------------------
					 */
					?>
					<table class="table table-striped table-bordered table-hover dataTable no-footer">
					    <thead>
						<tr>
						    <th class="col-sm-2"><?= $this->lang->line('slno') ?></th>
		
						    <th class="col-sm-2"><?= $this->lang->line('eattendance_name') ?></th>
						    <th class="col-sm-2"><?= $this->lang->line('eattendance_roll') ?></th>
						    
						    <?php
						    if (count($exams_title) > 0) {
							foreach ($exams_title as $exam) {
							    echo '<th class="col-sm-2">' . $exam->exam . '</th>';
							}
						    }
						    ?>

						    <th class="col-sm-2">Total Marks</th>
						    <th class="col-sm-2">Average</th>
						    <th class="col-sm-2">Rank</th>
						    <th class="col-sm-2"><?= $this->lang->line('action') ?></th>
						</tr>
					    </thead>
					    <tbody>
						<?php
						if (count($allsection[$section->section])) {
						    $i = 1;
						    foreach ($allsection[$section->section] as $student) {
							?>
							<tr>
	    					    <td data-title="<?= $this->lang->line('slno') ?>">
							    <?php echo $i ?>
	    					    </td>    					   
	    					    <td data-title="<?= $this->lang->line('eattendance_name') ?>">
							    <?php echo $student['name']; ?>
	    					    </td>
	    					    <td data-title="<?= $this->lang->line('eattendance_roll') ?>">
							    <?php echo $student['roll']; ?>
	    					    </td>
							<?php
							if (count($exams_title) > 0) {
							    $url_ids = '';
							    foreach ($exams_title as $exam) {

								$name = preg_replace('/[^a-z0-9]/', '', strtolower($exam->exam));
								$url_ids.=$exam->examID . '_';
								echo '<td>' . $student[$name] . '</td>';
							    }
							}
							?>

	    					    <td data-title="<?= $this->lang->line('eattendance_roll') ?>">
							    <?= $student['total'] ?>
	    					    </td>
	    					    <td data-title="<?= $this->lang->line('eattendance_roll') ?>">
							    <?= $student['average'] ?>
	    					    </td>
	    					    <td data-title="<?= $this->lang->line('eattendance_phone') ?>">
							    <?=$student['rank'] ?>
	    					    </td>
	    					    <td data-title="<?= $this->lang->line('action') ?>">
	    						<a href="<?= base_url('exam/singlecombined/' . $student['studentID'] . '?rank=' . $student['rank'] . '&exam=' . rtrim($url_ids, '_').'&section_id=true') ?>" class="btn btn-success btn-xs mrg" data-placement="top" data-toggle="tooltip" data-original-title="Download Report"><i class="fa fa-download"></i></a>
	    					    </td>
	    					</tr>
							<?php
							$i++;
						    }
						}
						?>
					    </tbody>
					</table>
				    </div>
				</div>
				<?php
			    }
			    /**
			     * ------------------------------------------------------------
			     * Here we end report by section
			     * -----------------------------------------------------------
			     */
			    ?>
    		    </div>

    		</div> <!-- nav-tabs-custom -->
    	    </div> <!-- col-sm-12 for tab -->

	    <?php
	    } else {
		/**
		 * -------------------------------------------------------------------
		 * Here we load default table if there is no any report generated yet
		 * --------------------------------------------------------------------
		 */
		?>
    	    <div class="col-sm-12">

    		<div class="nav-tabs-custom">
    		    <ul class="nav nav-tabs">
    			<li class="active"><a data-toggle="tab" href="#all" aria-expanded="true"><?= $this->lang->line("eattendance_all_students") ?></a></li>
    		    </ul>


    		    <div class="tab-content">
    			<div id="all" class="tab-pane active">
    			    <div id="hide-table">
    				<table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
    				    <thead>
    					<tr>
    					    <th class="col-sm-2"><?= $this->lang->line('slno') ?></th>
    					    <th class="col-sm-2"><?= $this->lang->line('eattendance_photo') ?></th>
    					    <th class="col-sm-2"><?= $this->lang->line('eattendance_name') ?></th>
    					    <th class="col-sm-2"><?= $this->lang->line('eattendance_roll') ?></th>
    					    <th class="col-sm-2"><?= $this->lang->line('eattendance_phone') ?></th>
    					    <th class="col-sm-2"><?= $this->lang->line('action') ?></th>
    					</tr>
    				    </thead>
    				    <tbody>
					    <?php
					    if (count($students)) {
						$i = 1;
						foreach ($students as $student) {
						    ?>
	    					<tr>
	    					    <td data-title="<?= $this->lang->line('slno') ?>">
	    <?php echo $i; ?>
	    					    </td>

	    					    <td data-title="<?= $this->lang->line('eattendance_photo') ?>">
							    <?php
							    $array = array(
								"src" => base_url('uploads/images/' . $student->photo),
								'width' => '35px',
								'height' => '35px',
								'class' => 'img-rounded'
							    );
							    echo img($array);
							    ?>
	    					    </td>
	    					    <td data-title="<?= $this->lang->line('eattendance_name') ?>">
	    <?php echo $student->name; ?>
	    					    </td>
	    					    <td data-title="<?= $this->lang->line('eattendance_roll') ?>">
	    <?php echo $student->roll; ?>
	    					    </td>
	    					    <td data-title="<?= $this->lang->line('eattendance_phone') ?>">
	    <?php echo $student->phone; ?>
	    					    </td>
	    					    <td data-title="<?= $this->lang->line('action') ?>">
							    <?php
							    foreach ($eattendances as $eattendance) {
								if ($eattendance->studentID == $student->studentID && $examID == $eattendance->examID && $classesID == $eattendance->classesID && $subjectID == $eattendance->subjectID) {

								    if ($eattendance->eattendance == "Present") {
									echo "<button class='btn btn-success btn-xs'>" . $eattendance->eattendance . "</button>";
								    } elseif ($eattendance->eattendance == "") {
									echo "<button class='btn btn-danger btn-xs'>" . "Absent" . "</button>";
								    } else {
									echo "<button class='btn btn-danger btn-xs'>" . $eattendance->eattendance . "</button>";
								    }
								    break;
								}
							    }
							    ?>
	    					    </td>
	    					</tr>
						    <?php
						    $i++;
						}
					    }
					    ?>
    				    </tbody>
    				</table>
    			    </div>

    			</div>
    		    </div>
    		</div> <!-- nav-tabs-custom -->
    	    </div>
	    <?php }
	    ?>



	</div> <!-- col-sm-12 -->
    </div><!-- row -->
</div><!-- Body -->
</div><!-- /.box -->
<?php if (isset($url_ids)) { ?>
    <script type="text/javascript">
        //    $('#classesID').change(function (event) {
        //	var classesID = $(this).val();
        //	window.location.href = "<?= base_url('exam/combined') ?>/" + classesID;
        //    });
        send_report = function () {
    	$('#send_report').click(function () {
    	    swal({
    		title: "Sending Report",
    		text: 'Hello, you are about to send report to all parents in this class. Once you send, this option cannot be reverted. Please enter administrator password to confirm:',
    		type: 'input',
    		inputType: "password",
    		showCancelButton: true,
    		closeOnConfirm: false,
    		animation: "slide-from-top"
    	    },
    	    function (inputValue) {
    		if (inputValue === false)
    		    return false;

    		if (inputValue === "") {
    		    swal.showInputError("You need to write administrator password!");
    		    return false;
    		} else {
    		    swal({title: 'Enter Report Name', text: 'Write a report name', type: 'input', closeOnConfirm: false},
    		    function (inputName) {
    			if (inputValue === false)
    			    return false;

    			if (inputValue === "") {
    			    swal.showInputError("You need to write administrator password!");
    			    return false;
    			}
    			$.post('<?= base_url('exam/send_report') ?>',
    				{password: inputValue, report_name: inputName, exam: '<?= rtrim($url_ids, '_') ?>', class: '<?= $classesID ?>'}, function (data) {
    			    swal({
    				title: data.status,
    				text: data.message,
    				type: data.status
    			    }, function () {
    				window.location.reload();
    			    });
    			}, 'JSON');

    		    });
    		}

    	    });
    	});
        }
        $(document).ready(send_report);

    </script>
<?php } ?>
<script>
     $('#classesID').change(function (event) {

	var classesID = $(this).val();
	if (classesID === '0') {
	    $('#academic_year_id').val(0);
	} else {
	    $.ajax({
		type: 'POST',
		url: "<?= base_url('exam/get_academic_years') ?>",
		data: "id=" + classesID,
		dataType: "html",
		success: function (data) {
		    $('#academic_year_id').html(data);
		}
	    });
	}
    });
    $('#academic_year_id').change(function (event) {

	var classesID = $('#classesID').val();
	var academic_year_id=$(this).val();

	if (classesID === '0') {
	    $('#examID').val(0);
	} else {
	    $.ajax({
		type: 'POST',
		url: "<?= base_url('exam/getClassExam') ?>",
		data: "id=" + classesID+'&year_id='+academic_year_id,
		dataType: "html",
		success: function (data) {
		    $('#examID').html(data);
		}
	    });
	}

    });
</script>