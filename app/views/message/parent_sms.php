<?php 
    $email = $this->session->userdata('email');
    $usertype=$this->session->userdata('usertype'); 
?>
<div class="box">
    <div class="box-body">
        <div class="row">


            <div class="col-md-9">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"><?=$this->lang->line('compose_new')?></h3>                    
                </div><!-- /.box-header -->                
                <div class="box-body">
                    <form role="form" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                        <div class="select2-wrapper">
                            <select class="form-control select2" name="to" >
                                <option></option>                 
                                <optgroup label="Classes List">
                                <?php foreach ($classes as $class): ?>
                                 
                                 <option value="<?=$class->classesID ?>"><?=$class->classes?></option>
                                    
                                <?php endforeach ?>
                                </optgroup>                        
                            </select>
                        </div>
                        <div class="has-error">
                            <?php if (form_error('to')): ?>
                                <p class="text-danger"> <?php echo form_error('to'); ?></p>
                            <?php endif ?>
                        </div>  
                      </div>
                      <div class="form-group">
                        <textarea class="form-control" name="message" rows="10" placeholder="Message"><?=set_value('message')?></textarea>
                        <div class="has-error">
                            <?php if (form_error('message')): ?>
                                <p class="text-danger"> <?php echo form_error('message'); ?></p>
                            <?php endif ?>
                        </div> 
                      </div>
                   
                      <div class="pull-right">                    
                        <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> <?=$this->lang->line('send')?></button>
                      </div> 
                    </form>
                </div><!-- /.box-body -->                    
              </div><!-- /. box -->
            </div><!-- /.col -->
        </div>
    </div>
</div>
<script>
document.getElementById("attachment").onchange = function() {
    document.getElementById("uploadFile").value = this.value;
};
</script>
