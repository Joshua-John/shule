<div class="box container">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-gears"></i> <?=$this->lang->line('panel_title')?></h3>

        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li class="active"><?=$this->lang->line('menu_setting')?></li>
        </ol>
    </div><!-- /.box-header -->
    <div class="row">

        <div class="col-md-6 col-sm-6 col-xs-12">
            <?php
            $usertype = $this->session->userdata("usertype");
            if($usertype == "Admin" || $usertype == "Teacher") {
                ?>
                <h5 class="page-header">
                    <a class="btn btn-success" href="<?php echo base_url('setting/upload_signature') ?>">
                        <i class="fa fa-plus"></i>
                        <?=$this->lang->line('upload_signature')?>
                    </a>
                </h5>
            <?php } ?>
            <div class="small-box ">
                <a class="small-box-footer" href="<?=base_url('teacher')?>">

                    <div class="inner ">
                        <h3 class="box-title">
                            Your Captured Signature
                        </h3>
                        <p id="signature" class="text-center small">
                            <img src="<?=$signature?>" style="max-width: 30% !important;" >

                        </p>
                    </div>
                </a>
            </div>
        </div>

    </div>
</div>
<script>
    $(document).ready(function(){

    });

</script>