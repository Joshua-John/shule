<?php

/**
 * Description of mailList
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
trait mailList {

    private $password = '';

    /**
     * 
     * @param type $email
     * @return string
     */
    public function addEmail($email,$listname) {
	return 'approve '.$this->password.' subscribe '.$listname.' ' . $email;
    }

    public function removeEmail($email,$listname) {
	return 'approve '.$this->password.' unsubscribe  '.$listname.' ' . $email;
    }
    
    public function addList($listname) {
	return 'subscriber ' . $listname;
    }

    public function removeList($listname) {
	return 'unsubscribe  ' . $listname;
    }

    public function sendToList($listname) {
	return $listname . '@shulesoft.com';
    }
    
    public function sendEmail($listname) {
	$command = $_POST['command'];
	$email_addr = 'noreply@shulesoft.com';
	$list_name = 'your-list';
	$to_addr = ' majordomo@shulesoft.com';
	$body = $command . ' ' . $list_name;
	mail($to_addr, '', $body, "From: $email_addr");
    }

}
