<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Classlevel_m extends MY_Model {

	protected $_table_name = 'classlevel';
	protected $_primary_key = 'classlevel_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = "classlevel_id desc";

	function __construct() {
		parent::__construct();
	}

	function get_classlevel($array=NULL, $signal=FALSE) {
		$query = parent::get($array, $signal);
		return $query;
	}

	function get_order_by_classlevel($array=NULL) {
		$query = parent::get_order_by($array);
		return $query;
	}

	function insert_classlevel($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	function update_classlevel($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_classlevel($id){
		parent::delete($id);
	}
}

/* End of file grade_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/grade_m.php */