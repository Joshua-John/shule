<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mark_m extends MY_Model {

    protected $_table_name = "mark";
    protected $_primary_key = 'markID';
    protected $_primary_filter = 'intval';
    protected $_order_by = "subject asc";

    function __construct() {
	parent::__construct();
    }

    function get_mark($array = NULL, $signal = FALSE) {
	$query = parent::get($array, $signal);
	return $query;
    }

    function get_order_by_mark($array = NULL) {
	$query = parent::get_order_by($array);
	return $query;
    }

    function insert_mark($array) {
	return parent::insert($array);
    }

    function update_mark($data, $id = NULL) {
	parent::update($data, $id);
	return $id;
    }

    function update_mark_classes($array, $id) {
	$this->db->update($this->_table_name, $array, $id);
	return $id;
    }

    public function delete_mark($id) {
	parent::delete($id);
    }
/**
 * @todo We fetch from student table and not in student_info view bse we need 
 *	  to get result per all students
 * @param int $class_id
 * @param int $exam_id
 * @return Object (name,roll,examID,classesID,studentID,average,sum,rank) per class
 */
    public function get_mark_subject_avg_perclass($class_id,$exam_id,$academic_year_id) {
	$sql='select s.name, s.roll, m."examID", m."classesID", m."studentID", avg(m.mark) as average, sum(m.mark), rank() over (order by avg(m.mark) desc) from ' . set_schema_name() . 'mark m JOIN ' . set_schema_name() . 'student s ON s."studentID"=m."studentID" where m."examID" = ' . $exam_id . '  and m.academic_year_id='.$academic_year_id.' and m."subjectID" IN (SELECT subject_id FROM ' . set_schema_name() . 'subject_count WHERE is_counted=1) and m.mark is not null group by m."examID", m."classesID", m."studentID",s.name, s.roll';
	return $this->db->query($sql)->result();
    }
    /**
     * @todo We fetch from student table and not in student_info view bse we need 
    *	  to get result per all students
     * @param int $section_id
     * @param int $class_id
     * @param int $exam_id
     * @return Object (name,roll,examID,classesID,studentID,average,sum,rank) per section
     */
     public function get_mark_subject_avg_persection($section_id,$class_id,$exam_id,$academic_year_id) {
	return $this->db->query('select s.name, s.roll, m."examID", m."classesID", m."studentID", avg(m.mark) as average, sum(m.mark), rank() over (order by avg(m.mark) desc) from ' . set_schema_name() . 'mark m JOIN ' . set_schema_name() . 'student s ON s."studentID"=m."studentID" where m."examID" = ' . $exam_id . ' and s."sectionID"=' . $section_id . ' and m.academic_year_id='.$academic_year_id.' and m."subjectID" IN (SELECT subject_id FROM ' . set_schema_name() . 'subject_count WHERE is_counted=1)  and m.mark is not null group by m."examID", m."classesID", m."studentID",s.name, s.roll')->result();
    }
    /**
     * @description Main Get list of students who study this subject
     * @param type $subjectID
     * @return type Object
     */
    public function get_student_subject($subjectID) {
	return $this->db->query('SELECT a.* FROM ' . set_schema_name() . 'student a WHERE '
			. 'a."sectionID" IN (select section_id FROM ' . set_schema_name() . 'subject_section'
			. ' WHERE subject_id=' . $subjectID . ') OR a."studentID" IN (SELECT student_id FROM '
			. ' ' . set_schema_name() . 'subject_student WHERE subject_id=' . $subjectID . ' )')->result();
    }

    public function get_subject_for_student($studentID) {
	return $this->db->query('select subject_id, subject FROM ' . set_schema_name() . 'subject_count where student_id=' . $studentID)->result();
    }
    
    public function get_mark_by_section($student_id, $exam_id, $class_id, $subjectID, $sectionID) {
	$sql = 'select sum(mark) as  total_mark, avg(mark) as average from  ' . set_schema_name() . 'mark where "examID"=' . $exam_id . ' AND "classesID"=' . $class_id . '  AND "subjectID"=' . $subjectID . ' AND "studentID" IN (select "studentID" from ' . set_schema_name() . 'student WHERE "sectionID"=' . $sectionID . ')';
	return $this->db->query($sql)->row();
    }

    function sum_student_subject_mark($studentID, $classesID, $subjectID) {
	$array = array(
	    "studentID" => $studentID,
	    "classesID" => $classesID,
	    "subjectID" => $subjectID
	);
	$this->db->select_sum('mark');
	$this->db->where($array);
	$query = $this->db->get('mark');
	return $query->row();
    }

    function count_subject_mark($studentID, $classesID, $subjectID) {
	$SQL = 'SELECT COUNT(*) as total_semester FROM mark WHERE "studentID" =' . $studentID . ' AND "classesID" =' . $classesID . ' AND "subjectID" =' . $subjectID . ' AND (mark != NULL OR mark <= 0 OR mark >0)';
	$query = $this->db->query($SQL);
	$result = $query->row();
	return $result;
    }

    function get_order_by_mark_with_subject($classes, $year) {
	$this->db->select('*');
	$this->db->from('subject');
	$this->db->join('mark', 'subject.subjectID = mark.subjectID', 'LEFT');
	$this->db->join('exam', 'exam.examID = mark.examID');
	$this->db->where('mark.classesID', $classes);
	$this->db->where('mark.year', $year);
	$query = $this->db->get();
	return $query->result();
    }

}

/* End of file mark_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/mark_m.php */