<?php

/**
 * Description of subject_student_m
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class subject_student_m extends MY_Model {

    public $_table_name = 'subject_student';
    public $_primary_key = 'subject_student_id';
    protected $_primary_filter = 'intval';
    protected $_order_by = "subject_student_id asc";

    function __construct() {
	parent::__construct();
	$this->load->database();
    }

    function get_students_not_subscribe($class_id,$section_id,$subject_id) {
	$sql='select a.*, b.* FROM '.  set_schema_name().'student a '
		. ' JOIN '.  set_schema_name().'section b '
		. ' ON a."sectionID" = b."sectionID" '
		. ' WHERE a."classesID"='.$class_id.' AND a."sectionID"='.$section_id.' AND '
		. ' a."studentID" NOT IN '
		. ' (SELECT student_id FROM '.  set_schema_name().'subject_student '
		. ' WHERE subject_id='.$subject_id.' )';
	return $this->db->query($sql)->result();
    }


    function insert_student($array) {
	$error = parent::insert($array);
	return TRUE;
    }

    function update_subject_student($data, $id = NULL) {

	parent::update($data, $id);

	return $id;
    }

    function update_student_classes($data, $array = NULL) {
	$this->db->set($data);
	$this->db->where($array);
	$this->db->update($this->_table_name);
    }

    function delete_subject_student($id) {
	parent::delete($id);
    }

    function delete_parent($id) {
	$this->db->delete('parent', array('studentID' => $id));
    }

    function hash($string) {
	return parent::hash($string);
    }

    /* infinite code starts here */
}

/* End of file student_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/student_m.php */