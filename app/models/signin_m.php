<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class signin_m extends MY_Model {

    function __construct() {
	parent::__construct();
	$this->load->helper('cookie');
    }

    public function signin() {
	//$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
	$array = array();
	$i = 0;
	$username = $this->input->post('username');
	$password = $this->hash($this->input->post('password'));
	$userdata = '';
	//foreach ($tables as $table) {
	$alluserdata = $this->user_m->get_username_row('users', array("username" => $username, "password" => $password));
	if (!empty($alluserdata)) {
	    $userdata = $alluserdata;
	    $array['permition'][$i] = 'yes';
	} else {
	    $array['permition'][$i] = 'no';
	}
	$i++;
	//}

	if (in_array('yes', $array['permition'])) {

	    $data = array(
		"id" => $userdata->id,
		"email" => $userdata->email,
		"usertype" => $userdata->usertype,
		"username" => $userdata->username,
		"lang" => "english",
		"loggedin" => TRUE
	    );
	    foreach ($data as $key => $value) {
		set_cookie($key, $value, 60 * 60 * 60 * 6); //set cookie for one day
	    }

	    $this->session->set_userdata($data);
	    return TRUE;
	} else {
	    return FALSE;
	}
    }

    function change_password() {
	$username = $this->session->userdata("username");
	$this->input->post('old_password');
	$old_password = $this->hash($this->input->post('old_password'));
	$new_password = $this->hash($this->input->post('new_password'));

	$user =$this->user_m->get_username_row('users', array("username" => $username, "password" => $old_password));
	if (!empty($user)) {
	    if ($user->password == $old_password) {
		$array = array(
		    "password" => $new_password
		);
		$this->db->where(array("username" => $username, "password" => $old_password));	
		$this->db->update($user->table, $array);
		$this->session->set_flashdata('success', $this->lang->line('menu_success'));
		return TRUE;
	    }
	} else {
	    return FALSE;
	}
    }

    public function signout() {
	$this->session->sess_destroy();
	$data = array(
	    "id",
	    "email",
	    "usertype",
	    "username",
	    "lang",
	    "loggedin"
	);
	foreach ($data as $value) {
	    delete_cookie($value);
	}
    }

    function loggedin() {
	$data = get_cookie('usertype');
	if ($data != '') {
	    $data = array(
		"id" => get_cookie('id'),
		"email" => get_cookie('email'),
		"usertype" => get_cookie('usertype'),
		"username" => get_cookie('username'),
		"lang" => "english",
		"loggedin" => TRUE
	    );
	    $this->session->set_userdata($data);
	    return TRUE;
	} else {
	    return (bool) $this->session->userdata("loggedin");
	}
    }

}

/* End of file signin_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/signin_m.php */