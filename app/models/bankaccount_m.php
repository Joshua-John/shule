<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of bankaccount_m
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
 
class bankaccount_m extends MY_Model {

	protected $_table_name = 'bank_account';
	protected $_primary_key = 'bank_account_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = "bank_account_id asc";

	function __construct() {
		parent::__construct();
	}

	function get_banks($array=NULL, $signal=FALSE) {
		$query = parent::get($array, $signal);
		return $query;
	}

	function get_order_by_bankaccount($array=NULL) {
		$query = parent::get_order_by($array);
		return $query;
	}

	function insert_bankaccount($array) {
		$error = parent::insert($array);
		return TRUE;
	}

	function update_bankaccount($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_bankaccount($id){
		parent::delete($id);
	}

	function allbankaccount($bankaccount) {
		$query = $this->db->query("SELECT * FROM bankaccount WHERE bankaccount LIKE '$bankaccount%'");
		return $query->result();
	}
            function get_allacc_number() {
		$this->db->select('*')->from('bank_account');
		$query = $this->db->get();
		return $query->result();
	}
}