<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Install_m extends CI_Model {

    function __construct() {
	parent::__construct();
	$this->sql_path = APPPATH . 'models/shiba.txt';
	$this->load->helper('file');
	$this->load->helper('url');
	$this->load->database();
    }

    public function insert_setting($data) {
	$this->db->insert('setting', $data);
	return TRUE;
    }

    public function select_setting() {
	$this->db->select('*');
	$query = $this->db->get('setting');
	return $query->result();
    }

    public function update_setting($data, $id) {
	$this->db->where('settingID', $id);
	$this->db->update('setting', $data);
	return TRUE;
    }

    public function hash($string) {
	return hash("sha512", $string . config_item("encryption_key"));
    }

    public function use_sql_string() {
	$sql = read_file($this->sql_path);
	$sql = trim($sql);
	$sql = $this->db->_prep_query($sql);
	$link = @mysqli_connect($this->db->hostname, $this->db->username, $this->db->password, $this->db->database);
	mysqli_multi_query($link, $sql);
    }

    public function install_new_db() {
	$db_file = file_get_contents('install.sql');
	$schema = str_replace('.', '', set_schema_name());
	$sql = str_replace('testing', $schema, $db_file);

	$queries = explode(';', $sql);
	$Conn = new PDO('pgsql:host='.$this->db->hostname.';port='.$this->db->port.';dbname='.$this->db->database,$this->db->username, $this->db->password);

	foreach ($queries as $query) {
	    $Conn->exec($query);
	}
	return TRUE;
    }

    public function wrappStatements() {
	$this->db->trans_start();
	$data = $this->db->query('ALTER TABLE testing.student ALTER COLUMN ujinga SET DEFAULT 0');
	print_r($data);
	$this->db->query($sql);
	$this->db->trans_complete();
    }

    public function createDb($db_name) {
	if ($this->dbforge->create_database($db_name)) {
	    return TRUE;
	}
    }

    public function dropDb($db_name) {
	if ($this->dbforge->drop_database($db_name)) {
	    return TRUE;
	}
    }

}

?>