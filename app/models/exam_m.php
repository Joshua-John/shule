<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Exam_m extends MY_Model {

    protected $_table_name = 'exam';
    protected $_primary_key = 'examID';
    protected $_primary_filter = 'intval';
    protected $_order_by = "date asc";

    function __construct() {
	parent::__construct();
    }

    function get_exam($array = NULL, $signal = FALSE) {
	$query = parent::get($array, $signal);
	return $query;
    }

    function get_order_by_exam($array = NULL) {
	$query = parent::get_order_by($array);
	return $query;
    }

    function insert_exam($array) {
	$error = parent::insert($array);
	return TRUE;
    }

    function update_exam($data, $id = NULL) {
	parent::update($data, $id);
	return $id;
    }

    function get_exam_by_class($id,$academic_year=NULL) {
	return $this->db->query('SELECT a.exam, a."examID" from ' . set_schema_name() . 'exam a join ' . set_schema_name() . 'class_exam b ON b.exam_id=a."examID" where a."examID" IN ( SELECT DISTINCT "examID" FROM ' . set_schema_name() . 'mark WHERE "classesID"=' . $id . ') and b.class_id=' . $id)->result();
    }

    function get_result_format($studentID) {
	return $this->db->query('select a.result_format, b."classesID" from ' . set_schema_name() . 'student b JOIN ' . set_schema_name() . 'classes c ON c."classesID"=b."classesID" JOIN ' . set_schema_name() . 'classlevel a ON a.classlevel_id=c.classlevel_id WHERE b."studentID"=' . $studentID)->row();
    }

    public function delete_exam($id) {
	parent::delete($id);
    }

}

/* End of file exam_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/exam_m.php */