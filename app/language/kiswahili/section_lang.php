<?php

/* List Language  */
$lang['panel_title'] = "Mkondo";
$lang['add_title'] = "Ongeza Mkondo";
$lang['slno'] = "#";
$lang['section_name'] = "Mkondo";
$lang['section_category'] = "Kipengere";
$lang['section_classes'] = "Darasa";
$lang['section_teacher_name'] = "Jina la Mwalimu";
$lang['section_note'] = "Kumbuka";
$lang['action'] = "Hatua";

$lang['section_select_class'] = "Chagua Darasa";
$lang['section_select_teacher'] = "Chagua Mwalimu";

$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_class'] = 'Ongeza Mkondo';
$lang['update_class'] = 'Sasisha Mkondo';