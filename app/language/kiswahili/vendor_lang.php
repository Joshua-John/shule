<?php


/**
 * Description of vendor_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */

$lang['vendor_title'] = 'Orodha ya Wauzaji';
$lang['add_vendor'] = 'Ongeza Muuzaji';

$lang['vendor_name'] = 'Jina';
$lang['vendor_email'] = 'Barua pepe';
$lang['vendor_phone'] = 'Namba ya Simu';
$lang['vendor_location'] = 'Eneo';
$lang['vendor_bank'] = 'Jina la Benki';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['view'] = 'Tazama';
/*
 * vendor add page
 */
$lang['panel_title'] = 'Ongeza Muuzaji Mpya';
$lang['vendor_telephone'] = 'Namba ya Simu';

/*
 * Add form
 */
$lang['name'] = 'Jina';
$lang['email'] = 'Barua pepe';
$lang['phone_number'] = 'Namba ya Simu ya Mkononi';
$lang['telephone_number'] = 'Namba ya Simu';
$lang['country'] = 'Nchi';
$lang['city'] = 'Mji';
$lang['location'] = 'Eneo';
$lang['bank_name'] = 'Jina la Benki';
$lang['bank_branch']='Tawi la Benki';
$lang['account_number'] = 'Akaunti Namba ya benki';
$lang['contact_person_name'] = 'Jina';
$lang['contact_person_email'] = 'Barua pepe';
$lang['contact_person_phone'] = 'Namba ya Simu';
$lang['contact_person_jobtitle'] = 'Jina la kazi';
$lang['update_vendor']='Sasisha';
$lang['service_product']='Huduma itolewayo';
$lang['created_at']='Muda ilipotegenezwa';
/**
 * view vendor
 */
$lang['pdf_preview']='PDF preview';
$lang['other_information']='Other Information';