<?php

/* List Language  */
$lang['panel_title'] = "Somo";
$lang['add_title'] = "Ongeza Somo";
$lang['slno'] = "#";
$lang['subject_class_name'] = "Jina la Darasa";
$lang['subject_teacher_name'] = "Jina la Mwalimu";
$lang['subject_student'] = "Mwanafunzi";
$lang['subject_name'] = "Jina la Somo";
$lang['subject_author'] = "Aina ya Somo";
$lang['subject_code'] = "Namba ya Somo";
$lang['subject_teacher'] = "Mwalimu";
$lang['subject_classes'] = "Darasa";
$lang['select_all'] = "Yote";
$lang['subject_select_class'] = "Chagua Darasa";
$lang['subject_select_section'] = "Chagua Mkondo";
$lang['subject_select_teacher'] = "Chagua Mwalimu";
$lang['subject_select_student'] = "Chagua Mwanafunzi";




$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['close'] = 'Funga';

/* Add Language */

$lang['add_subject'] = 'Ongeza Somo';
$lang['update_subject'] = 'Sasisha Somo';