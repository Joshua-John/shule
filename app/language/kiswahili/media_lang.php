<?php

/* List Language  */
$lang['panel_title'] = "Media";
$lang['add_title'] = "Tengeneza Folder";
$lang['slno'] = "#";
$lang['media_title'] = "Kichwa";
$lang['media_date'] = "Tarehe";
$lang['action'] = "Hatua";

$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_class'] = 'Ongeza media';
$lang['update_class'] = 'Sasisha media';
$lang['file'] = 'Media';
$lang['upload_file'] = 'Pakia media';
$lang['folder_name'] = 'Folder name';
$lang['share'] = 'Shirikisha';
$lang['share_with'] = 'Shirikisha na';
$lang['select_class'] = 'Chagua Darasa';
$lang['all_class'] = 'Darasa lote';
$lang['public'] = 'Wazi';
$lang['class'] = 'Darasa';
