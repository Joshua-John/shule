<?php

/* List Language  */
$lang['panel_title'] = "Mtumiaji";
$lang['add_title'] = "Ongeza Mtumiaji";
$lang['slno'] = "#";
$lang['user_photo'] = "Picha";
$lang['user_name'] = "Jina";
$lang['user_email'] = "Barua pepe";
$lang['user_dob'] = "Tarehe ya kuzaliwa";
$lang['user_sex'] = "Jinsia";
$lang['user_sex_male'] = "Kiume";
$lang['user_sex_female'] = "Kike";
$lang['user_religion'] = "Dini";
$lang['user_phone'] = "Namba ya Simu ya Mkononi";
$lang['user_address'] = "Anuani";
$lang['user_jod'] = "Tarehe ya Kujiunga";
$lang['user_type'] = "Aina";
$lang['user_username'] = "Jina la Kutumia";
$lang['user_password'] = "Neno siri";


$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['pdf_preview'] = 'Onesha PDF ya awali';
$lang['idcard'] = 'Nmaba ya KItambulisho';
$lang['print'] = 'Chapa';
$lang["mail"] = "Tuma PDF kwa barua pepe";

/* Add Language */
$lang['personal_information'] = "Taarifa Binafsi";
$lang['add_user'] = 'Ongeza Mtumiaji';
$lang['update_user'] = 'Sasisha Mtumiaji';

$lang['to'] = 'Kwenda';
$lang['subject'] = 'Kichwa cha Ujumbe';
$lang['message'] = 'Ujumbe';
$lang['send'] = 'Tuma';
$lang['mail_to'] = "Sehemu ya kwenda inahitajika";
$lang['mail_valid'] = "Sehemu ya kwenda lazima iwe na barua pepe halali";
$lang['mail_subject'] = "Sehemu ya somo inahitajika";
$lang['mail_success'] = 'Barua pepe imefanikiwa kutumwa';
$lang['mail_error'] = 'Barua pepe imeshindwa kutuma';