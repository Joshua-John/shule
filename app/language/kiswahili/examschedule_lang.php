<?php

/* List Language  */
$lang['panel_title'] = "Ratiba ya Mtihani";
$lang['add_title'] = "Ongeza Ratiba ya Mtihani";
$lang['slno'] = "#";
$lang['examschedule_name'] = "Jina la Mtihani";
$lang['examschedule_classes'] = "Darasa";
$lang['examschedule_all_examschedule'] = "Ratiba zote za Mitihani";
$lang['examschedule_select_exam'] = "Chagua Mtihani";
$lang['examschedule_select_classes'] = "Chagua Darasa";
$lang['examschedule_select_subject'] = "Chagua Somo";
$lang['examschedule_select_section'] = "Chagua Mkondo";
$lang['examschedule_select_student'] = "Chagua Mwanafunzi";

$lang['examschedule_section'] = "Mkondo";
$lang['examschedule_student'] = "Mwanafunzi";
$lang['examschedule_subject'] = "Somo";
$lang['examschedule_date'] = "Tarehe";
$lang['examschedule_time'] = "Muda";
$lang['examschedule_examfrom'] = "Muda wa kuanza";
$lang['examschedule_examto'] = "Muda wa kuisha";
$lang['examschedule_room'] = "Chumba";
$lang['examschedule_note'] = "Kumbuka";

$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_examschedule'] = 'Ongeza Rabita ya Mtihani';
$lang['update_examschedule'] = 'Sasisha Rabita ya Mtihani';