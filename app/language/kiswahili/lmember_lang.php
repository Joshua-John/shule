<?php

/* List Language  */
$lang['panel_title'] = "Mwanachama";
$lang['panel_title_profile'] = "Profile";
$lang['slno'] = "#";
$lang['lmember_lID'] = "Namba ya Maktaba";
$lang['lmember_photo'] = "Picha";
$lang['lmember_name'] = "Jina";
$lang['lmember_section'] = "Mkondo";
$lang['lmember_roll'] = "Namba ya Udahili";
$lang['lmember_email'] = "Barua Pepe";
$lang['lmember_phone'] = "Namba ya Simu";
$lang['lmember_lfee'] = "Ada ya Maktaba";
$lang['lmember_joindate'] = "Tarehe ya Kujiunga";
$lang['lmember_classes'] = "Darasa";
$lang['lmember_add_all']="Ongeza Wote";
$lang['lmember_select_class'] = "Chagua Darasa";

$lang['lmember_dob'] = "Tarehe ya Kuzaliwa";
$lang['lmember_sex'] = "Jinsia";
$lang['lmember_religion'] = "Dini";
$lang['lmember_address'] = "Anuani";
$lang['lmember_message'] = "Haujaongezwa.";


$lang['lmember_book'] = "Kitabu";
$lang['lmember_author'] = "Mwandishi";
$lang['lmember_serial_no'] = "Serial No"; 
$lang['lmember_issue_date'] = "Tarehe ya kuazima";
$lang['lmember_due_date'] = "Tarehe ya Kudaiwa";
$lang['lmember_return_date'] = "Tarehe ya Kurudisha"; 
$lang['lmember_fine'] = "Tozo";


$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['lmember'] = 'Maktaba';
$lang['delete'] = 'Futa';
$lang['librarycard'] = 'Kitambulisho cha Maktaba';
$lang['pdf_preview'] = 'Onesha PDF ya Awali';
$lang['print'] = 'Chapa';
$lang["mail"] = "Tuma PDF kwa barua pepe";

$lang['personal_information'] = "Taarifa Binafsi";
$lang['book_issue_history'] = "Historia ya kuazimisha kitabu";

$lang["add_lmember"] = "Ongeza Mwanachama";
$lang["update_lmember"] = "Sasisha Mwanachama";

$lang['to'] = 'Kwenda';
$lang['subject'] = 'Kichwa';
$lang['message'] = 'Ujumbe';
$lang['send'] = 'Tuma';
$lang['mail_to'] = "Sehemu ya Kutuma inahitajika";
$lang['mail_valid'] = "Sehemu ya kutuma lazima iwe na barua pepe halisi";
$lang['mail_subject'] = "Sehemu ya somo inahitajika.";
$lang['mail_success'] = 'Barua pepe imefanikiwa imetumwa ';
$lang['mail_error'] = 'Barua pepe haijatumwa';