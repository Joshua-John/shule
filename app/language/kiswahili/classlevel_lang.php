<?php

/**
 * Description of classlevel_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
$lang['panel_title']='Daraja la Darasa';
$lang['add_title']='Ongeza Daraja';
$lang['name']='Jina la Daraja';
$lang['start_date']='Tarehe ya kuanza';
$lang['end_date']='Tarehe ya kumaliza';
$lang['span_number']='Span Number';
$lang['note']='Notisi';
$lang['action']='Hatua';
$lang['slno']='#';

$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_level'] = 'Ongeza Daraja';
$lang['update_level'] = 'Sasisha Daraja';
