<?php

/* List Language  */
$lang['panel_title'] = "Mwalimu";
$lang['add_title'] = "Ongeza Mwalimu";
$lang['slno'] = "#";
$lang['teacher_photo'] = "picha";
$lang['teacher_name'] = "Jina"; 
$lang['teacher_designation'] = 'Cheo';
$lang['teacher_email'] = "Barua pepe";
$lang['teacher_dob'] = "Tarehe ya Kuzaliwa";
$lang['teacher_sex'] = "Jinsia";
$lang['teacher_sex_male'] = "Kiume";
$lang['teacher_sex_female'] = "Kike";
$lang['teacher_religion'] = "Dini";
$lang['teacher_phone'] = "Picha";
$lang['teacher_address'] = "Anuani";
$lang['teacher_jod'] = "Tarehe ya Kujiunga";
$lang['teacher_username'] = "Jina la Kutumia";
$lang['teacher_password'] = "Neno Siri";


$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['print'] = "Chapa";
$lang['pdf_preview'] = "Onesha PDF ya awali";
$lang['idcard'] = "Mamba ya Kitambulisho";
$lang['mail'] = "Tuma PDF kwa barua pepe";

/* Add Language */
$lang['personal_information'] = "Taarifa Binafsi";
$lang['add_teacher'] = 'Ongeza Mwalimu';
$lang['update_teacher'] = 'Sasisha Mwalimu';

$lang['to'] = 'Kwenda';
$lang['subject'] = 'Kichwa cha Ujumbe';
$lang['message'] = 'Ujumbe';
$lang['send'] = 'Tuma';
$lang['mail_to'] = "Sehemu ya kwenda inahitajika";
$lang['mail_valid'] = "Sehemu ya kwenda lazima iwe na barua pepe halali";
$lang['mail_subject'] = "Sehemu ya somo inahitajika";
$lang['mail_success'] = 'Barua pepe imefanikiwa kutumwa';
$lang['mail_error'] = 'Barua pepe imeshindwa kutuma';