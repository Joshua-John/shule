<?php


/* List Language  */
$lang['panel_title'] = "Daraja";
$lang['add_title'] = "Ongeza Daraja";
$lang['slno'] = "#";
$lang['grade_name'] = "Jina la Daraja";
$lang['grade_point'] = "Pointi za Daraja";
$lang['grade_gradefrom'] = "Alama kutoka";
$lang['grade_gradeupto'] = "Alama Mpaka";
$lang['grade_note'] = "Maelezo";
$lang['overall_note'] = "Maelezo Kwa ujumla";

$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_class'] = 'Ongeza Daraja';
$lang['update_class'] = 'Sasisha Daraja';
$lang['classlevel'] = 'Kiwango cha Darasa';