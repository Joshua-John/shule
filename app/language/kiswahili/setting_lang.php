<?php

/* List Language  */
$lang['panel_title'] = "Mpangilio";
$lang['add_title'] = "Badilisha Mpangilio";

$lang['setting_school_name'] = "Kichwa cha tovuti";
$lang['setting_school_phone'] = "Namba ya Simu ";
$lang['setting_school_email'] = "Mfumo wa Barua pepe";
$lang['setting_school_address'] = "Anuani";
$lang['setting_school_currency_code'] = "Aina ya Fedha";
$lang['setting_school_currency_symbol'] = "Alama ya Fedha";
$lang['setting_school_automation'] = "Kujiendesha";
$lang['setting_school_footer'] = "Footer";
$lang['setting_school_photo'] = "Nembo";

/*edit */
$lang['update_setting'] = 'Sasishi mpangilio';
$lang['upload_setting'] = 'Pakia';
$lang['settings_pde'] = "Namba ya Manunuzi sio sahihi";