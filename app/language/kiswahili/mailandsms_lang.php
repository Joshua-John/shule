<?php

/* List Language  */
$lang['panel_title'] = "Posta / ujumbe Mfupi ";
$lang['add_title'] = "Ongeza Barua pepe / Ujumbe mfupi";
$lang['slno'] = "#";
$lang['mailandsms_users'] = "Watumiaji";
$lang['mailandsms_select_user'] = "Chagua watumiaji";
$lang['mailandsms_select_template'] = "Chagua Template";
$lang['mailandsms_select_send_by'] = "Chagia Imetumwa kwa";
$lang['mailandsms_students'] = "Wanafunzi";
$lang['mailandsms_parents'] = "Wazazi";
$lang['mailandsms_teachers'] = "Walimu";
$lang['mailandsms_librarians'] = "Wakutubi";
$lang['mailandsms_accountants'] = "Wahasibu";
$lang['mailandsms_template'] = "Template";
$lang['mailandsms_type'] = "Aina";
$lang['mailandsms_email'] = "Barua pepe";
$lang['mailandsms_sms'] = "Ujumbe Mfupi";
$lang['mailandsms_getway'] = 'Imetumwa kwa';
$lang['mailandsms_subject'] = 'Kichwa cha Ujumbe';
$lang['mailandsms_message'] = "Ujumbe";
$lang['mailandsms_dateandtime'] = "Tarehe na Muda";


$lang['mailandsms_clickatell'] = "Clickatell";
$lang['mailandsms_twilio'] = "Twilio";
$lang['mailandsms_bulk'] = "Bulk";


$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['send'] = "Tuma";
$lang['mail_success'] = 'Barua pepe imefanikiwa kutumwa!';
$lang['mail_error'] = 'Barua pepe imeshindwa kutumwa!';
$lang['mail_error_user'] = 'Orodha ya watumia haipo!';





