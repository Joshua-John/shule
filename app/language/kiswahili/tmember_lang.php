<?php

/* List Language  */
$lang['panel_title'] = "Mwanachama";
$lang['panel_title_profile'] = "Profile";
$lang['slno'] = "#";
$lang['tmember_photo'] = "Picha";
$lang['tmember_name'] = "Jina";
$lang['tmember_section'] = "Mkondo";
$lang['tmember_roll'] = "Nmaba ya Udahili";
$lang['tmember_email'] = "Barua pepe";
$lang['tmember_phone'] = "Namba ya simu ya mkononi";
$lang['tmember_tfee'] = "Ada ya Usafiri";
$lang['tmember_route_name'] = "Jina la Njia";
$lang['tmember_classes'] = "Darasa";
$lang['tmember_select_class'] = "Chagua Darasa";
$lang['classes_select_route_name'] = "Chagua Njia";
$lang['tmember_message'] = "Haujaongezwa bado.";

$lang['tmember_joindate'] = "Tarehe ya Kujiunga";
$lang['tmember_dob'] = "Tarehe ya Kuzaliwa";
$lang['tmember_sex'] = "Jinsia";
$lang['tmember_religion'] = "Dini";
$lang['tmember_address'] = "Anuani";

$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['tmember'] = 'Usafiri';
$lang['delete'] = 'Futa';
$lang['pdf_preview'] = 'Onesha PDF ya awli';
$lang['print'] = 'Chapa';
$lang["mail"] = "Tuma PDF kwa barua pepe";

$lang['personal_information'] = "Taarifa Binafsi";

$lang["add_tmember"] = "Ongeza Mwanachama";
$lang["update_tmember"] = "Sasisha Mwanachama";

$lang['to'] = 'Kwenda';
$lang['subject'] = 'Kichwa cha Ujumbe';
$lang['message'] = 'Ujumbe';
$lang['send'] = 'Tuma';
$lang['mail_to'] = "Sehemu ya kwenda inahitajika";
$lang['mail_valid'] = "Sehemu ya kwenda lazima iwe na barua pepe halali";
$lang['mail_subject'] = "Sehemu ya somo inahitajika";
$lang['mail_success'] = 'Barua pepe imefanikiwa kutumwa';
$lang['mail_error'] = 'Barua pepe imeshindwa kutuma';