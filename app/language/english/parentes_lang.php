<?php
/* List Language  */
$lang['panel_title'] = "Parents";
$lang['add_title'] = "Add a parents";
$lang['slno'] = "#";
$lang['parentes_photo'] = "Photo";
$lang['parentes_name'] = "Name";
$lang['parentes_email'] = "Email";
$lang['parentes_dob'] = "Date of Birth";
$lang['parentes_sex'] = "Sex";
$lang['parentes_religion'] = "Religion";
$lang['parentes_phone'] = "Phone";
$lang['parentes_other_phone'] = "Other Phone";
$lang['parentes_address'] = "Address";
$lang['parentes_classes'] = "Class";
$lang['parentes_roll'] = "Roll";
$lang['parentes_photo'] = "Photo";
$lang['parentes_username'] = "Username";
$lang['parentes_password'] = "Password";
$lang['parentes_select_class'] = "Select Class";

/* Parentes */
$lang['parentes_guargian_name'] = "Guardian Name";
$lang['parentes_father_name'] = "Father's Name";
$lang['parentes_mother_name'] = "Mother's Name";
$lang['parentes_father_profession'] = "Father's Profession";
$lang['parentes_mother_profession'] = "Mother's Profession";


$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['pdf_preview'] = 'PDF Preview';
$lang['print'] = 'Print';
$lang["mail"] = "Send Pdf to Mail";

$lang['to'] = 'To';
$lang['subject'] = 'Subject';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['mail_to'] = "The To field is required.";
$lang['mail_valid'] = "The To field must contain a valid email address.";
$lang['mail_subject'] = "The Subject field is required.";
$lang['mail_success'] = 'Email send successfully!';
$lang['mail_error'] = 'oops! Email could not be sent!';

/* Add Language */

$lang['add_parentes'] = 'Add Parents';
$lang['update_parentes'] = 'Update Parents';

/* ini code starts here*/
$lang['personal_information'] = "Personal Information";
$lang['parentess_information'] = "Parents Information";


$lang['student_photo'] = "Photo";
$lang['student_name'] = "Name";
$lang['student_email'] = "Email";
$lang['student_dob'] = "Date of Birth";
$lang['student_sex'] = "Gender";
$lang['student_sex_male'] = "Male";
$lang['student_sex_female'] = "Female";
$lang['student_religion'] = "Religion";
$lang['student_hostel'] = "Hostel";
$lang['student_address'] = "Address";
$lang['student_classes'] = "Class";
$lang['student_roll'] = "Roll";
$lang['student_username'] = "Username";
$lang['student_library'] = "Library";
$lang['student_select_class'] = "Select Class";
$lang['student_guargian'] = "Guardian";
$lang['guardian_employer'] = "Employer Name";