<?php

/**
 * Description of exam_report_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
$lang['panel_title'] = "Exam Report";
$lang['add_title'] = "Add Exam Attendance";
$lang['slno'] = "#";
$lang['eattendance_photo'] = "Photo";
$lang['eattendance_name'] = "Name";
$lang['eattendance_email'] = "Email";
$lang['eattendance_roll'] = "Roll";
$lang['eattendance_phone'] = "Phone";
$lang['eattendance_attendance'] = "Attendance";
$lang['eattendance_section'] = "Section";
$lang['eattendance_exam'] = "Exam";
$lang['eattendance_classes'] = "Class";
$lang['eattendance_subject'] = "Subject";
$lang['eattendance_all_students'] = 'All Students';

$lang['eattendance_select_exam'] = "Select Exam";
$lang['eattendance_select_classes'] = "Select Class";
$lang['eattendance_select_subject'] = "Select Subject";


$lang['action'] = "Action";

/* Add Language */

$lang['add_attendance'] = 'Attendance';
$lang['add_all_attendance'] = 'Add All Attendance';
$lang['view_report'] = "View Report";
$lang['division']='Division';
$lang['total_point']='Total Points';
$lang['view_combined_report']='View Combined Report';