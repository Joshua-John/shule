<?php


/**
 * Description of vendor_lang
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */

$lang['vendor_title'] = 'List of Vendors';
$lang['add_vendor'] = 'Add Vendor';

$lang['vendor_name'] = 'Name';
$lang['vendor_email'] = 'Email';
$lang['vendor_phone'] = 'Phone Number';
$lang['vendor_location'] = 'Location';
$lang['vendor_bank'] = 'Bank Name';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['view'] = 'View';
/*
 * vendor add page
 */
$lang['panel_title'] = 'Add new Vendor';
$lang['vendor_telephone'] = 'Telephone';

/*
 * Add form
 */
$lang['name'] = 'Name';
$lang['email'] = 'Email';
$lang['phone_number'] = 'Phone';
$lang['telephone_number'] = 'Telephone';
$lang['country'] = 'Country';
$lang['city'] = 'City';
$lang['location'] = 'Location';
$lang['bank_name'] = 'Bank Name';
$lang['bank_branch']='Bank Branch';
$lang['account_number'] = 'Bank Account Number';
$lang['contact_person_name'] = 'Contact Person Name';
$lang['contact_person_email'] = 'Contact Person Email';
$lang['contact_person_phone'] = 'Contact Person Phone';
$lang['contact_person_jobtitle'] = 'Contact Person Job Title';
$lang['update_vendor']='Update';
$lang['service_product']='Service or Product offerred';
$lang['created_at']='Time Created';
/**
 * view vendor
 */
$lang['pdf_preview']='PDF preview';
$lang['other_information']='Other Information';