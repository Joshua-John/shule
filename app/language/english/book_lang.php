<?php

/* List Language  */
$lang['panel_title'] = "Books";
$lang['add_title'] = "Add a book";
$lang['slno'] = "#";
$lang['book_name'] = "Name";
$lang['book_subject_code'] = "Subject Code";
$lang['book_author'] = "Author";
$lang['book_class'] = "Class";
$lang['book_subject'] = "Subject";
$lang['book_price'] = "Price";
$lang['book_edition'] = "Edition";
$lang['book_for'] = "Book For";
$lang['book_quantity'] = "Quantity";
$lang['book_rack_no'] = "Rack No";
$lang['book_status'] = "Status";
$lang['book_available'] = "Available";
$lang['book_unavailable'] = "Unavailable";

$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */

$lang['add_book'] = 'Add Book';
$lang['update_book'] = 'Update Book';