<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reset extends CI_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
	parent::__construct();
	$this->load->helper("email");
	$this->load->helper("url");
    }

    protected function rules() {
	$rules = array(
	    array(
		'field' => 'newpassword',
		'label' => "New Password",
		'rules' => "trim|required|xss_clean|min_length[4]|max_length[40]|matches[repassword]"
	    ),
	    array(
		'field' => 'repassword',
		'label' => "Re-Password",
		'rules' => "trim|required|xss_clean|min_length[4]|max_length[40]"
	    )
	);
	return $rules;
    }

    protected function rules_email() {
	$rules = array(
	    array(
		'field' => 'phone',
		'label' => "Phone",
		'rules' => "trim|required|xss_clean|max_length[20]"
	    ),
	    array(
		'field' => 'answer',
		'label' => "Answer",
		'rules' => "trim|required|xss_clean|max_length[4]"
	    )
	);
	return $rules;
    }

    public function index() {
	$this->load->library('session');
	$array = array();
	$reset_key = "";
	$tmp_url = "";
	$i = 0;
	$this->data['form_validation'] = "No";
	$this->data['siteinfos'] = $this->reset_m->get_admin();

	$number1 = rand(1, 9);
	$number2 = rand(1, 9);

	$this->data['firstnumber'] = $number1;
	$this->data['secondnumber'] = $number2;

	!isset($_POST) ? $this->load->view('_layout_reset', $this->data) : '';


	if ($_POST) {

	    $rules = $this->rules_email();
	    $this->form_validation->set_rules($rules);

	    $first_number = $this->input->post('firstNumber');
	    $second_number = $this->input->post('secondNumber');
	    $capture_number = $this->input->post('answer');

	    $sum = $first_number + $second_number;



	    if ($this->form_validation->run() == FALSE) {
		$this->data['form_validation'] = validation_errors();

		$this->data["subview"] = 'reset/index';
		$this->load->view('_layout_reset', $this->data);
	    } else if ($capture_number == $sum) {
		// we have validated that this is a valid user, so let us reset pswd
		$phone = $this->input->post('phone');
		//$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
		$users = $this->user_m->get_username_row('users', array("phone" => $phone));

		//foreach ($tables as $table) {
		//$dbuser = $this->reset_m->get_table_users('users', $email);

		if (!empty($users)) {
		    $reset_key = $this->reset_m->hash($users->usertype . $users->username . $users->name . time());
		    $tmp_url = base_url("reset/password/" . $reset_key);
		    $array['permition'][$i] = 'yes';
		} else {
		    $array['permition'][$i] = 'no';
		}
		$i++;
		//}

		if (in_array('yes', $array['permition'])) {
		    $dbreset = $this->reset_m->get_reset();
		    $code = random_string();
		    if (!empty($dbreset)) {
			if ($this->db->truncate('reset')) {
			    $this->reset_m->insert_reset(array('keyID' => $reset_key, 'email' => $users->email, 'code' => $code));
			} else {
			    $this->session->set_flashdata('reset_error', 'reset access off!');
			}
		    } else {
			$this->reset_m->insert_reset(array('keyID' => $reset_key, 'email' => $users->email, 'code' => $code));
		    }

		    $this->email->from($this->data['siteinfos']->email, $this->data['siteinfos']->sname);
		    $this->email->to($users->email);
		    $this->email->subject('Reset Password');
		    $message = 'Click on the link below to reset your password -> ' . $tmp_url;
		    $this->email->message($message);
		    $headers = 'From: Grace prayer' . "\r\n";
		    $headers .= "Reply-To: noreply@shulesoft.com\n";
		    $headers .= "X-Mailer: PHP/" . phpversion() . "\n";
		    $headers .= "MIME-Version: 1.0\n";
		    $headers .= "Content-Type: text/html; charset=iso-8859-1";
		    $send = mail($users->email, 'Reset Password', $message);

		    //send SMS for user input codes

		    $verification_message = 'Hello ' . $users->name . '. Password verification code is ' . $code;
		    $status = $this->db->insert("sms", array('phone_number' => $phone, 'body' => $verification_message));
		    if ($status) {
			$data = json_encode(array('success' => 1, 'message' => 'sent'));
		    } else {
			$data = json_encode(array('success' => 0, 'message' => 'sent'));
		    }

		    if ($send && $status) {
			$this->data['reset_send']='Email & SMS sent! . Wait for few seconds to receive SMS';
			$this->data["subview"] = 'reset/enter_code';
			$this->load->view('_layout_reset', $this->data);
		    } else {
			$this->session->set_flashdata('reset_error', 'Email & SMS could not be Sent!');
			$this->load->helper("url");
			redirect(base_url("reset/index"));
		    }
		} else {
		    $this->session->set_flashdata('reset_error', 'Phone number is not found!');
		    $this->load->helper("url");
		    redirect(base_url("reset/index"));
		}
	    }
	} else {
	    $this->data["subview"] = 'reset/index';
	    $this->load->view('_layout_reset', $this->data);
	}
    }

    public function addcodes() {
	$this->load->model("reset_m");
	$code = $this->input->post('codes');
	if (!empty($code)) {
	    $dbreset = $this->reset_m->get_single_reset(array('code' => $code));
	    if (!empty($dbreset)) {
		$this->load->helper("url");
		redirect(base_url("reset/password/" . $dbreset->keyID));
	    } else {
		$this->data['reset_error'] = 'This code is not valid. Please enter a valid code';
		$this->data["subview"] = 'reset/enter_code';
		$this->load->view('_layout_reset', $this->data);
	    }
	} else {
	    $this->data['reset_error'] = 'Please write code you have received first';
	    $this->data["subview"] = 'reset/enter_code';
	    $this->load->view('_layout_reset', $this->data);
	}
    }

    public function password() {
	$this->load->model("reset_m");
	$this->load->library('session');
	$array = array();
	$i = 0;
	$key = $this->uri->segment(3);
	$this->data['siteinfos'] = $this->reset_m->get_admin();
	if (!empty($key)) {
	    $dbreset = $this->reset_m->get_single_reset(array('keyID' => $key));
	    if (!empty($dbreset)) {

		$this->data['form_validation'] = "No";
		$this->data['siteinfos'] = $this->reset_m->get_admin();
		if ($_POST) {
		    $rules = $this->rules();
		    $this->form_validation->set_rules($rules);
		    if ($this->form_validation->run() == FALSE) {
			$this->data['form_validation'] = validation_errors();
			$this->data["subview"] = "reset/add";
			$this->load->view('_layout_reset', $this->data);
		    } else {
			$password = $this->input->post('newpassword');
			$email = $dbreset->email;

			$tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
			foreach ($tables as $table) {
			    $dbuser = $this->reset_m->get_table_users($table, $email);
			    if (count($dbuser)) {
				$data = array('password' => $this->reset_m->hash($password));
				$this->db->update($table, $data, "email = '" . $email . "'");
				$this->session->set_flashdata('reset_success', 'Password Reset Success!');
				$this->db->truncate('reset');
				$array['permition'][$i] = 'yes';
			    } else {
				$array['permition'][$i] = 'no';
			    }
			    $i++;
			}

			if (in_array('yes', $array['permition'])) {
			    redirect(base_url("signin/index"));
			}
		    }
		} else {
		    $this->data["subview"] = "reset/add";
		    $this->load->view('_layout_reset', $this->data);
		}
	    } else {
		echo "<p> Session Out </p>";
	    }
	} else {
	    echo "<p> Session Out </p>";
	}
    }

}

/* End of file class.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/class.php */