<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of fake
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
class FakeData extends Admin_Controller {

    public $faker;
    public $sex = array('Male', 'Female');
    public $class = array('one', 'two', 'three', 'four', 'five', 'six');
    public $designation = array(
	'History', 'Geography', 'Chemestry', 'Mathematics', 'English', 'Kiswahili', 'Physics', 'Economics', 'Biology', 'Agriculture'
    );

    function __construct() {
	parent::__construct();
	$this->load->library('Faker-master/fake');
	$this->faker = Faker\Factory::create();
    }

    function alter_table() {
	$tables = $this->db->query("SELECT * FROM pg_catalog.pg_tables where schemaname='public'")->result();
	foreach ($tables as $table) {
	    $check = $this->db->query("SELECT column_name  
					FROM information_schema.columns 
			    WHERE table_name='" . $table->tablename . "' and column_name='created_at';")->result();

	    if (empty($check)) {
		$this->db->query("ALTER TABLE " . $table->tablename . " ADD COLUMN					created_at timestamp without time zone DEFAULT now();");
	    }
	}
    }

    public function add_data() {
	/*
	 * 1. Add teachers
	 * 2. Add class
	 * 3. Add section
	 * 4. Add parents
	 * 5. Add student
	 */

	$section_id = rand(1, 37);
	$class_id = rand(43, 46);
	$parent_id = rand(2, 84);
	$this->new_section($class_id, rand(2, 41));
	//

		$this->add_teacher();
		$this->add_grade();
		$this->add_student($section_id, $class_id, $parent_id, 20);
		$this->add_class(6);
		$this->add_subject();
		$this->add_parent();
		echo "Ok";


    }

    function edit_data() {
	
	$section = array('A', 'B', 'C');
	$students = $this->db->query("select * from student")->result();
	foreach ($students as $student) {
	    $this->db->update('student', array(
		'classesID' => $classesID[rand(0, 5)],
		'section' => $section[rand(0, 2)]
		    ), array('studentID' => $student->studentID));
	}
    }

    function add_grade() {
	$section = array('A'=>80, 'B'=>60, 'C'=>40,'D'=>20,'F'=>0);
	foreach ($section as $key=>$value) {
	    $this->db->insert('grade', array(
		'grade' =>$key,
		'point' => $value,
		'gradefrom' => $value,
		'gradeupto' => $value+20,
		'note'=>$key
	    ));
	}
    }
    function add_subject() {

	$teachers = $this->db->query("select * from teacher")->result();
	foreach ($teachers as $teacher) {
	    $classesID = array('29', '30', '33', '34', '36', '42');
	    $this->db->insert('subject', array(
		'subject' => $teacher->designation,
		'teacherID' => $teacher->teacherID,
		'subject_author' => $this->faker->name,
		'subject_code' => substr($teacher->designation,0, 2),
		'classesID' => $classesID[rand(0, 5)],
		'teacher_name' => $teacher->name
	    ));
	} 
    }

    function add_class($limit) {
	for ($i = 1; $i <= $limit; $i++) {
	    $number = rand(0, 5);
	    $name = $this->class[$number];

	    $this->db->insert('classes', array(
		'classes' => 'Form ' . $name,
		'classes_numeric' => $number + 1,
		'teacherID' => rand(2, 41),
		'note' => 'o-level class ' . $name
	    ));
	}
    }

    function new_section($class_id, $teacher_id) {
	$section = array('A', 'B', 'C');
	$number = rand(0, 2);
	if ($number == 0) {
	    $category = 'best students';
	} else if ($number == 1) {
	    $category = 'performed students';
	} else {
	    $category = 'low performed students';
	}
	$this->db->insert('section', array(
	    'section' => $section[$number],
	    'category' => $category,
	    'classesID' => $class_id,
	    'teacherID' => $teacher_id
	));
    }

    function add_student($section_id, $class_id, $parent_id, $limit) {
	for ($i = 1; $i <= $limit; $i++) {
	    $section = array('A', 'B');
	    $this->db->insert('student', array(
		'name' => $this->faker->name,
		'dob' => $this->faker->date(),
		'sex' => $this->sex[rand(0, 1)],
		'religion' => 'christianity',
		'email' => $this->faker->email,
		'phone' => $this->faker->phoneNumber,
		'address' => $this->faker->address,
		'sectionID' => $section_id,
		'section' => $section[rand(0, 1)],
		'roll' => $this->faker->unique()->numberBetween(100, 999),
		'library' => 0,
		'hostel' => 0,
		'transport' => 0,
		'create_date' => $this->faker->date(),
		'classesID' => $class_id,
		'parentID' => $parent_id,
		'email' => $this->faker->email,
		'phone' => $this->faker->phoneNumber,
		'username' => $this->faker->unique()->userName,
		'password' => hash("sha512", 'password' . config_item("encryption_key")),
		'usertype' => 'Student'
	    ));
	}
    }

    function add_teacher($limit = NULL) {

	for ($i = 1; $i <= $limit; $i++) {
	    $this->db->insert('teacher', array(
		'name' => $this->faker->name,
		'designation' => $this->designation[rand(0, 9)],
		'dob' => $this->faker->date(),
		'sex' => $this->sex[rand(0, 1)],
		'religion' => 'christianity',
		'email' => $this->faker->email,
		'phone' => $this->faker->phoneNumber,
		'jod' => $this->faker->date(),
		'address' => $this->faker->address,
		'username' => $this->faker->unique()->userName,
		'password' => hash("sha512", 'password' . config_item("encryption_key")),
		'usertype' => 'Teacher'
	    ));
	}
    }

    function add_parent($limit = NULL) {
	for ($i = 1; $i <= $limit; $i++) {
	    $this->db->insert('parent', array(
		'name' => $this->faker->name,
		'father_name' => $this->faker->firstNameMale . ' ' . $this->faker->lastName,
		'mother_name' => $this->faker->firstNameFemale . ' ' . $this->faker->lastName,
		'father_profession' => $this->faker->titleMale,
		'mother_profession' => $this->faker->titleFemale,
		'email' => $this->faker->email,
		'phone' => $this->faker->phoneNumber,
		'address' => $this->faker->address,
		'photo' => $this->faker->imageUrl(),
		'username' => $this->faker->unique()->userName,
		'password' => hash("sha512", 'password' . config_item("encryption_key")),
		'usertype' => 'Parent'
	    ));
	}
    }

}
