<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Feetype extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
	parent::__construct();
	$this->load->model("feetype_m");
	$language = $this->session->userdata('lang');
	$this->lang->load('feetype', $language);
    }

    public function index() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Accountant") {
	    $this->data['feetypes'] = $this->feetype_m->get_feetype();
	    $this->data["subview"] = "feetype/index";
	    $this->load->view('_layout_main', $this->data);
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    protected function rules() {
	$rules = array(
	    array(
		'field' => 'feetype',
		'label' => $this->lang->line("feetype_name"),
		'rules' => 'trim|required|xss_clean|max_length[60]|callback_unique_feetype'
	    ),
//              array(
//		'field' => 'amount',
//		'label' => $this->lang->line("feetype_amount"),
//		'rules' => 'trim|required|xss_clean|numeric|callback_unique_feetype'
//	    ),
//              array(
//		'field' => 'date',
//		'label' => $this->lang->line("feetype_date"),
//		'rules' => 'trim|required|xss_clean|callback_unique_feetype'
//	    ),
	    array(
		'field' => 'note',
		'label' => $this->lang->line("feetype_note"),
		'rules' => 'trim|xss_clean|max_length[200]'
	    )
	     
	);
	return $rules;
    }
      protected function feecat_rules() {
	$rules = array(
	    array(
		'field' => 'category_name',
		'label' => $this->lang->line("category_name"),
		'rules' => 'trim|required|xss_clean|max_length[250]'
	    ),
	    array(
		'field' => 'category_note',
		'label' => $this->lang->line("category_note"),
		'rules' => 'trim|xss_clean|max_length[200]'
	    )
	     
	);
	return $rules;
    }

    public function add() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Accountant") {
	    if ($_POST) {
		$rules = $this->rules();
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE) {
		    $this->data["subview"] = "feetype/add";
		    $this->load->view('_layout_main', $this->data);
		} else {
		    $array = array(
			"feetype" => $this->input->post("feetype"),
                        "bank_account_id" => $this->input->post("bank_account_id"),
                         "feetype_categoryID" => $this->input->post("feetype_categoryID"),
			"note" => $this->input->post("category_note"),
			'amount' => $this->input->post("amount"),
			"is_repeative" => $this->input->post("is_repeative"),
			"startdate" => date("Y-m-d", strtotime($this->input->post("startdate"))),
			"enddate" => date("Y-m-d", strtotime($this->input->post("enddate")))
		    );
		    $this->feetype_m->insert_feetype($array);
		    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
		    redirect(base_url("feetype/index"));
		}
	    } else {
		$this->data["subview"] = "feetype/add";
                $this->data['account_numbers'] = $this->feetype_m->get_allacc_number();
                   $this->data['feetype_category'] = $this->feetype_m->feetype_category_all();
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }
    
    public function add_feecategory() {
   	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Accountant") {
	    if ($_POST) {               
                $rules = $this->feecat_rules();
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE) {
		   $this->data["subview"] = "feetype/feetype_category/add";
		$this->load->view('_layout_main', $this->data);
		} else {
	       {
		    $array = array(
			"feetype_name" => $this->input->post("category_name"),
			"note" => $this->input->post("note"),
                        "created_date"=>date_create('now')->format('Y-m-d H:i:s')
		    );
		    $this->feetype_m->insert_feetype_category('feetype_category',$array);
                    
		    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
		    redirect(base_url("feetype/add_feecategory"));
		}
	    } 
            } else {
		$this->data["subview"] = "feetype/feetype_category/add";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}           
    }

    public function edit() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Accountant") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->data['feetype'] = $this->feetype_m->get_feetype($id);
		if ($this->data['feetype']) {
		    if ($_POST) {
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
			    $this->data["subview"] = "feetype/edit";
			    $this->load->view('_layout_main', $this->data);
			} else {
			    $array = array(
				"feetype" => $this->input->post("feetype"),
                                "amount" => $this->input->post("amount"),
                                "enddate" => date("Y-m-d", strtotime($this->input->post("enddate"))),
                                "startdate" => date("Y-m-d", strtotime($this->input->post("startdate"))),
                                "bank_account_id" => $this->input->post("bank_account_id"),
				"note" => $this->input->post("note")
			    );
                            $id=$this->input->post("feetypeID");
                            
			    $this->db->where('feetypeID',$id);
                            $this->db->update('feetype',$array);
			    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
			    redirect(base_url("feetype/index"));
			}
		    } else {
                        $this->data['account_numbers'] = $this->feetype_m->get_allacc_number();
			$this->data["subview"] = "feetype/edit";
			$this->load->view('_layout_main', $this->data);
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function delete() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Accountant") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->feetype_m->delete_feetype($id);
		$this->session->set_flashdata('success', $this->lang->line('menu_success'));
		redirect(base_url("feetype/index"));
	    } else {
		redirect(base_url("feetype/index"));
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function unique_feetype() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $feetype = $this->feetype_m->get_order_by_feetype(array("feetype" => $this->input->post("feetype"), "feetypeID !=" => $id));
	    if (count($feetype)) {
		$this->form_validation->set_message("unique_feetype", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	} else {
	    $feetype = $this->feetype_m->get_order_by_feetype(array("feetype" => $this->input->post("feetype")));

	    if (count($feetype)) {
		$this->form_validation->set_message("unique_feetype", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	}
    }

}

/* End of file feetype.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/feetype.php */