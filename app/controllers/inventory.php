<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Inventory extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
	parent::__construct();
	$language = $this->session->userdata('lang');
	$this->lang->load('inventory', $language);
    }

    public function vendor_add() {
	$this->data["subview"] = "inventory/add_vendor";
	$this->load->view('_layout_main', $this->data);
    }

    public function index() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Teacher") {
	    $id = htmlentities($this->uri->segment(3));
	    if ((int) $id) {
		$this->data['set'] = $id;
		$this->data['item'] = $this->item_m->get_item($id);
		;
		$this->data["subview"] = "inventory/view";
		$this->load->view('_layout_main', $this->data);
	    } else {
		$this->data['items'] = $this->item_m->get_item();
		$this->data["subview"] = "inventory/index";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    protected function rules() {
	$rules = array(
	    array(
		'field' => 'name',
		'label' => $this->lang->line("name"),
		'rules' => 'trim|required|xss_clean|max_length[90]'
	    ),
	    array(
		'field' => 'date_purchased',
		'label' => $this->lang->line("date_purchased"),
		'rules' => 'trim|required|xss_clean'
	    ),
	    array(
		'field' => 'vendor_name',
		'label' => $this->lang->line("vendor_name"),
		'rules' => 'trim|required|max_length[250]|xss_clean'
	    ),
	    array(
		'field' => 'batch_number',
		'label' => $this->lang->line("batch_number"),
		'rules' => 'trim|max_length[40]|xss_clean'
	    ),
	    array(
		'field' => 'comments',
		'label' => $this->lang->line("comments"),
		'rules' => 'trim|required|max_length[250]|xss_clean'
	    ),
	    array(
		'field' => 'contact_person_number',
		'label' => $this->lang->line("contact_person_number"),
		'rules' => 'trim|max_length[90]|min_length[5]|xss_clean'
	    ),
	    array(
		'field' => 'status',
		'label' => $this->lang->line("status"),
		'rules' => 'trim|max_length[20]|xss_clean'
	    ),
	    array(
		'field' => 'price',
		'label' => $this->lang->line("price"),
		'rules' => 'trim|required|numeric|max_length[20]|xss_clean'
	    ),
	    array(
		'field' => 'quantity',
		'label' => $this->lang->line("quantity"),
		'rules' => 'trim|required|max_length[11]|xss_clean|numeric'
	    ),
	    array(
		'field' => 'contact_person_name',
		'label' => $this->lang->line("contact_person_name"),
		'rules' => 'trim|max_length[150]|xss_clean'
	    ),
	    array(
		'field' => 'depreciation',
		'label' => $this->lang->line("depreciation"),
		'rules' => 'trim|required|max_length[40]|xss_clean'
	    )
	);
	return $rules;
    }

    public function add() {

	$usertype = $this->session->userdata("usertype");
	$setting = $this->setting_m->get_setting(1);

	if ($usertype == "Admin") {
	    $this->data['vendors'] = $this->vendor_m->get_vendor();
	    if ($_POST) {
		$rules = $this->rules();
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() != FALSE) {
		    $this->data["subview"] = "inventory/add";
		    $this->load->view('_layout_main', $this->data);
		} else {
		    $this->db->insert('item', $_POST);
		    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
		    redirect(base_url("inventory/index"));
		}
	    } else {
		$this->data["subview"] = "inventory/add";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function edit() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->data['item'] = $this->item_m->get_item($id);
		$this->data['vendors'] = $this->vendor_m->get_vendor();
		$this->data['set'] = '';
		if ($this->data['item']) {
		    if ($_POST) {
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() != FALSE) {
			    $this->data["subview"] = "inventory/edit";
			    $this->load->view('_layout_main', $this->data);
			} else {
			    $this->item_m->update_item($_POST, $id);
			    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
			    redirect(base_url("inventory/index"));
			}
		    } else {
			$this->data["subview"] = "inventory/edit";
			$this->load->view('_layout_main', $this->data);
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function delete() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->item_m->delete_item($id);
		$this->session->set_flashdata('success', $this->lang->line('menu_success'));
		redirect(base_url("inventory/index"));
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	}
    }

}