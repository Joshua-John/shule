<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Parentes extends Admin_Controller {

    /**
     * -----------------------------------------
     *
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     *
     *
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     *
     *
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    public function __construct() {
	parent::__construct();
	$language = $this->session->userdata('lang');
	$this->lang->load('parentes', $language);
	$this->lang->load('email', $language);
    }

    public function index() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype=='Secretary') {
	    $this->data['classes'] = $this->student_m->get_classes();
	    $id = htmlentities($this->uri->segment(3));
	    if ((int) $id) {
		$this->data['set'] = $id;
		$this->data['parentes'] = $id == date('Y') ? $this->parentes_m->get_parentes() :
			$this->parentes_m->get_parent_by_class($id);
		$this->data["subview"] = "parentes/index";
		$this->load->view('_layout_main', $this->data);
	    } else {
		$this->data["subview"] = "parentes/search";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function parentes_list() {
	$classID = $this->input->post('id');
	if ((int) $classID) {
	    $string = base_url("parentes/index/$classID");
	    echo $string;
	} else {
	    redirect(base_url("parents/index"));
	}
    }

    protected function rules() {
	$rules = array(
	    array(
		'field' => 'name',
		'label' => $this->lang->line("parentes_guargian_name"),
		'rules' => 'trim|required|xss_clean|max_length[60]'
	    ),
	    array(
		'field' => 'father_name',
		'label' => $this->lang->line("parentes_father_name"),
		'rules' => 'trim|xss_clean|max_length[60]'
	    ),
	    array(
		'field' => 'mother_name',
		'label' => $this->lang->line("parentes_mother_name"),
		'rules' => 'trim|xss_clean|max_length[60]'
	    ),
	    array(
		'field' => 'father_profession',
		'label' => $this->lang->line("parentes_father_name"),
		'rules' => 'trim|xss_clean|max_length[40]'
	    ),
	    array(
		'field' => 'mother_profession',
		'label' => $this->lang->line("parentes_mother_name"),
		'rules' => 'trim|xss_clean|max_length[40]'
	    ),
            array(
		'field' => 'employer',
		'label' => $this->lang->line("employer"),
		'rules' => 'trim|xss_clean|max_length[40]'
	    ),
	    array(
		'field' => 'email',
		'label' => $this->lang->line("parentes_email"),
		'rules' => 'trim|required|max_length[40]|valid_email|xss_clean|callback_unique_email'
	    ),
	    array(
		'field' => 'phone',
		'label' => $this->lang->line("parentes_phone"),
		'rules' => 'trim|min_length[5]|max_length[25]|xss_clean'
	    ),
	    array(
		'field' => 'address',
		'label' => $this->lang->line("parentes_address"),
		'rules' => 'trim|max_length[200]|xss_clean'
	    ),
	    array(
		'field' => 'photo',
		'label' => $this->lang->line("parentes_photo"),
		'rules' => 'trim|max_length[200]|xss_clean'
	    )
	);
	return $rules;
    }

    public function add() {
	$setting = $this->setting_m->get_setting(1);
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype=='Secretary') {

	    if ($_POST) {
		$rules = $this->rules();
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE) {
		    $this->data['form_validation'] = validation_errors();
		    $this->data["subview"] = "parentes/add";
		    $this->load->view('_layout_main', $this->data);
		} else {
		    $array = array();
		    for ($i = 0; $i < count($rules); $i++) {
			$array[$rules[$i]['field']] = $this->input->post($rules[$i]['field']);
		    }
		    $array['username'] = $this->input->post("phone");
		    $array['password'] = $this->student_m->hash("123456");
		    $array['usertype'] = "Parent";

		    $new_file = "defualt.png";
		    if ($_FILES["image"]['name'] != "") {
			$file_name = $_FILES["image"]['name'];
			$file_name_rename = rand(1, 100000000000);
			$explode = explode('.', $file_name);
			if (count($explode) >= 2) {
			    $upload = $this->uploadImage();
			    $array['photo'] = $upload;

			    if ($upload == FALSE) {
				$this->data["image"] = $this->upload->display_errors();
				$this->data["subview"] = "parentes/add";
				$this->load->view('_layout_main', $this->data);
			    } else {
				$data = array("upload_data" => $this->upload->data());
				$this->parentes_m->insert_parentes($array);
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				//send email

				$message = sprintf($this->lang->line('add_new_parent'), $this->input->post('name'), $setting->sname, $this->input->post('phone'), '123456');
				$this->send_email($this->input->post("email"), $setting->sname . ' Online Login Information', $message);
				//send sms
				$this->send_sms($this->input->post("phone"), $message);
				redirect(base_url("parentes/index"));
			    }
			} else {
			    $this->data["image"] = "Invalid file";
			    $this->data["subview"] = "parentes/add";
			    $this->load->view('_layout_main', $this->data);
			}
		    } else {
			$array["photo"] = $new_file;
			$this->parentes_m->insert_parentes($array);
			$this->session->set_flashdata('success', $this->lang->line('menu_success'));

			//send email
			$message = sprintf($this->lang->line('add_new_parent'), $this->input->post('name'), $setting->sname, $this->input->post('phone'), '123456');
			$this->send_email($this->input->post("email"), $setting->sname . ' Online Login Information', $message);
			$this->send_sms($this->input->post("phone"), $message);
			redirect(base_url("parentes/index"));
		    }
		}
	    } else {
		$this->data["subview"] = "parentes/add";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function edit() {
	$setting = $this->setting_m->get_setting(1);
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Parent" || $usertype=='Secretary') {
	    $id = htmlentities(($this->uri->segment(3)));

	    if ((int) $id) {
		$this->data['parentes'] = $this->parentes_m->get_parentes($id);
		if ($this->data['parentes']) {
		    if ($_POST) {
			$rules = $this->rules();
			unset($rules[8], $rules[9], $rules[10]);
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
			    $this->data["subview"] = "parentes/edit";
			    $this->load->view('_layout_main', $this->data);
			} else {

			    $array = array();
			    for ($i = 0; $i < count($rules); $i++) {
				$array[$rules[$i]['field']] = $this->input->post($rules[$i]['field']);
			    }

			    if ($_FILES["image"]['name'] != "") {
				$file_name = $_FILES["image"]['name'];
				$file_name_rename = rand(1, 100000000000);
				$explode = explode('.', $file_name);
				if (count($explode) >= 2) {
				    $upload = $this->uploadImage();
				    $array['photo'] = $upload;
				    if ($upload == FALSE) {
					$this->data["image"] = $this->upload->display_errors();
					$this->data["subview"] = "parentes/edit";
					$this->load->view('_layout_main', $this->data);
				    } else {
					$data = array("upload_data" => $this->upload->data());
					$this->parentes_m->update_parentes($array, $id);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));

					//send email
					$message = sprintf($this->lang->line('edit_parent'), $this->input->post('name'), $this->input->post('phone'), 'parent123');

					$this->send_email($this->input->post("email"), $setting->sname . ' Online Login Information', $message);
					//send sms
					//$this->send_sms($this->input->post("phone"), $message);

					redirect(base_url("parentes/index"));
				    }
				} else {
				    $this->data["image"] = "invalid file";
				    $this->data["subview"] = "parentes/edit";
				    $this->load->view('_layout_main', $this->data);
				}
			    } else {
				$this->parentes_m->update_parentes($array, $id);
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));

				//send email
				$message = sprintf($this->lang->line('edit_parent'), $this->input->post('name'), $this->input->post('phone'), 'parent123');

				$this->send_email($this->input->post("email"), $setting->sname . ' Online Login Information', $message);
				//send sms
				//$this->send_sms($this->input->post("phone"), $message);

				$usertype == 'Admin' ?
						redirect(base_url("parentes/index")) :
						redirect(base_url("profile/index"));
			    }
			}
		    } else {
			$this->data["subview"] = "parentes/edit";
			$this->load->view('_layout_main', $this->data);
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function view() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype=='Secretary') {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->data['parentes'] = $this->parentes_m->get_parentes($id);
		$this->data['students'] = $this->student_m->get_order_by_student(array('parentID' => $id));
		if ($this->data['parentes']) {
		    $this->data["subview"] = "parentes/view";
		    $this->load->view('_layout_main', $this->data);
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function print_preview() {
	$id = htmlentities(($this->uri->segment(3)));
	$url = htmlentities(($this->uri->segment(4)));

	if ((int) $id) {
	    $usertype = $this->session->userdata('usertype');
	    if ($usertype == "Admin") {
		$this->load->library('html2pdf');
		$this->html2pdf->folder('./assets/pdfs/');
		$this->html2pdf->filename('Report.pdf');
		$this->html2pdf->paper('a4', 'portrait');

		$this->data['parentes'] = $this->parentes_m->get_parentes($id);
		if ($this->data['parentes']) {
		    $this->data['panel_title'] = $this->lang->line('panel_title');
		    $html = $this->load->view('parentes/print_preview', $this->data, true);
		    $this->html2pdf->html($html);
		    $this->html2pdf->create();
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function send_mail() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = $this->input->post('id');
	    if ((int) $id) {
		$this->data['parentes'] = $this->parentes_m->get_parentes($id);
		if ($this->data['parentes']) {
		    $html = $this->load->view('parentes/print_preview', $this->data, true);
		    $this->load->library('html2pdf');
		    $this->html2pdf->folder('uploads/report');
		    $this->html2pdf->filename('Report.pdf');
		    $this->html2pdf->paper('a4', 'portrait');
		    $this->data['panel_title'] = $this->lang->line('panel_title');
		    $this->html2pdf->html($html);
		    $this->html2pdf->create('save');

		    if ($path = $this->html2pdf->create('save')) {
			$this->load->library('email');
			$this->email->set_mailtype("html");
			$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);
			$this->email->to($this->input->post('to'));
			$this->email->subject($this->input->post('subject'));
			$this->email->message($this->input->post('message'));
			$this->email->attach($path);
			if ($this->email->send()) {
			    $this->session->set_flashdata('success', $this->lang->line('mail_success'));
			} else {
			    $this->session->set_flashdata('error', $this->lang->line('mail_error'));
			}
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function delete() {
	$setting = $this->setting_m->get_setting(1);
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    $parent_id = $this->parentes_m->get_single_parentes(array("parentID" => $id));

	    if ((int) $id) {

		$this->parentes_m->delete_parentes($id);
		$this->session->set_flashdata('success', $this->lang->line('menu_success'));
		$message = sprintf($this->lang->line('delete_parent'), $parent_id->name, $setting->sname);

		$this->send_email($parent_id->email, $setting->sname, $message);
		redirect(base_url("parentes/index"));
	    } else {
		redirect(base_url("parentes/index"));
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function unique_email() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $user_info = $this->parentes_m->get_single_parentes(array('parentID' => $id));
	    $tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
	    $array = array();
	    $i = 0;
	    foreach ($tables as $table) {
		$user = $this->parentes_m->get_username($table, array("email" => $this->input->post('email'), 'username !=' => $user_info->username));
		if (count($user)) {
		    $this->form_validation->set_message("unique_email", "%s already exists");
		    $array['permition'][$i] = 'no';
		} else {
		    $array['permition'][$i] = 'yes';
		}
		$i++;
	    }
	    if (in_array('no', $array['permition'])) {
		return FALSE;
	    } else {
		return TRUE;
	    }
	} else {
	    $tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
	    $array = array();
	    $i = 0;
	    foreach ($tables as $table) {
		$user = $this->student_m->get_username($table, array("email" => $this->input->post('email')));
		if (count($user)) {
		    $this->form_validation->set_message("unique_email", "%s already exists");
		    $array['permition'][$i] = 'no';
		} else {
		    $array['permition'][$i] = 'yes';
		}
		$i++;
	    }

	    if (in_array('no', $array['permition'])) {
		return FALSE;
	    } else {
		return TRUE;
	    }
	}
    }

    public function lol_username() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
	    $array = array();
	    $i = 0;
	    foreach ($tables as $table) {
		$user = $this->student_m->get_username($table, array("username" => $this->input->post('username'), $table . "ID !=" => $id));
		if (count($user)) {
		    $this->form_validation->set_message("lol_username", "%s already exists");
		    $array['permition'][$i] = 'no';
		} else {
		    $array['permition'][$i] = 'yes';
		}
		$i++;
	    }
	    if (in_array('no', $array['permition'])) {
		return FALSE;
	    } else {
		return TRUE;
	    }
	} else {
	    $tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
	    $array = array();
	    $i = 0;
	    foreach ($tables as $table) {
		$user = $this->student_m->get_username($table, array("username" => $this->input->post('username')));
		if (count($user)) {
		    $this->form_validation->set_message("lol_username", "%s already exists");
		    $array['permition'][$i] = 'no';
		} else {
		    $array['permition'][$i] = 'yes';
		}
		$i++;
	    }

	    if (in_array('no', $array['permition'])) {
		return FALSE;
	    } else {
		return TRUE;
	    }
	}
    }

       public function export() {
	$id = htmlentities($this->uri->segment(3));
	// get all users in array format
	$users = $this->parentes_m->get_data_toexport();
	$keys=(array)array_shift($users);
	$title = array_keys($keys);
	$this->exportExcel($users, $title, 'parents');
    }
    
    private function checkKeysExists($value) {
	$required = array('phone', 'other_phone', 'guardian_name', 'father_name', 'mother_name',
	    'father_job', 'mother_job', 'email', 'address');
	$data = array_shift($value);
	if (count(array_intersect_key(array_flip($required), $data)) === count($required)) {
	    //All required keys exist! 
	    $status = 1;
	} else {
	    $missing = array_intersect_key(array_flip($required), $data);
	    $data_miss = array_diff(array_flip($required), $missing);
	    $status = ' Column with title <b>' . implode(', ', array_keys($data_miss)) . '</b>  miss from Excel file. Please make sure file is in the same format as a sample file';
	}
	return $status;
    }

    public function uploadByFile() {
	$data = $this->uploadExcel();

	$status = $this->checkKeysExists($data);
	if ((int) $status == 1) {
	    $status = '';
	    $password = $this->student_m->hash("123456");
	    foreach ($data as $value) {

		/*
		 * We need to check first if that parent exist
		 * 
		 * WE have forced to place 0 before, but this will be removed once
		 * we allow country codes to be in the system
		 */
		$phone = '0' . preg_replace('/[^a-zA-Z0-9 ]/', '', $value['phone']);
		$other_phone = '0' . preg_replace('/[^a-zA-Z0-9 ]/', '', $value['other_phone']);

		$parent_info1 = $this->parentes_m->get_single_parentes(array('phone' => $phone, 'other_phone' => $other_phone));
		$parent_info2 = $this->parentes_m->get_single_parentes(array('phone' => $other_phone, 'other_phone' => $phone));


		if (empty($parent_info1) && empty($parent_info2)) {
		    $email = $value['email'] == '' ? str_replace(' ', '.', $value['guardian_name']) . '@shulesoft.com' : $value['email'];
		    $array = array(
			"name" => $value['guardian_name'],
			"father_name" => $value['father_name'],
			"mother_name" => $value['mother_name'],
			"father_profession" => $value['father_job'],
			"mother_profession" => $value['mother_job'],
			"email" => $email,
			"phone" => $phone,
			"username" => $phone,
			"usertype" => "Parent",
			"address" => $value['address'],
			"password" => $password,
			"other_phone" => $value['other_phone'],
			"photo" => "defualt.png"
		    );
		    $this->parentes_m->insert_parentes($array);

		    $status .= '<p class="alert alert-success">Guardian "' . $value['guardian_name'] . '" uploaded successfully</p>';
		} else {
		    $status .= '<p class="alert alert-info">Guardian "' . $value['guardian_name'] . '" exist</p>';
		}
	    }
	}
	$this->session->set_flashdata('success', $this->lang->line('menu_success'));
	$this->data['status'] = $status;
	$this->data["subview"] = "mark/upload_status";
	$this->load->view('_layout_main', $this->data);
    }

}

/* End of file parentes.php */
    /* Location: .//D/xampp/htdocs/school/mvc/controllers/parentes.php */