<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class setting extends Admin_Controller {

    /**
     * -----------------------------------------
     *
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     *
     *
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     *
     *
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
	parent::__construct();
	$this->load->model("setting_m");
	$language = $this->session->userdata('lang');
	$this->lang->load('setting', $language);
    }

    protected function rules() {
	$rules = array(
	    array(
		'field' => 'sname',
		'label' => $this->lang->line("setting_school_name"),
		'rules' => 'trim|required|xss_clean|max_length[128]'
	    ),
	    array(
		'field' => 'phone',
		'label' => $this->lang->line("setting_school_phone"),
		'rules' => 'trim|required|xss_clean|max_length[45]'
	    ),
	    array(
		'field' => 'email',
		'label' => $this->lang->line("setting_school_email"),
		'rules' => 'trim|required|valid_email|max_length[40]|xss_clean'
	    ),
	    array(
		'field' => 'automation',
		'label' => $this->lang->line("setting_school_automation"),
		'rules' => 'trim|required|max_length[2]|xss_clean|numeric|callback_unique_day'
	    ),
	    array(
		'field' => 'currency_code',
		'label' => $this->lang->line("setting_school_currency_code"),
		'rules' => 'trim|required|max_length[11]|xss_clean'
	    ),
	    array(
		'field' => 'currency_symbol',
		'label' => $this->lang->line("setting_school_currency_symbol"),
		'rules' => 'trim|required|max_length[3]|xss_clean'
	    ),
	    array(
		'field' => 'footer',
		'label' => $this->lang->line("setting_school_footer"),
		'rules' => 'trim|required|max_length[200]|xss_clean'
	    ),
	    array(
		'field' => 'address',
		'label' => $this->lang->line("setting_school_address"),
		'rules' => 'trim|required|max_length[200]|xss_clean'
	    ),
	    array(
		'field' => 'api_key',
		'label' => 'karibuSMS API KEY',
		'rules' => 'trim|required|max_length[200]|xss_clean'
	    ),
	    array(
		'field' => 'api_secret',
		'label' => 'karibuSMS API SECRET',
		'rules' => 'trim|required|max_length[200]|xss_clean'
	    ),
	    array(
		'field' => 'pass_mark',
		'label' => 'Exam Pass Mark',
		'rules' => 'trim|required|max_length[3]|xss_clean'
	    ),
	    array(
		'field' => 'website',
		'label' => 'School Website',
		'rules' => 'trim|required|max_length[120]|xss_clean'
	    ),
	    array(
		'field' => 'box',
		'label' => 'School P.O BOX',
		'rules' => 'trim|required|max_length[120]|xss_clean'
	    ),
	    array(
		'field' => 'motto',
		'label' => 'School Motto',
		'rules' => 'trim|required|max_length[250]|xss_clean'
	    )
	);
	return $rules;
    }

    public function index() {

	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $this->data['setting'] = $this->setting_m->get_setting(1);
	    $this->data['academic_years'] = $this->academic_year_m->get_all_years();
	    $this->data['class_levels'] = $this->classlevel_m->get_classlevel();
	    if ($this->data['setting']) {

		if ($_POST) {
		    $posted_data = array_keys($_POST);
		    $rules = $this->rules();

		    foreach ($rules as $key => $value) {
			if (!in_array($value['field'], $posted_data)) {
			    unset($rules[$key]);
			}
		    }
		    $this->form_validation->set_rules($rules);
		    if ($this->form_validation->run() == FALSE) {
			$this->data['form_validation'] = validation_errors();
			$this->data["subview"] = "setting/index";
			$this->load->view('_layout_main', $this->data);
		    } else {
			if ($this->pcode_validation($this->data['setting']->purchase_code) == FALSE) {
			    $this->session->set_flashdata('error', $this->lang->line('settings_pde'));
			    redirect(base_url('setting/index'));
			}
			$array = array();
			$rules = reset_keys($rules);

			for ($i = 0; $i < count($rules); $i++) {
			    $array[$rules[$i]['field']] = $this->input->post($rules[$i]['field']);
			}

			if ($_FILES["image"]['name'] != "") {
			    $file_name = $_FILES["image"]['name'];
			    $file_name_rename = rand(1, 100000000000);
			    $explode = explode('.', $file_name);
			    $new_file = $file_name_rename . '.' . $explode[1];

			    $config['upload_path'] = __DIR__ . "../../../uploads/images";
			    $config['allowed_types'] = "gif|jpg|png";
			    $config['file_name'] = $new_file;
			    $config['max_size'] = '1024';
			    $config['max_width'] = '3000';
			    $config['max_height'] = '3000';
			    $array['photo'] = $new_file;

			    //$this->load->library('upload', $config);
			    if (!$this->upload->do_upload("image")) {
				$this->data["image"] = $this->upload->display_errors();
				$this->data["subview"] = "setting/index";
				$this->load->view('_layout_main', $this->data);
			    } else {
				$data = array("upload_data" => $this->upload->data());
				$this->setting_m->update_setting($array, 1);
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				redirect(base_url("setting/index"));
			    }
			} else {
			    $this->setting_m->update_setting($array, 1);
			    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
			    redirect(base_url("setting/index"));
			}
		    }
		} else {
		    $this->data["subview"] = "setting/index";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function academic_year() {
	$data = array();
	parse_str($this->input->get_post('formdata'), $data);
	//check if data exist
	$check = $this->academic_year_m->get_single_year(array('name' => $data['name'], 'class_level_id' => $data['class_level_id']));
	if (empty($check)) {
	    $this->academic_year_m->insert_academic($data);
	    echo '1';
	} else {
	    echo 'Academic year NAME of the same class level already exist';
	}
    }

    public function edit_academic() {
	$id = htmlentities(($this->uri->segment(3)));
	$this->data['academic_year']=  $this->academic_year_m->get_single_year(array('id'=>$id));
	if (!empty($_POST)) {
	     $this->academic_year_m->update_academic($_POST,$id);
	     $this->session->set_flashdata('success', $this->lang->line('menu_success'));
	     redirect(base_url('setting/index'));
	} else {
	     $this->data['class_levels'] = $this->classlevel_m->get_classlevel();
	    $this->data["subview"] = "setting/academic_edit";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function signature() {
	$this->data["signature"] = $this->retrieve_signature();
	$this->data["subview"] = "setting/signature";
	$this->load->view('_layout_main', $this->data);
    }

    public function upload_signature() {
	$this->data["signature"] = $this->retrieve_signature();
	$this->data["subview"] = "setting/upload_signature";
	$this->load->view('_layout_main', $this->data);
    }

    public function save_signature() {


	$usertype = $this->session->userdata("usertype");
	$username = $this->session->userdata("username");
	$signature = str_replace(' ', '+', $_POST['signature']);
	$data_uri = $signature;
	$encoded_image = explode(",", $data_uri)[1];
	$decoded_image = base64_decode($encoded_image);
	$path = './uploads/signature/' . date("Y") . '/' . str_replace('.', '', set_schema_name());
	if (!is_dir($path)) {
	    if (mkdir($path, 0777, TRUE))
		;
	    else
		echo "ERROR";
	}
	$file_name = time() . ".png";
	$file_path = $path . "/" . $file_name;
	$old_name = NULL;
	if ($usertype == 'Admin' || $usertype == 'Accountant') {
	    $old_name = $this->db->query("SELECT signature_path FROM " . set_schema_name() . "user WHERE usertype='" . $usertype . "' AND username= '" . $username . "'")->row()->signature_path;
	} elseif ($usertype == 'Teacher') {
	    $old_name = $this->db->query("SELECT signature_path FROM " . set_schema_name() . "teacher WHERE usertype='" . $usertype . "' AND username= '" . $username . "'")->row()->signature_path;
	}

	if ($old_name != NULL) {
	    $old_file_path = $path . "/" . $old_name;
	    if (is_file($old_file_path)) {
		unlink($old_file_path);
	    }
	}

	file_put_contents($file_path, $decoded_image);
	if ($this->setting_m->save_signature($signature, $file_name)) {
	    echo "SUCCESS";
	} else
	    echo "ERROR";
    }

    public function retrieve_signature() {
	return $this->setting_m->get_signature();
    }

    public function unique_day() {
	$day = $this->input->post('automation');
	if ((int) $day) {
	    if ($day < 0 || $day > 28) {
		$this->form_validation->set_message("unique_day", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	} else {
	    $this->form_validation->set_message("unique_day", "%s already exists");
	    return FALSE;
	}
    }

    public function pcode_validation($pcode) {

	return TRUE;
    }

    public function version() {
	echo CI_VERSION;
    }

}

/* End of file setting.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/setting.php */