<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Book extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
	parent::__construct();
	$language = $this->session->userdata('lang');
	$this->lang->load('book', $language);
    }

    public function index() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Librarian" || $usertype == "Student" || $usertype == "Parent" || $usertype == "Teacher") {
	    $this->data['books'] = $this->book_m->get_order_by_book();
	    $this->data["subview"] = "book/index";
	    $this->load->view('_layout_main', $this->data);
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    protected function rules() {
	$rules = array(
	    array(
		'field' => 'book',
		'label' => $this->lang->line("book_name"),
		'rules' => 'trim|required|xss_clean|max_length[150]'
	    ),
	    array(
		'field' => 'author',
		'label' => $this->lang->line("book_author"),
		'rules' => 'trim|required|max_length[100]|xss_clean'
	    ),
	    array(
		'field' => 'subject',
		'label' => $this->lang->line("book_subject_code"),
		'rules' => 'trim|required|max_length[20]|xss_clean'
	    ),
	    array(
		'field' => 'quantity',
		'label' => $this->lang->line("book_quantity"),
		'rules' => 'trim|required|numeric|max_length[10]|xss_clean|callback_valid_number_for_quantity'
	    ),
	    array(
		'field' => 'rack',
		'label' => $this->lang->line("book_rack_no"),
		'rules' => 'trim|required|max_length[60]|xss_clean'
	    )
	);
	return $rules;
    }

    public function add() {
	$usertype = $this->session->userdata("usertype");
	$this->data['classes'] = $this->student_m->get_classes();
	$this->data['subjects'] = $this->db->query("SELECT distinct lower(subject) as subject FROM " . set_schema_name() . "subject GROUP BY subject")->result();
	if ($usertype == "Admin" || $usertype == "Librarian") {
	    if ($_POST) {
		$rules = $this->rules();
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE) {
		    $this->data["subview"] = "book/add";
		    $this->load->view('_layout_main', $this->data);
		} else {
		    $username = $this->session->userdata("username");
		    $user = $this->user_m->get_single_user(array('username' => $username));
		    $array = array(
			"book" => $this->input->post("book"),
			"author" => $this->input->post("author"),
			"subject" => $this->input->post("subject"),
			"quantity" => $this->input->post("quantity"),
			"edition" => $this->input->post("edition"),
			"book_for" => $this->input->post("book_for"),
			"due_quantity" => 0,
			"rack" => $this->input->post("rack"),
			"user_id" => $user->userID
		    );
		    $this->book_m->insert_book($array);
		    $book_id = $this->db->insert_id();
		    $classes = $this->input->get_post('classes');
		    foreach ($classes as $class_id) {
			$class_book_array = array(
			    "book_id" => $book_id,
			    "classes_id" => $class_id,
			);
			$this->db->insert('book_class', $class_book_array);
		    }
		    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
		    redirect(base_url("book/index"));
		}
	    } else {
		$this->data["subview"] = "book/add";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function edit() {
	$usertype = $this->session->userdata("usertype");

	if ($usertype == "Admin") {

	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->data['book'] = $this->book_m->get_book($id);
		$this->data['subjects'] = $this->db->query("SELECT distinct lower(subject) as subject FROM " . set_schema_name() . "subject GROUP BY subject")->result();
		$this->data['classes'] = $this->student_m->get_classes();
		$this->data['book_classes'] = $this->db->query('SELECT * FROM ' . set_schema_name() . 'book_class WHERE book_id=' . $id)->result();
		if ($this->data['book']) {
		    if ($_POST) {
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
			    $this->data['form_validation'] = validation_errors();
			    $this->data["subview"] = "book/add";
			    $this->load->view('_layout_main', $this->data);
			} else {
			    $array = array(
				"book" => $this->input->post("book"),
				"author" => $this->input->post("author"),
				"subject" => $this->input->post("subject"),
				"quantity" => $this->input->post("quantity"),
				"edition" => $this->input->post("edition"),
				"book_for" => $this->input->post("book_for"),
				"rack" => $this->input->post("rack")
			    );
			    $this->book_m->update_book($array, $id);

			    $classes = $this->input->get_post('classes');
			    $this->db->query('delete FROM ' . set_schema_name() . 'book_class WHERE book_id=' . $id);

			    foreach ($classes as $class_id) {
				$class_book_array = array(
				    "book_id" => $id,
				    "classes_id" => $class_id,
				);
				$this->db->insert('book_class', $class_book_array);
			    }
			    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
			    redirect(base_url("book/index"));
			}
		    } else {
			$this->data["subview"] = "book/edit";
			$this->load->view('_layout_main', $this->data);
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function delete() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->book_m->delete_book($id);
		$this->session->set_flashdata('success', $this->lang->line('menu_success'));
		redirect(base_url("book/index"));
	    } else {
		redirect(base_url("book/index"));
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function unique_book() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $student = $this->book_m->get_order_by_book(array("book" => $this->input->post("book"), "bookID !=" => $id, "author" => $this->input->post('author'), "subject_code" => $this->input->post('subject_code')));
	    if (count($student)) {
		$this->form_validation->set_message("unique_book", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	} else {
	    $student = $this->book_m->get_order_by_book(array("book" => $this->input->post("book"), "author" => $this->input->post('author'), "subject_code" => $this->input->post('subject_code')));

	    if (count($student)) {
		$this->form_validation->set_message("unique_book", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	}
    }

    function valid_number() {
	if ($this->input->post('price') && $this->input->post('price') < 0) {
	    $this->form_validation->set_message("valid_number", "%s is invalid number");
	    return FALSE;
	}
	return TRUE;
    }

    function valid_number_for_quantity() {
	if ($this->input->post('quantity') && $this->input->post('quantity') < 0) {
	    $this->form_validation->set_message("valid_number_for_quantity", "%s is invalid number");
	    return FALSE;
	}
	return TRUE;
    }

    private function checkKeysExists($value) {
	$required = array('name', 'author', 'edition', 'subject',
	    'quantity', 'book_for', 'rank', 'class');
	$data = array_shift($value);
	if (count(array_intersect_key(array_flip($required), $data)) === count($required)) {
	    //All required keys exist! 
	    $status = 1;
	} else {
	    $missing = array_intersect_key(array_flip($required), $data);
	    $data_miss = array_diff(array_flip($required), $missing);
	    $status = ' Column with title <b>' . implode(', ', array_keys($data_miss)) . '</b>  miss from Excel file. Please make sure file is in the same format as a sample file';
	}
	return $status;
    }

    public function uploadByFile() {
	$data = $this->uploadExcel();

	$status = $this->checkKeysExists($data);
	if ((int) $status == 1) {

	    $username = $this->session->userdata("username");
	    $user = $this->user_m->get_single_user(array('username' => $username));
	    $status = '';

	    foreach ($data as $value) {

		/*
		 * To avoid any duplicate, lets check first if this book name & author exists
		 */
		$book_name = preg_replace('/[)(]/', '-', $value['name']);
		//$book_name=preg_replace('/[^a-zA-Z0-9 ]/', '',$value['name']);
		$author = preg_replace('/[^a-zA-Z0-9 ]/', '', $value['author']);
		$edition = preg_replace('/[^a-zA-Z0-9 ]/', '', $value['edition']);
		$where = array("book" => $book_name,
		    "author" => $author,
		    "edition" => $edition);

		$book_exists = $this->book_m->get_order_by_book($where);
		if (empty($book_exists) && $book_name != '') {
		    $array = array(
			"book" => $book_name,
			"author" => $author,
			"subject" => $value['subject'],
			"quantity" => $value['quantity'],
			"edition" => $edition,
			"book_for" => $value['book_for'],
			"rack" => $value['rack'],
			"due_quantity" => 0,
			"user_id" => $user->userID
		    );
		    $this->book_m->insert_book($array);
		    $book_id = $this->db->insert_id();

		    $classes = explode(',', $value['class']);
		    foreach ($classes as $class_numeric_id) {
			$class_info = $this->classes_m->get_order_by_classes(array('classes_numeric' => $class_numeric_id));
			$class = array_shift($class_info);
			$class_book_array = array(
			    "book_id" => $book_id,
			    "classes_id" => $class->classesID,
			);
			$this->db->insert('book_class', $class_book_array);
		    }
		    $status .= '<p class="alert alert-success">Book "' . $book_name . '" uploaded successfully</p>';
		} else {
		    $status .= '<p class="alert alert-info">Book "' . $book_name . '" exist</p>';
		}
	    }
	}
	$this->session->set_flashdata('success', $this->lang->line('menu_success'));
	$this->data['status'] = $status;
	$this->data["subview"] = "mark/upload_status";
	$this->load->view('_layout_main', $this->data);
    }

}

/* End of file book.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/book.php */