<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends Admin_Controller {

    /**
     * -----------------------------------------
     *
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     *
     *
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     *
     *
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
	parent::__construct();
	$language = $this->session->userdata('lang');
	$this->lang->load('user', $language);
	$this->lang->load('email', $language);
    }

    public function index() {
	/**
	  there is no need to detect usertype, permission check will be controlled by
	  condition statement in the menu.
	 * 
	 */
	$usertype = $this->session->userdata("usertype");
	//$this->data['roles'] = $this->role->all();
	$id = htmlentities($this->uri->segment(3));
	if ((int) $id) {
	    $this->data['set'] = $id;
	    $this->data['users'] = $this->user_m->get_user($id);
	    $this->data["subview"] = "user/index";
	    $this->load->view('_layout_main', $this->data);
	} else {
	    $this->data['users'] = $this->user_m->get_user();
	    $this->data["subview"] = "user/index";
	    $this->load->view('_layout_main', $this->data);
	}
	if ($usertype == "Admin") {
	    /**
	      $this->data['users'] = $this->user_m->get_user();
	      $this->data["subview"] = "user/index";
	      $this->load->view('_layout_main', $this->data);
	     * 
	     */
	} else {
	    /**
	      $this->data["subview"] = "error";
	      $this->load->view('_layout_main', $this->data);
	     * 
	     */
	}
    }

    public function user_list() {
	$roleID = $this->input->post('id');
	if ((int) $roleID) {
	    $string = base_url("user/index/$roleID");
	    echo $string;
	} else {
	    redirect(base_url("student/index"));
	}
    }

    protected function rules() {
	$rules = array(
	    array(
		'field' => 'name',
		'label' => $this->lang->line("user_name"),
		'rules' => 'trim|required|xss_clean|max_length[60]'
	    ),
	    array(
		'field' => 'dob',
		'label' => $this->lang->line("user_dob"),
		'rules' => 'trim|required|max_length[20]xss_clean'
	    ),
	    array(
		'field' => 'sex',
		'label' => $this->lang->line("user_sex"),
		'rules' => 'trim|max_length[10]|xss_clean'
	    ),
	    array(
		'field' => 'religion',
		'label' => $this->lang->line("user_religion"),
		'rules' => 'trim|max_length[25]|xss_clean'
	    ),
	    array(
		'field' => 'email',
		'label' => $this->lang->line("user_email"),
		'rules' => 'trim|required|max_length[40]|valid_email|xss_clean|callback_unique_email'
	    ),
	    array(
		'field' => 'phone',
		'label' => $this->lang->line("user_phone"),
		'rules' => 'trim|min_length[5]|max_length[25]|xss_clean'
	    ),
	    array(
		'field' => 'address',
		'label' => $this->lang->line("user_address"),
		'rules' => 'trim|max_length[200]|xss_clean'
	    ),
	    array(
		'field' => 'jod',
		'label' => $this->lang->line("user_jod"),
		'rules' => 'trim|required|max_length[20]|xss_clean'
	    ),
	    array(
		'field' => 'usertype',
		'label' => $this->lang->line("user_type"),
		'rules' => 'trim|required|max_length[20]|xss_clean'
	    ),
	    array(
		'field' => 'photo',
		'label' => $this->lang->line("user_photo"),
		'rules' => 'trim|max_length[200]|xss_clean'
	    )
	);
	return $rules;
    }

    public function add() {
	$usertype = $this->session->userdata("usertype");
	$setting = $this->setting_m->get_setting(1);
	if ($usertype == "Admin") {
	    if ($_POST) {
		$rules = $this->rules();
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE) {
		    $this->data["subview"] = "user/add";
		    $this->load->view('_layout_main', $this->data);
		} else {
		    $array = array();
		    $array["name"] = $this->input->post("name");
		    $array["dob"] = date("Y-m-d", strtotime($this->input->post("dob")));
		    $array["sex"] = $this->input->post("sex");
		    $array["religion"] = $this->input->post("religion");
		    $array["email"] = $this->input->post("email");
		    $array["phone"] = $this->input->post("phone");
		    $array["address"] = $this->input->post("address");
		    $array["jod"] = date("Y-m-d", strtotime($this->input->post("jod")));
		    $array["username"] = $this->input->post("phone");
		    $array['password'] = $this->user_m->hash('user123');
		    $array["usertype"] = $this->input->post("usertype");

		    $new_file = "defualt.png";
		    if ($_FILES["image"]['name'] != "") {
			$file_name = $_FILES["image"]['name'];
			$file_name_rename = rand(1, 100000000000);
			$explode = explode('.', $file_name);
			if (count($explode) >= 2) {
			    $upload = $this->uploadImage();
			    $array['photo'] = $upload;
			    if ($upload == FALSE) {
				$this->data["image"] = $this->upload->display_errors();
				$this->data["subview"] = "user/add";
				$this->load->view('_layout_main', $this->data);
			    } else {
				$data = array("upload_data" => $this->upload->data());

				$this->db->insert('user', $array);
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				$message = sprintf($this->lang->line('add_new_user'), $this->input->post("name"), $this->input->post("usertype"), $setting->sname, $this->input->post("phone"), $this->user_m->hash('user123')
				);

				$this->send_email($this->input->post('email'), $setting->sname . ' Online Login Information', $message);
				$this->send_sms($this->input->post("phone"), $message);


				redirect(base_url("user/index"));
			    }
			} else {
			    $this->data["image"] = "Invalid file";
			    $this->data["subview"] = "user/add";
			    $this->load->view('_layout_main', $this->data);
			}
		    } else {
			$array["photo"] = $new_file;
			$this->db->insert('user', $array);
			$this->session->set_flashdata('success', $this->lang->line('menu_success'));

			$message = sprintf($this->lang->line('add_new_user'), $this->input->post("name"), $this->input->post("usertype"), $setting->sname, $this->input->post("phone"), $this->user_m->hash('user123')
			);
			$this->send_email($this->input->post('email'), $setting->sname . ' Online Login Information', $message);
			$this->send_sms($this->input->post("phone"), $message);
			redirect(base_url("user/index"));
		    }
		}
	    } else {
		$this->data["subview"] = "user/add";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function edit() {
	$usertype = $this->session->userdata("usertype");
	$setting = $this->setting_m->get_setting(1);
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {

		$this->data['user'] = $this->user_m->get_user($id);
		if ($this->data['user']) {
		    $rules = $this->rules();
		    unset($rules[9], $rules[10], $rules[11]);
		    $this->form_validation->set_rules($rules);
		    if ($this->form_validation->run() == FALSE) {
			$this->data["subview"] = "user/edit";
			$this->load->view('_layout_main', $this->data);
		    } else {
			$array = array();
			$array["name"] = $this->input->post("name");
			$array["dob"] = date("Y-m-d", strtotime($this->input->post("dob")));
			$array["sex"] = $this->input->post("sex");
			$array["religion"] = $this->input->post("religion");
			$array["email"] = $this->input->post("email");
			$array["phone"] = $this->input->post("phone");
			$array["address"] = $this->input->post("address");
			$array["jod"] = date("Y-m-d", strtotime($this->input->post("jod")));
			$array["usertype"] = $this->input->post("usertype");

			if ($_FILES["image"]['name'] != "") {
			    $file_name = $_FILES["image"]['name'];
			    $file_name_rename = rand(1, 100000000000);
			    $explode = explode('.', $file_name);
			    if (count($explode) >= 2) {
				$upload = $this->uploadImage();
				$array['photo'] = $upload;
				if ($upload == FALSE) {
				    $this->data["image"] = $this->upload->display_errors();
				    $this->data["subview"] = "user/edit";
				    $this->load->view('_layout_main', $this->data);
				} else {

				    $data = array("upload_data" => $this->upload->data());
				    $this->user_m->update_user($array, $id);
				    $this->session->set_flashdata('success', $this->lang->line('menu_success'));

				    $message = sprintf($this->lang->line('edit_user'), $this->input->post("name"), $this->input->post("phone"), $this->input->post('1234567'));
				    $this->send_sms($this->input->post("phone"), $message);
				    $this->send_email($this->input->post('email'), $setting->sname . ' Online Login Information', $message);
				    redirect(base_url("user/index"));
				}
			    } else {
				$this->data["image"] = "Invalid file";
				$this->data["subview"] = "user/edit";
				$this->load->view('_layout_main', $this->data);
			    }
			} else {
			    //echo date("Y-m-d", strtotime($this->input->post("dob")));exit;
			    $this->user_m->update_user($array, $id);
			    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
			    $message = sprintf($this->lang->line('edit_user'), $this->input->post("name"), $this->input->post("phone"), $this->input->post('teacher123'));
			    //$this->send_sms($this->input->post("phone"), $message);
			    $this->send_email($this->input->post('email'), $setting->sname . ' Online Login Information', $message);
			    redirect(base_url("user/index"));
			}
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function delete() {
	$usertype = $this->session->userdata("usertype");
	$setting = $this->setting_m->get_setting(1);
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$user = $this->user_m->get_single_user(array("userID" => $id));
		$this->user_m->delete_user($id);
		$this->session->set_flashdata('success', $this->lang->line('menu_success'));
		//$message='Hello'.$user->name.'You have been deleted in shulesoft';
		$message = sprintf($this->lang->line('delete_user'), $user->name, $setting->sname, $setting->sname);
		$this->send_sms($user->phone, $message);
		$this->send_email($user->email, $setting->sname, $message);

		$this->session->set_flashdata('success', $this->lang->line('menu_success'));
		redirect(base_url("user/index"));
	    } else {
		redirect(base_url("user/index"));
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function view() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));

	    if ((int) $id) {
		$this->data["user"] = $this->user_m->get_user($id);
		if ($this->data["user"]) {
		    $this->data["subview"] = "user/view";
		    $this->load->view('_layout_main', $this->data);
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function print_preview() {
	$id = htmlentities(($this->uri->segment(3)));

	if ((int) $id) {
	    $usertype = $this->session->userdata('usertype');
	    if ($usertype == "Admin") {
		$this->load->library('html2pdf');
		$this->html2pdf->folder('./assets/pdfs/');
		$this->html2pdf->filename('Report.pdf');
		$this->html2pdf->paper('a4', 'portrait');

		$this->data['user'] = $this->user_m->get_user($id);
		if ($this->data['user']) {
		    $this->data['panel_title'] = $this->lang->line('panel_title');
		    $html = $this->load->view('user/print_preview', $this->data, true);
		    $this->html2pdf->html($html);
		    $this->html2pdf->create();
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function send_mail() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = $this->input->post('id');
	    if ((int) $id) {
		$this->load->library('html2pdf');
		$this->html2pdf->folder('uploads/report');
		$this->html2pdf->filename('Report.pdf');
		$this->html2pdf->paper('a4', 'portrait');

		$this->data['user'] = $this->user_m->get_user($id);
		if ($this->data["user"]) {
		    $this->data['panel_title'] = $this->lang->line('panel_title');
		    $html = $this->load->view('user/print_preview', $this->data, true);
		    $this->html2pdf->html($html);
		    $this->html2pdf->create('save');

		    if ($path = $this->html2pdf->create('save')) {
			$this->load->library('email');
			$this->email->set_mailtype("html");
			$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);
			$this->email->to($this->input->post('to'));
			$this->email->subject($this->input->post('subject'));
			$this->email->message($this->input->post('message'));
			$this->email->attach($path);
			if ($this->email->send()) {
			    $this->session->set_flashdata('success', $this->lang->line('mail_success'));
			} else {
			    $this->session->set_flashdata('error', $this->lang->line('mail_error'));
			}
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function lol_username() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
	    $array = array();
	    $i = 0;
	    foreach ($tables as $table) {
		$user = $this->user_m->get_username($table, array("username" => $this->input->post('username'), $table . "ID !=" => $id));
		if (count($user)) {
		    $this->form_validation->set_message("lol_username", "%s already exists");
		    $array['permition'][$i] = 'no';
		} else {
		    $array['permition'][$i] = 'yes';
		}
		$i++;
	    }
	    if (in_array('no', $array['permition'])) {
		return FALSE;
	    } else {
		return TRUE;
	    }
	} else {
	    $tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
	    $array = array();
	    $i = 0;
	    foreach ($tables as $table) {
		$user = $this->user_m->get_username($table, array("username" => $this->input->post('username')));
		if (count($user)) {
		    $this->form_validation->set_message("lol_username", "%s already exists");
		    $array['permition'][$i] = 'no';
		} else {
		    $array['permition'][$i] = 'yes';
		}
		$i++;
	    }

	    if (in_array('no', $array['permition'])) {
		return FALSE;
	    } else {
		return TRUE;
	    }
	}
    }

    public function date_valid($date) {
	if (strlen($date) < 10) {
	    $this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	    return FALSE;
	} else {
	    $arr = explode("-", $date);
	    $dd = $arr[0];
	    $mm = $arr[1];
	    $yyyy = $arr[2];
	    if (checkdate($mm, $dd, $yyyy)) {
		return TRUE;
	    } else {
		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
		return FALSE;
	    }
	}
    }

    public function unique_email() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $user_info = $this->user_m->get_single_user(array('userID' => $id));
	    $tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
	    $array = array();
	    $i = 0;
	    foreach ($tables as $table) {
		$user = $this->user_m->get_username($table, array("email" => $this->input->post('email'), 'username !=' => $user_info->username));
		if (count($user)) {
		    $this->form_validation->set_message("unique_email", "%s already exists");
		    $array['permition'][$i] = 'no';
		} else {
		    $array['permition'][$i] = 'yes';
		}
		$i++;
	    }
	    if (in_array('no', $array['permition'])) {
		return FALSE;
	    } else {
		return TRUE;
	    }
	} else {
	    $tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
	    $array = array();
	    $i = 0;
	    foreach ($tables as $table) {
		$user = $this->user_m->get_username($table, array("email" => $this->input->post('email')));
		if (count($user)) {
		    $this->form_validation->set_message("unique_email", "%s already exists");
		    $array['permition'][$i] = 'no';
		} else {
		    $array['permition'][$i] = 'yes';
		}
		$i++;
	    }

	    if (in_array('no', $array['permition'])) {
		return FALSE;
	    } else {
		return TRUE;
	    }
	}
    }

    public function export() {

	// get all users in array format
	$users = $this->db->query('SELECT name,usertype,dob,sex,religion,email,phone,address FROM ' . set_schema_name() . 'user')->result_array();
	$title = array(' Name', 'usertype', 'Date of birth', 'Gender', 'Religion', 'email', 'phone,address');
	$this->exportExcel($users, $title, 'users');
    }

    private function checkKeysExists($value) {
	$required = array('phone', 'name', 'usertype', 'dob', 'sex',
	    'religion', 'joining_date', 'email', 'address');
	$data = array_shift($value);
	if (count(array_intersect_key(array_flip($required), $data)) === count($required)) {
	    //All required keys exist! 
	    $status = 1;
	} else {
	    $missing = array_intersect_key(array_flip($required), $data);
	    $data_miss = array_diff(array_flip($required), $missing);
	    $status = ' Column with title <b>' . implode(', ', array_keys($data_miss)) . '</b>  miss from Excel file. Please make sure file is in the same format as a sample file';
	}
	return $status;
    }

    public function uploadByFile() {
	$data = $this->uploadExcel();

	$status = $this->checkKeysExists($data);
	if ((int) $status == 1) {
	    $status = '';
	    $password = $this->user_m->hash('user123');
	    foreach ($data as $value) {

		/*
		 * To avoid any duplicate, lets check first if this  name & phone
		 */
		$name = preg_replace('/[^a-zA-Z0-9 ]/', '', $value['name']);
		$phone = preg_replace('/[^a-zA-Z0-9 ]/', '', $value['phone']);

		$teacher_info = $this->user_m->get_single_user(array('name' => $name, 'phone' => $phone));
		if ($name == '' || $phone == '') {
		    $status .= '<p class="alert alert-danger">User name and phone cannot be empty. Record failed to be uploaded</p>';
		} else if (empty($teacher_info)) {

		    $array = array(
			"name" => $name,
			"dob" => date("Y-m-d", strtotime($value['dob'])),
			"sex" => $value['sex'],
			"religion" => $value['religion'],
			"email" => $value['email'],
			"phone" => $value['phone'],
			"jod" => date("Y-m-d", strtotime($value['joining_date'])),
			"address" => $value['address'],
			"password" => $password,
			"username" => $phone,
			"usertype" => ucfirst(strtolower($value['usertype'])),
			"photo" => "defualt.png"
		    );
		    $this->user_m->insert_user($array);

		    $status .= '<p class="alert alert-success">User "' . $name . '" uploaded successfully</p>';
		} else {
		    $status .= '<p class="alert alert-info">User "' . $name . '" exist</p>';
		}
	    }
	}
	$this->session->set_flashdata('success', $this->lang->line('menu_success'));
	$this->data['status'] = $status;
	$this->data["subview"] = "mark/upload_status";
	$this->load->view('_layout_main', $this->data);
    }

}

/* End of file user.php */
/* Location: .//D/xampp/htdocs/school/controllers/user.php */