<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Student extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
	parent::__construct();
	$language = $this->session->userdata('lang');
	$this->lang->load('student', $language);
	$this->lang->load('email', $language);
    }

    public function index() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Teacher" || $usertype == 'Secretary') {
	    $id = htmlentities($this->uri->segment(3));
	    if ((int) $id) {
		$this->data['set'] = $id;
		$this->data['classes'] = $this->student_m->get_classes();
		$this->data['students'] = $this->student_m->get_order_by_student(array('classesID' => $id));
		$class = $this->classes_m->get_classes($id);
		$academic = $this->academic_year_m->get_current_year($class->classlevel_id);
		$this->data['current_academic_year_id'] = $academic->id;
		if ($this->data['students']) {
		    $sections = $this->section_m->get_order_by_section(array("classesID" => $id));
		    $this->data['sections'] = $sections;
		    foreach ($sections as $key => $section) {
			$this->data['allsection'][$section->section] = $this->student_m->get_order_by_student(array('classesID' => $id, "sectionID" => $section->sectionID));
		    }
		} else {
		    $this->data['students'] = NULL;
		}

		$this->data["subview"] = "student/index";
		$this->load->view('_layout_main', $this->data);
	    } else {
		$this->data['classes'] = $this->student_m->get_classes();
		$this->data["subview"] = "student/search";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    protected function rules() {
	$rules = array(
	    array(
		'field' => 'name',
		'label' => $this->lang->line("student_name"),
		'rules' => 'trim|required|xss_clean|max_length[60]'
	    ),
	    array(
		'field' => 'dob',
		'label' => $this->lang->line("student_dob"),
		'rules' => 'trim|required|max_length[20]|xss_clean'
	    ),
	    array(
		'field' => 'sex',
		'label' => $this->lang->line("student_sex"),
		'rules' => 'trim|required|max_length[10]|xss_clean'
	    ),
	    array(
		'field' => 'religion',
		'label' => $this->lang->line("student_religion"),
		'rules' => 'trim|max_length[25]|xss_clean'
	    ),
//			array(
//				'field' => 'email', 
//				'label' => $this->lang->line("student_email"), 
//				'rules' => 'trim|required|max_length[40]|valid_email|xss_clean|callback_unique_email'
//			),
//	    array(
//		'field' => 'phone',
//		'label' => $this->lang->line("student_phone"),
//		'rules' => 'trim|max_length[25]|min_length[5]|xss_clean'
//	    ),
	    array(
		'field' => 'address',
		'label' => $this->lang->line("student_address"),
		'rules' => 'trim|max_length[200]|xss_clean'
	    ),
	    array(
		'field' => 'classesID',
		'label' => $this->lang->line("student_classes"),
		'rules' => 'trim|required|numeric|max_length[11]|xss_clean|callback_unique_classesID'
	    ),
	    array(
		'field' => 'sectionID',
		'label' => $this->lang->line("student_section"),
		'rules' => 'trim|required|numeric|max_length[11]|xss_clean|callback_unique_sectionID'
	    ),
	    array(
		'field' => 'roll',
		'label' => $this->lang->line("student_roll"),
		'rules' => 'trim|required|max_length[40]|callback_unique_roll|xss_clean'
	    ),
	    array(
		'field' => 'guargianID',
		'label' => $this->lang->line("student_guargian"),
		'rules' => 'trim|required|max_length[11]|xss_clean|numeric'
	    ),
	    array(
		'field' => 'photo',
		'label' => $this->lang->line("student_photo"),
		'rules' => 'trim|max_length[200]|xss_clean'
	    ),
//			array(
//			'field' => 'username', 
//			'label' => $this->lang->line("student_username"), 
//			'rules' => 'trim|required|min_length[4]|max_length[40]|xss_clean|callback_lol_username'
//			),
//			array(
//				'field' => 'password',
//				'label' => $this->lang->line("student_password"), 
//				'rules' => 'trim|required|min_length[4]|max_length[40]|xss_clean'
//			)
	);
	return $rules;
    }
    public function add() {

	$usertype = $this->session->userdata("usertype");
	$setting = $this->setting_m->get_setting(1);


	if ($usertype == "Admin" || $usertype == 'Secretary') {
	    $this->data['classes'] = $this->student_m->get_classes();
	    $this->data['sections'] = $this->section_m->get_section();
	    $this->data['parents'] = $this->parentes_m->get_parentes();

	    $classesID = $this->input->post("classesID");

	    if ($classesID != 0) {
		$this->data['sections'] = $this->section_m->get_order_by_section(array("classesID" => $classesID));
	    } else {
		$this->data['sections'] = "empty";
	    }
	    $this->data['sectionID'] = $this->input->post("sectionID");
	    if (!empty($_POST)) {

		$rules = $this->rules();
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE) {
		    $this->data["subview"] = "student/add";
		    $this->load->view('_layout_main', $this->data);
		} else {
		    $parent_id = array("parentID" => $this->input->post('guargianID'));
		    $single_parent = $this->parentes_m->get_single_parentes($parent_id);

		    $sectionID = $this->input->post("sectionID");
		    if ($sectionID == 0) {
			$this->data['sectionID'] = 0;
		    } else {
			$this->data['sections'] = $this->section_m->get_allsection($classesID);
			$this->data['sectionID'] = $this->input->post("sectionID");
		    }

		    $dbmaxyear = $this->student_m->get_order_by_student_single_max_year($classesID);
		    $maxyear = "";
		    if (count($dbmaxyear)) {
			$maxyear = $dbmaxyear->year;
		    } else {
			$maxyear = date("Y");
		    }


		    $section = $this->section_m->get_section($sectionID);
		    $array = array();
		    $array["name"] = $this->input->post("name");
		    $array["dob"] = date("Y-m-d", strtotime($this->input->post("dob")));
		    $array["sex"] = $this->input->post("sex");
		    $array["religion"] = $this->input->post("religion");

		    $array["phone"] = $this->input->post("phone");
		    $array["address"] = $this->input->post("address");
		    $array["classesID"] = $this->input->post("classesID");
		    $array["sectionID"] = $this->input->post("sectionID");
		    $array["section"] = empty($section) ? 'Null' : $section->section;
		    $array["roll"] = $this->input->post("roll");
		    $array["username"] = str_replace(' ', '.', strtolower($this->input->post("name")));
		    $array['password'] = $this->student_m->hash("abc123456");
		    $array['usertype'] = "Student";
		    $array['parentID'] = $this->input->post('guargianID');
		    $array['parent_type'] = $this->input->post('parent_type');
		    $array['nationality'] = $this->input->post('nationality');
		    $array['health'] = $this->input->post('health');
		    $array['health_other'] = $this->input->post('health_other');
		    $class = $this->classes_m->get_classes($array["classesID"]);
		    $academic = $this->academic_year_m->get_current_year($class->classlevel_id);
		    $array['academic_year_id'] = $academic->academic_year_id;
		    $array['library'] = 0;
		    $array['hostel'] = 0;
		    $array['transport'] = 0;
		    $array['create_date'] = date("Y-m-d");
		    $array['year'] = $maxyear;
		    $array['totalamount'] = 0;
		    $array['paidamount'] = 0;
                    $other_info=array();
                    $other_info['admitted_from'] = $this->input->post('admitted_from');
                    $other_info['reg_number'] = $this->input->post('reg_number');
                    $other_info['year_finished'] = $this->input->post('year_finished');
                    $other_info['region'] = $this->input->post('region');
                    $other_info['math'] = $this->input->post('math');
		    $other_info['physics'] = $this->input->post('physics');
                    $other_info['chemistry'] = $this->input->post('chemistry');
                    $other_info['geography'] = $this->input->post('geograph');
                    $other_info['kiswahili'] = $this->input->post('kiswahili');
		    $other_info['civics'] = $this->input->post('civics');
                    $other_info['history'] = $this->input->post('history');
                    $other_info['biology'] = $this->input->post('biology');
                    $other_info['english'] = $this->input->post('english');
		    $other_info['commerce'] = $this->input->post('commerce');
                    $other_info['book_keeping'] = $this->input->post('bk');
                    $other_info['division'] = $this->input->post('division');
                    $other_info['point'] = $this->input->post('point');
                    $other_info['other_subject'] = $this->input->post('other_subject');

		    $array["email"] = $this->input->post("email") == '' ?
			    $array['username'] . '@shulesoft.com' :
			    $this->input->post("email");
		    $new_file = "defualt.png";
		    if ($_FILES["image"]['name'] != "") {
			$file_name = $_FILES["image"]['name'];
			$file_name_rename = rand(1, 100000000000);
			$explode = explode('.', $file_name);
			if (count($explode) >= 2) {
			    $upload = $this->uploadImage();
			    $array['photo'] = $upload;

			    if ($upload == FALSE) {
				$this->data["image"] = $this->upload->display_errors();
				$this->data["subview"] = "student/add";
				$this->load->view('_layout_main', $this->data);
			    } else {
				$data = array("upload_data" => $this->upload->data());
				$this->student_m->insert_student($array);
                                $studentID= $this->db->insert_id();
                                 $other_info['studentID'] = $studentID;
                                $this->db->insert('student_other', $other_info);  
                          
                     

				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				$message = sprintf($this->lang->line('parent_registered_student'), $single_parent->name, $this->input->post('name'), $setting->sname
				);

				//$this->send_email($single_parent->email, $setting->sname, $message);
				//$this->send_sms($single_parent->phone, $message);

				//redirect(base_url("student/index"));
			    }
			} else {
			    $this->data["image"] = "Invalid file";
			    $this->data["subview"] = "student/add";
			    $this->load->view('_layout_main', $this->data);
			}
		    } else {
			$array["photo"] = $new_file;
			$this->student_m->insert_student($array);
                            $studentID= $this->db->insert_id();                  
                               $other_info['studentID'] = $studentID;
                               $this->db->insert('student_other', $other_info);         
                        
			$this->session->set_flashdata('success', $this->lang->line('menu_success'));
			$message = array("message" => sprintf($this->lang->line('parent_registered_student'), $single_parent->name, $this->input->post('name'), $setting->sname
			));

			//$this->send_email($single_parent->email, $setting->sname, $message);
			//$this->send_sms($this->input->post("phone"), 'Hello ' . $this->input->post("name") . ', you have been added as a Student at ' . $setting->sname);

			redirect(base_url("student/index"));
		    }
		}
	    } else {
		$this->data["subview"] = "student/add";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }
    public function subjectform(){
        $this->data["view"] = "student/subjects";
		$this->load->view('student/subjects');
    }

    public function edit() {
	$usertype = $this->session->userdata("usertype");
	$setting = $this->setting_m->get_setting(1);
	if ($usertype == "Admin" || $usertype == 'Secretary') {
	    $id = htmlentities(($this->uri->segment(3)));
	    $url = htmlentities(($this->uri->segment(4)));

	    if ((int) $id && (int) $url) {
		$this->data['classes'] = $this->student_m->get_classes();
		$this->data['student'] = $this->student_m->get_student_otherinfo($id);
		$this->data['parents'] = $this->parentes_m->get_parentes();
		$classesID = $this->data['student']->classesID;

		$this->data['sections'] = $this->section_m->get_order_by_section(array('classesID' => $classesID));
		$this->data['set'] = $url;
		if ($this->data['student']) {
		    if ($_POST) {
			$parent_id = array("parentID" => $this->input->post('guargianID'));
			$single_parent = $this->parentes_m->get_single_parentes($parent_id);

			$rules = $this->rules();
			unset($rules[11], $rules[12], $rules[13]);
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
			    $this->data["subview"] = "student/edit";
			    $this->load->view('_layout_main', $this->data);
			} else {

			    $array = array();
			    $array["name"] = $this->input->post("name");
			    $array["dob"] = date("Y-m-d", strtotime($this->input->post("dob")));
			    $array["sex"] = $this->input->post("sex");
			    $array["religion"] = $this->input->post("religion");
			    $array["email"] = $this->input->post("email");
			    $array["phone"] = $this->input->post("phone");
			    $array["address"] = $this->input->post("address");
			    $array["classesID"] = $this->input->post("classesID");
			    $array["sectionID"] = $this->input->post("sectionID");
			    $section = $this->section_m->get_section($this->input->post("sectionID"));
			    $array["section"] = $section->section;
			    $array["roll"] = $this->input->post("roll");
                            $array['nationality'] = $this->input->post('nationality');
                            $array['health'] = $this->input->post('health');
                            $array['health_other'] = $this->input->post('health_other'); 
			    $array["parentID"] = $this->input->post("guargianID");
                            
                             $other_info=array();
                    $other_info['admitted_from'] = $this->input->post('admitted_from');
                    $other_info['reg_number'] = $this->input->post('reg_number');
                    $other_info['year_finished'] = $this->input->post('year_finished');
                    $other_info['region'] = $this->input->post('region');
                    $other_info['math'] = $this->input->post('math');
		    $other_info['physics'] = $this->input->post('physics');
                    $other_info['chemistry'] = $this->input->post('chemistry');
                    $other_info['geography'] = $this->input->post('geograph');
                    $other_info['kiswahili'] = $this->input->post('kiswahili');
		    $other_info['civics'] = $this->input->post('civics');
                    $other_info['history'] = $this->input->post('history');
                    $other_info['biology'] = $this->input->post('biology');
                    $other_info['english'] = $this->input->post('english');
		    $other_info['commerce'] = $this->input->post('commerce');
                    $other_info['book_keeping'] = $this->input->post('bk');
                    $other_info['division'] = $this->input->post('division');
                    $other_info['point'] = $this->input->post('point');
                    $other_info['other_subject'] = $this->input->post('other_subject');  
                            
                            

			    if ($_FILES["image"]['name'] != "") {
				//upload image

				$file_name = $_FILES["image"]['name'];
				$file_name_rename = rand(1, 100000000000);
				$explode = explode('.', $file_name);
				if (count($explode) >= 2) {
				    $upload = $this->uploadImage();
				    $array['photo'] = $upload;
				    if ($upload == FALSE) {
					$this->session->set_flashdata('success', "Error occurs, try later");
					$this->data["subview"] = "student/edit";
					$this->load->view('_layout_main', $this->data);
				    } else {
					$data = array("upload_data" => $this->upload->data());
					$this->student_m->update_student($array, $id);
                                         $this->db->update('student_other', $other_info,"studentID=$id");  

					$message = sprintf($this->lang->line('parent_edit_student'), $single_parent->name, $this->input->post('name'));
					//$this->send_email($single_parent->email, $setting->sname, $message);
					$this->session->set_flashdata('success', $this->lang->line('menu_success'));
					redirect(base_url("student/index/$url"));
				    }
				} else {
				    $this->data["image"] = "Invalid file";
				    $this->data["subview"] = "student/edit";
				    $this->load->view('_layout_main', $this->data);
				}
			    } else {
				        $this->student_m->update_student($array, $id);
                                      $this->db->where('studentID', $id);
                                      $this->db->update('student_other', $other_info); 
				$message = sprintf($this->lang->line('parent_edit_student'), $single_parent->name, $this->input->post('name'));
				//$this->send_email($single_parent->email, $setting->sname, $message);
				$this->session->set_flashdata('success', $this->lang->line('menu_success'));
				redirect(base_url("student/index/$url"));
			    }
			}
		    } else {
			$this->data["subview"] = "student/edit";
			$this->load->view('_layout_main', $this->data);
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function view() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin" || $usertype == "Teacher" || $usertype == 'Secretary') {
	    $id = htmlentities(($this->uri->segment(3)));
	    $url = htmlentities(($this->uri->segment(4)));
	    if ((int) $id && (int) $url) {
		$this->data["student"] = $this->student_m->get_student($id);
		$this->data["class"] = $this->student_m->get_class($url);
		if ($this->data["student"] && $this->data["class"]) {
		    $this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);
		    $this->data['set'] = $url;
		    $this->data["parent"] = $this->parentes_m->get_parentes($this->data["student"]->parentID);

		    $this->data["subview"] = "student/view";
		    $this->load->view('_layout_main', $this->data);
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function print_preview() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    $url = htmlentities(($this->uri->segment(4)));

	    if ((int) $id && (int) $url) {
		$this->data["student"] = $this->student_m->get_student($id);
		$this->data["class"] = $this->student_m->get_class($url);
		if ($this->data["student"] && $this->data["class"]) {
		    $this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);
		    $this->load->library('html2pdf');
		    $this->html2pdf->folder('./assets/pdfs/');
		    $this->html2pdf->filename('Report.pdf');
		    $this->html2pdf->paper('a4', 'portrait');
		    $this->data['panel_title'] = $this->lang->line('panel_title');
		    $this->data["parent"] = $this->parentes_m->get_parentes($this->data["student"]->parentID);
		    $html = $this->load->view('student/print_preview', $this->data, true);
		    $this->html2pdf->html($html);
		    $this->html2pdf->create();
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function send_mail() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = $this->input->post('id');
	    $url = $this->input->post('set');
	    if ((int) $id && (int) $url) {
		$this->data["student"] = $this->student_m->get_student($id);
		$this->data["class"] = $this->student_m->get_class($url);
		if ($this->data["student"] && $this->data["class"]) {
		    $this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);
		    $this->load->library('html2pdf');
		    $this->html2pdf->folder('uploads/report');
		    $this->html2pdf->filename('Report.pdf');
		    $this->html2pdf->paper('a4', 'portrait');
		    $this->data['panel_title'] = $this->lang->line('panel_title');
		    $this->data["parent"] = $this->parentes_m->get_parentes($this->data["student"]->parentID);
		    $html = $this->load->view('student/print_preview', $this->data, true);
		    $this->html2pdf->html($html);
		    $this->html2pdf->create('save');

		    if ($path = $this->html2pdf->create('save')) {
			$this->load->library('email');
			$this->email->set_mailtype("html");
			$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);
			$this->email->to($this->input->post('to'));
			$this->email->subject($this->input->post('subject'));
			$this->email->message($this->input->post('message'));
			$this->email->attach($path);
			if ($this->email->send()) {
			    $this->session->set_flashdata('success', $this->lang->line('mail_success'));
			} else {
			    $this->session->set_flashdata('error', $this->lang->line('mail_error'));
			}
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function delete() {
	$usertype = $this->session->userdata("usertype");
	$setting = $this->setting_m->get_setting(1);
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    $url = htmlentities(($this->uri->segment(4)));
	    if ((int) $id && (int) $url) {
		$student = $this->student_m->get_student($id);

		$getParent = $this->parentes_m->get_single_parentes(array("parentID" => $student->parentID));

		$this->student_m->delete_student($id);
		$message = sprintf($this->lang->line('parent_deleted_student'), $getParent->name, $setting->sname);
		$this->send_email($getParent->email, $setting->sname, $message);

		$this->session->set_flashdata('success', $this->lang->line('menu_success'));
		redirect(base_url("student/index/$url"));
	    } else {
		redirect(base_url("student/index/$url"));
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function deleteView($id) {
	$this->data["student"] = $this->student_m->get_student($id);
	if (!empty($_POST)) {
	    $dlt = $this->input->post('dlt');
	    if ($dlt == 3) {
		$this->delete();
	    } else {
		$url = htmlentities(($this->uri->segment(4)));
		$this->student_m->update_student(array('status' => $dlt), $id);
		redirect(base_url("student/index/$url"));
	    }
	} else {
	    $this->data["subview"] = "student/delete_view";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function unique_roll() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $student = $this->student_m->get_order_by_roll(array("roll" => $this->input->post("roll"), "studentID !=" => $id, "classesID" => $this->input->post('classesID')));
	    if (count($student)) {
		$this->form_validation->set_message("unique_roll", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	} else {
	    $student = $this->student_m->get_order_by_roll(array("roll" => $this->input->post("roll"), "classesID" => $this->input->post('classesID')));

	    if (count($student)) {
		$this->form_validation->set_message("unique_roll", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	}
    }

    public function lol_username() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
	    $array = array();
	    $i = 0;
	    foreach ($tables as $table) {
		$user = $this->student_m->get_username($table, array("username" => $this->input->post('username'), $table . "ID !=" => $id));
		if (count($user)) {
		    $this->form_validation->set_message("lol_username", "%s already exists");
		    $array['permition'][$i] = 'no';
		} else {
		    $array['permition'][$i] = 'yes';
		}
		$i++;
	    }
	    if (in_array('no', $array['permition'])) {
		return FALSE;
	    } else {
		return TRUE;
	    }
	} else {
	    $tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
	    $array = array();
	    $i = 0;
	    foreach ($tables as $table) {
		$user = $this->student_m->get_username($table, array("username" => $this->input->post('username')));
		if (count($user)) {
		    $this->form_validation->set_message("lol_username", "%s already exists");
		    $array['permition'][$i] = 'no';
		} else {
		    $array['permition'][$i] = 'yes';
		}
		$i++;
	    }

	    if (in_array('no', $array['permition'])) {
		return FALSE;
	    } else {
		return TRUE;
	    }
	}
    }

    public function unique_classesID() {
	if ($this->input->post('classesID') == 0) {
	    $this->form_validation->set_message("unique_classesID", "The %s field is required");
	    return FALSE;
	}
	return TRUE;
    }

    public function unique_sectionID() {
	if ($this->input->post('sectionID') == 0) {
	    $this->form_validation->set_message("unique_sectionID", "The %s field is required");
	    return FALSE;
	}
	return TRUE;
    }

    public function student_list() {
	$classID = $this->input->post('id');
	if ((int) $classID) {
	    $string = base_url("student/index/$classID");
	    echo $string;
	} else {
	    redirect(base_url("student/index"));
	}
    }

    public function date_valid($date) {
	if (strlen($date) < 10) {
	    $this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	    return FALSE;
	} else {
	    $arr = explode("-", $date);
	    $dd = $arr[0];
	    $mm = $arr[1];
	    $yyyy = $arr[2];
	    if (checkdate($mm, $dd, $yyyy)) {
		return TRUE;
	    } else {
		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
		return FALSE;
	    }
	}
    }

    public function unique_email() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $user_info = $this->student_m->get_single_student(array('studentID' => $id));
	    $tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
	    $array = array();
	    $i = 0;
	    foreach ($tables as $table) {
		$user = $this->student_m->get_username($table, array("email" => $this->input->post('email'), 'username !=' => $user_info->username));
		if (count($user)) {
		    $this->form_validation->set_message("unique_email", "%s already exists");
		    $array['permition'][$i] = 'no';
		} else {
		    $array['permition'][$i] = 'yes';
		}
		$i++;
	    }
	    if (in_array('no', $array['permition'])) {
		return FALSE;
	    } else {
		return TRUE;
	    }
	} else {
	    $tables = array('student' => 'student', 'parent' => 'parent', 'teacher' => 'teacher', 'user' => 'user', 'setting' => 'setting');
	    $array = array();
	    $i = 0;
	    foreach ($tables as $table) {
		$user = $this->student_m->get_username($table, array("email" => $this->input->post('email')));
		if (count($user)) {
		    $this->form_validation->set_message("unique_email", "%s already exists");
		    $array['permition'][$i] = 'no';
		} else {
		    $array['permition'][$i] = 'yes';
		}
		$i++;
	    }

	    if (in_array('no', $array['permition'])) {
		return FALSE;
	    } else {
		return TRUE;
	    }
	}
    }

    function sectioncall() {
	$classesID = $this->input->post('id');

	if ((int) $classesID) {
	    $allsection = $this->section_m->get_order_by_section(array('classesID' => $classesID));
	    echo "<option value='0'>", $this->lang->line("student_select_section"), "</option>";
	    foreach ($allsection as $value) {
		echo "<option value=\"$value->sectionID\">", $value->section, "</option>";
	    }
	}
    }

    public function export() {
	$id = htmlentities($this->uri->segment(3));
	// get all users in array format
	$users = $this->student_m->getStudentParentInfo($id);
	$title = array('student_name', 'date_of_birth', 'section', 'roll', 'guardian', 'father_name', 'mother_name', 'father_profession', 'mother_profession', 'email', 'phone', 'email', 'address');
	$this->exportExcel($users, $title, 'student');
    }

    private function checkKeysExists($value) {
	$required = array('name', 'roll', 'parent_phone', 'class', 'section',
	    'dob', 'sex', 'religion', 'phone', 'parent_status');
	$data = array_shift($value);
	if (count(array_intersect_key(array_flip($required), $data)) === count($required)) {
	    //All required keys exist! 
	    $status = 1;
	} else {
	    $missing = array_intersect_key(array_flip($required), $data);
	    $data_miss = array_diff(array_flip($required), $missing);
	    $status = ' Column with title <b>' . implode(', ', array_keys($data_miss)) . '</b>  miss from Excel file. Please make sure file is in the same format as a sample file';
	}
	return $status;
    }

    public function uploadByFile() {
	$data = $this->uploadExcel();

	$status = $this->checkKeysExists($data);
	if ((int) $status == 1) {
	    $status = '';
	    $password = $this->student_m->hash("abc123456");
	    foreach ($data as $value) {


		/*
		 * To avoid any duplicate, lets check first if this  name & phone
		 */
		$name = preg_replace('/[^a-zA-Z0-9 ]/', '', $value['name']);
		$roll = preg_replace('/[^a-zA-Z0-9 ]/', '', $value['roll']);

		$student_info = $this->student_m->get_single_student(array('name' => $name, 'roll' => $roll));

		//get parent ID
		// Get parent ID
		$parent = $this->student_m->getParentId('0' . $value['parent_phone']);

		//Get class & sections

		$class = $this->classes_m->get_single_class(array('classes_numeric' => $value['class']));

		//get section
		$section = $this->section_m->get_single_section(array('classesID' => $class->classesID, 'section' => $value['section']));

		if (empty($class)) {
		    $status .= '<p class="alert alert-danger">This class "' . $value['class'] . '" '
			    . 'is not yet defined. Please define this class numeric in class navigation</p>';
		} else if (empty($section)) {
		    $status .= '<p class="alert alert-danger">This section "' . $value['section'] . '" '
			    . 'is not yet defined. Please define this section name in section navigation</p>';
		} else if (empty($parent)) {
		    $status .= '<p class="alert alert-danger">This parent phone "' . $value['parent_phone'] . '"'
			    . ' is not registered in the system</p>';
		} else if ($name == '' || $roll == '') {
		    $status .= '<p class="alert alert-danger">Student name and Admission(roll) number'
			    . ' cannot be empty. This record skipped to be uploaded</p>';
		} else if (empty($student_info)) {
		    
		    $acedemic_year_id = $this->academi_year_m->get_current_year($class->classlevel_id);
		    $email = $value['email'] == '' ? $value['parent_phone'] . '@shulesoft.com' : $value['email'];
		    $array = array(
			"name" => $name,
			"dob" => date("Y-m-d", strtotime($value['dob'])),
			"sex" => $value['sex'],
			"religion" => $value['religion'],
			"email" => $email,
			"phone" => $value['phone'],
			"address" => $value['address'],
			"password" => $password,
			"username" => $email,
			"classesID" => $class->classesID,
			"sectionID" => $section->sectionID,
			"section" => $section->section,
			"roll" => $value['roll'],
			'parentID' => $parent->parentID,
			'year' => date('Y'),
			'parent_type' => $value['parent_status'],
			"usertype" => 'Student',
			"photo" => "defualt.png",
			"academi_year_id"=>$acedemic_year_id->id
		    );
		    $this->student_m->insert_student($array);

		    $status .= '<p class="alert alert-success">Student "' . $name . '" uploaded successfully</p>';
		} else {
		    $status .= '<p class="alert alert-info">Student "' . $name . '" exist</p>';
		}
	    }
	}
	$this->session->set_flashdata('success', $this->lang->line('menu_success'));
	$this->data['status'] = $status;
	$this->data["subview"] = "mark/upload_status";
	$this->load->view('_layout_main', $this->data);
    }

}
