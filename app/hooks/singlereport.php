<?php
require_once __DIR__ . '../../../../app/controllers/mark.php';
$usertype = $this->session->userdata("usertype");

if ($usertype == "Admin") {
    ?>
    <div class="well">
        <div class="row">
    	<div class="col-sm-6">
    	    <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span
    		    class="fa fa-print"></span> <?= $this->lang->line('print') ?> </button>
    	    <a href="<?= base_url("exam/printall/" . $exams->examID . '?class_id=' . $student->classesID . '&section_id=' . $student->sectionID) ?>" target="_blank"><i class="fa fa-laptop"></i><?= $this->lang->line('print') ?> all reports</a>
		<?php
		//echo btn_add_pdf('exam/print_preview/' . $exams->examID, $this->lang->line('pdf_preview'))
		?>
    	    <button class="btn-cs btn-sm-cs" data-toggle="modal" data-target="#mail"><span
    		    class="fa fa-envelope-o"></span> <?= $this->lang->line('mail') ?></button>
    	</div>
    	<div class="col-sm-6">
    	    <ol class="breadcrumb">
    		<li><a href="<?= base_url("dashboard/index") ?>"><i
    			    class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
    		<li><a href="<?= base_url("exam/index") ?>"><?= $this->lang->line('menu_parent') ?></a></li>
    		<li class="active"><?= $this->lang->line('view') ?></li>
    	    </ol>
    	</div>
        </div>
    </div>

<?php } else if (strtolower($usertype) == 'parent') { ?>
    <div class="well">
        <div class="row">
    	<div class="col-sm-6">
    	    <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> <?= $this->lang->line('print') ?> </button>
		<?php
		//echo btn_add_pdf('exam/print_preview/' . $exams->examID.'/'.$student->studentID, $this->lang->line('pdf_preview'))
		?>
    	</div>
        </div>
    </div>
<?php } ?>

<div id="printablediv" style="margin: 2%;">
    <section class="panel">

	<div id="p1" style="overflow: hidden; left: 5%; position: relative; width: 935px; height: 240px;">

	    <!-- Begin shared CSS values -->
	    <style type="text/css" >

		.t {
		    -webkit-transform-origin: top left;
		    -moz-transform-origin: top left;
		    -o-transform-origin: top left;
		    -ms-transform-origin: top left;
		    -webkit-transform: scale(0.25);
		    -moz-transform: scale(0.25);
		    -o-transform: scale(0.25);
		    -ms-transform: scale(0.25);
		    z-index: 2;
		    position: absolute;
		    white-space: pre;
		    overflow: visible;
		}
	    </style>
	    <!-- End shared CSS values -->


	    <!-- Begin inline CSS -->
	    <style type="text/css" >

		#t1_1{left:26%;top:47px;letter-spacing:0.2px;word-spacing:-0.4px;}
		#t2_1{left:26%;top:92px;letter-spacing:-0.6px;word-spacing:1.4px;}
		#t3_1{left:26%;top:120px;}
		#t4_1{left:26%;top:142px;}
		#t5_1{left:26%;top:178px;}
		#t6_1{left:26%;top:168px;letter-spacing:-0.4px;word-spacing:0.5px;}
		#t7_1{left:4%;top:190px;}

		.s4_1{
		    FONT-SIZE: 93.3px;
		    FONT-FAMILY: sans-serif;
		    color: rgb(0,0,0);
		}


		.s2_1{
		    FONT-SIZE: 67.2px;
		    FONT-FAMILY: sans-serif;
		    color: rgb(0,0,0);
		}

		.s1_1{
		    FONT-SIZE: 11em;
		    FONT-FAMILY: sans-serif;
		    color: rgb(0,0,0);
		}

		.s3_1{
		    FONT-SIZE: 67.2px;
		    FONT-FAMILY: sans-serif;
		    color: rgb(5,99,193);
		}


		#form1_1{	z-index:2;	cursor: pointer;	border-style:none;	position: absolute;	left:376px;	top:197px;	width:242px;	height:23px;	background: transparent;	font:normal 18px Arial, Helvetica, sans-serif;}

		/*	.table { border: 1px solid black; }
			.table thead > tr > th { border-bottom: none; font-weight: bolder }*/
		.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td { font-weight: bolder }

	    </style>
	    <!-- End inline CSS -->

	    <!-- Begin embedded font definitions -->
	    <style id="fonts1" type="text/css" >

		@font-face {
		    font-family: sans-serif;
		    src: url("../../assets/fonts/sans-serif.woff") format("woff");
		}
		table.table.table-striped td,table.table.table-striped th, table.table {
		    border: 1px solid #000000;
		}
		@media print { 
		    table.table.table-striped td,table.table.table-striped th, table.table {
			border: 1px solid #000000 !important;
		    }
		    .table-stripe {
			background-color: #dedede !important;
			border: 1px solid #000000 !important;
		    }
		}
	    </style>
	    <!-- End embedded font definitions -->

	    <!-- Begin text definitions (Positioned/styled in CSS) -->
	    <div class="letterhead">
		<div id="t1_1" class="t s1_1"><?= $siteinfos->sname ?></div>
		<div id="t2_1" class="t s2_1"><?= 'P.O BOX ' . $siteinfos->box . ', ' . $siteinfos->address ?>.</div>
		<div id="t3_1" class="t s2_1">Cell: <?= str_replace(',', ' / ', $siteinfos->phone) ?></div>
		<div id="t4_1" class="t s2_1">Email: <?= $siteinfos->email ?></div>
		<div id="t5_1" class="t s3_1" ></div>
		<div id="t6_1" class="t s2_1">Website: <?= $siteinfos->website ?></div>
	    </div>
	    <div id="t7_1" class="t s4_1">_______________________________________________________________________________________________________________</div>

	    <!-- End text definitions -->


	    <!-- Begin Form Data -->
	    <form>
		<input type="button"  tabindex="1"  id="form1_1" data-objref="5 0 R" title="mailto:<?= $siteinfos->email ?>" onclick="window.location.href = 'mailto:<?= $siteinfos->email ?>'" />

	    </form>
	    <!-- End Form Data -->
	    <!-- Begin page background -->
	    <div id="pg1Overlay" style="width:100%; height:10%; position:absolute; z-index:1; background-color:rgba(0,0,0,0); -webkit-user-select: none;"></div>
	    <div id="pg1"><?php
		$array = array(
		    "src" => base_url('uploads/images/' . $siteinfos->photo),
		    'width' => '120px',
		    'height' => '140px',
		    'class' => 'img-rounded',
		    'style' => 'margin-left: 5em; padding-top: 4%;'
		);
		echo img($array);
		?></div>


	</div>

	<div class="">
	    <h1 align='center' style="font-size: 23px; font-weight: bolder;padding-top: 0%; ">STUDENT PROGRESSIVE REPORT (<?= strtoupper($classes->classes) . ' ' . $student->year ?>)</h1>
	</div>
	<div class="panel-body profile-view-dis" style="max-height: 50px;">
	    <div class="row" style="margin: 2%;">
		<div class="col-sm-9" style="float:left;" >
		    <h1 style="font-weight: bolder;"> NAME: <?= $student->name ?></h1>
		</div>
		<div class="col-sm-3" style="float: right;">
		    <h1 style="font-weight: bolder; font-size: 14px;"><?=
			$section_id == '' ?
				'CLASS: ' . strtoupper($classes->classes) . '' :
				'SECTION ' . $student->section
			?></h1>
		</div>
		<div style="clear:both;"></div>
	    </div>

	</div>

	<?php if (count($exams) > 0 && !empty($exams)) {
	    ?>

    	<div class="row"  style="margin: 2%;">
		<?php
		//print_r($marks);
		if ($marks && $exams) {

		    $examController = new Mark();
		    ?>

		    <div class="col-lg-12">

			<?php
			$map1 = function($r) {
			    return intval($r->examID);
			};
			$marks_examsID = array_map($map1, $marks);
			$max_semester = max($marks_examsID);

			$map2 = function($r) {
			    return intval($r->examID);
			};
			$examsID = array_map($map2, $exams);

			$map3 = function($r) {
			    return array("mark" => intval($r->mark), "semester" => $r->examID);
			};
			$all_marks = array_map($map3, $marks);

			$map4 = function($r) {
			    return array("gradefrom" => $r->gradefrom, "gradeupto" => $r->gradeupto);
			};
			$grades_check = array_map($map4, $grades);


			echo "<table  class=\"table table-striped table-bordered\" style=\" margin-left: 2%; width:100%;\">";
			if ($exams->examID <= $max_semester) {

			    $check = array_search($exams->examID, $marks_examsID);

			    if ($check >= 0) {
				$f = 0;
				foreach ($grades_check as $key => $range) {
				    foreach ($all_marks as $value) {
					if ($value['semester'] == $exams->examID) {
					    if ($value['mark'] >= $range['gradefrom'] && $value['mark'] <= $range['gradeupto']) {
						$f = 1;
					    }
					}
				    }
				    if ($f == 1) {
					break;
				    }
				}


				echo "<thead>";
				echo "<tr>";
				echo "<th>";
				echo strtoupper($this->lang->line("mark_subject"));
				echo "</th>";
				echo "<th>";
				echo strtoupper($exams->exam);
				echo "</th>";
				if (count($grades) && $f == 1) {
				    echo "<th>";
				    echo strtoupper($this->lang->line("mark_point"));
				    echo "</th>";
				    echo "<th>";
				    echo strtoupper($this->lang->line("mark_grade"));
				    echo "</th>";
				}
				echo "<th>";
				echo strtoupper('Grade Status');
				echo "</th>";
				echo "<th>";
				echo strtoupper($section_id == '' ? 'Position in  Class' : 'Position in  Section');
				echo "</th>";
				if (strtolower($usertype) == 'parent') {
				    echo "<th>";
				    echo 'Position in Section ';
				    echo "</th>";
				}
				echo "</tr>";

				echo "</thead>";
			    }
			}

			echo "<tbody>";

			foreach ($marks as $mark) {
			    if ($exams->examID == $mark->examID) {
				echo "<tr>";
				echo "<td data-title='" . $this->lang->line('mark_subject') . "'>";
				echo strtoupper($mark->subject);
				echo "</td>";
				echo "<td data-title='" . $this->lang->line('mark_mark') . "'>";
				echo $mark->mark;
				echo "</td>";

				/*
				 * ---------------------------------------------------------------
				 * 
				 * We need to check if this subject is under penalty and if a school
				 * accept normal grading for that subject or not
				 * -----------------------------------------------------------------
				 */
				if (count($grades)) {
				    foreach ($grades as $grade) {
					if ($grade->gradefrom <= $mark->mark && $grade->gradeupto >= $mark->mark) {
					    echo "<td data-title='" . $this->lang->line('mark_point') . "'>";
					    echo $grade->point;
					    echo "</td>";
					    echo "<td data-title='" . $this->lang->line('mark_grade') . "'>";
					    echo $grade->grade;
					    echo "</td>";
					    break;
					}
				    }
				}

				echo "<td data-title='" . $this->lang->line('mark_mark') . "'>";
				echo $grade->note;
				echo "</td>";

				echo "<td data-title='" . $this->lang->line('mark_mark') . "'>";

				$subject_info = $examController->get_position($mark->studentID, $mark->examID, $mark->subjectID, $mark->classesID, $section_id);
				echo $subject_info->rank;
				echo "</td>";
				if (strtolower($usertype) == 'parent') {
				    $subject_info_insection = $examController->get_position($mark->studentID, $mark->examID, $mark->subjectID, $mark->classesID, $student->sectionID);
				    echo "<td data-title='" . $this->lang->line('mark_mark') . "'>";
				    echo $subject_info_insection->rank;
				    echo "</td>";
				}
				echo "</tr>";
			    }
			}
			echo "</tbody>";
			echo "</table>";
			if (strtolower($usertype) == 'parent') {
			    $sectionreport = $examController->get_rank_per_all_subjects($exams->examID, $student->classesID, $student->studentID, $student->sectionID);
			    $totalstudent_insection = $examController->get_student_per_class($student->classesID, $student->sectionID);
			}
			?>



			<h1 style="font-weight: bolder; margin-left: 2%;">Summary</h1>

			<table class="table table-striped table-bordered" style=" width: 100%; margin-left: 2%;">
			    <thead>
				<tr>
				    <th style="font-weight: bolder">Total Marks</th>
				    <th style="font-weight: bolder">Average</th>
				    <th style="font-weight: bolder">Number of Students</th>
				    <?= strtolower($usertype) == 'parent' ? '<th style="font-weight: bolder">Number of Students in Section</th>' : '' ?>
				    <th style="font-weight: bolder">Position in <?= $section_id == '' ? 'Class' : 'Section' ?></th>
				    <?= strtolower($usertype) == 'parent' ? '<th style="font-weight: bolder">Position in Section</th>' : '' ?>
				    <th style="font-weight: bolder">Points</th>
				    <th style="font-weight: bolder">Division</th>
				</tr>
			    </thead>
			    <tbody>
				<tr>
				    <td><?= $report->sum ?></td>
				    <td><?= round($report->average, 1) ?></td>
				    <td><?= count($total_students) ?></td>
				    <?= strtolower($usertype) == 'parent' ? '<td>' . $totalstudent_insection->total_student . '</td>' : '' ?>
				    <td><?= $report->rank ?></td>
				    <?= strtolower($usertype) == 'parent' ? '<td>' . $sectionreport->rank . '</td>' : '' ?>
				    <td><?php
					echo $points
					?></td>
				    <td><?php
					echo $division
					?></td>

				</tr>
			    </tbody>
			</table>

			<table class="table table-striped table-bordered" style=" width: 100%; margin-left: 2%;">
			    <thead>
				<tr>
				    <th style="font-weight: bolder">GRADES</th>
				    <?php foreach ($grades as $grade) { ?>
	    			    <th style="font-weight: bolder"><?= $grade->grade ?>=(<?= $grade->gradefrom ?>-<?= $grade->gradeupto ?>)</th>
				    <?php } ?>
				</tr>
			    </thead>

			</table>

		    </div>
		<?php } ?>
    	</div>

    	<div style="padding-left:5%;">


    	    <p>Class Teacher’s Signature: <img src="<?= $class_teacher_signature ?>" width="54" height="32"></p>

    	    <p>General Remarks:  <?php
		    foreach ($grades as $grade) {
			if ($grade->gradefrom <= round($report->average, 0) && $grade->gradeupto >= round($report->average, 0)) {
			    echo "<span class='bolder' style='border-bottom: 1px solid black'>";
			    echo $grade->overall_note;
			    echo "</span>";
			    break;
			}
		    }
		    ?>
    	    </p>


    	    <div>
    		<div style='font-weight: bolder;'>Signature of Headmistress: <img src="<?= $signature ?>" width="54" height="32"></div>
    	    </div>		               	 
    	    <br/>

    	</div>
<?php } ?>
    </section>
</div>

<?php if ($usertype == "Admin") { ?>
    <!-- email modal starts here -->
    <form class="form-horizontal" role="form" action="<?= base_url('exam/send_mail'); ?>" method="post">
        <div class="modal fade" id="mail">
    	<div class="modal-dialog">
    	    <div class="modal-content">
    		<div class="modal-header">
    		    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
    			    class="sr-only">Close</span></button>
    		    <h4 class="modal-title"><?= $this->lang->line('mail') ?></h4>
    		</div>
    		<div class="modal-body">

			<?php
			if (form_error('to'))
			    echo "<div class='form-group has-error' >";
			else
			    echo "<div class='form-group' >";
			?>
    		    <label for="to" class="col-sm-2 control-label">
    <?= $this->lang->line("to") ?>
    		    </label>
    		    <div class="col-sm-6">
    			<input type="email" class="form-control" id="to" name="to" value="<?= set_value('to') ?>">
    		    </div>
    		    <span class="col-sm-4 control-label" id="to_error">
    		    </span>
    		</div>

		    <?php
		    if (form_error('subject'))
			echo "<div class='form-group has-error' >";
		    else
			echo "<div class='form-group' >";
		    ?>
    		<label for="subject" class="col-sm-2 control-label">
    <?= $this->lang->line("subject") ?>
    		</label>
    		<div class="col-sm-6">
    		    <input type="text" class="form-control" id="subject" name="subject"
    			   value="<?= set_value('subject') ?>">
    		</div>
    		<span class="col-sm-4 control-label" id="subject_error">
    		</span>

    	    </div>

		<?php
		if (form_error('message'))
		    echo "<div class='form-group has-error' >";
		else
		    echo "<div class='form-group' >";
		?>
    	    <label for="message" class="col-sm-2 control-label">
    <?= $this->lang->line("message") ?>
    	    </label>
    	    <div class="col-sm-6">
    		<textarea class="form-control" id="message" style="resize: vertical;" name="message"
    			  value="<?= set_value('message') ?>"></textarea>
    	    </div>
    	</div>


        </div>
        <div class="modal-footer">
    	<button type="button" class="btn btn-default" style="margin-bottom:0px;"
    		data-dismiss="modal"><?= $this->lang->line('close') ?></button>
    	<input type="button" id="send_pdf" class="btn btn-success" value="<?= $this->lang->line("send") ?>"/>
        </div>
    </div>
    </div>
    </div>
    </form>
    <!-- email end here -->


    <script language="javascript" type="text/javascript">



        function closeWindow() {
    	location.reload();
        }

        function check_email(email) {
    	var status = false;
    	var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    	if (email.search(emailRegEx) == -1) {
    	    $("#to_error").html('');
    	    $("#to_error").html("<?= $this->lang->line('mail_valid') ?>").css("text-align", "left").css("color", 'red');
    	} else {
    	    status = true;
    	}
    	return status;
        }


        $("#send_pdf").click(function () {
    	var to = $('#to').val();
    	var subject = $('#subject').val();
    	var message = $('#message').val();
    	var id = "<?= $exams->examID ?>";
    	var error = 0;

    	if (to == "" || to == null) {
    	    error++;
    	    $("#to_error").html("");
    	    $("#to_error").html("<?= $this->lang->line('mail_to') ?>").css("text-align", "left").css("color", 'red');
    	} else {
    	    if (check_email(to) == false) {
    		error++
    	    }
    	}

    	if (subject == "" || subject == null) {
    	    error++;
    	    $("#subject_error").html("");
    	    $("#subject_error").html("<?= $this->lang->line('mail_subject') ?>").css("text-align", "left").css("color", 'red');
    	} else {
    	    $("#subject_error").html("");
    	}

    	if (error == 0) {
    	    $.ajax({
    		type: 'POST',
    		url: "<?= base_url('exam/send_mail') ?>",
    		data: 'to=' + to + '&subject=' + subject + "&id=" + id + "&message=" + message,
    		dataType: "html",
    		success: function (data) {
    		    location.reload();
    		}
    	    });
    	}
        });
    </script>

<?php } ?>
<script>
    function printDiv(divID) {
	var myStyle = '<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/print.css'); ?>" media="print" />';
	//Get the HTML of div
	var divElements = document.getElementById(divID).innerHTML;
	//Get the HTML of whole page
	var oldPage = document.body.innerHTML;

	//Reset the page's HTML with div's HTML only
	document.body.innerHTML =
		'<html><head><title></title>' + myStyle + '</head><body>' +
		divElements + '</body>';

	//Print Page
	window.print();

	//Restore orignal HTML
	document.body.innerHTML = oldPage;
    }
</script>

