<?php
/**
 * Description of singlecombined
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
?>

<?php
require_once __DIR__ . '../../../../app/controllers/mark.php';
$usertype = $this->session->userdata("usertype");

if ($usertype == "Admin") {
    ?>
    <div class="well">
        <div class="row">
    	<div class="col-sm-6">
    	    <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> <?= $this->lang->line('print') ?> </button>

    	    <a href="<?= base_url("exam/printall_combined/?class_id=" . $student->classesID . '&section_id=' . $student->sectionID) . '&exam=' . $this->input->get_post('exam') ?>" target="_blank"><i
    		    class="fa fa-laptop"></i><?= $this->lang->line('print') ?> all reports</a>


    	    <button class="btn-cs btn-sm-cs" data-toggle="modal" data-target="#mail"><span class="fa fa-envelope-o"></span> <?= $this->lang->line('mail') ?></button> 	</div>
    	<div class="col-sm-6">
    	    <ol class="breadcrumb">
    		<li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
    		<li><a href="<?= base_url("parentes/index") ?>"><?= $this->lang->line('menu_parent') ?></a></li>
    		<li class="active"><?= $this->lang->line('view') ?></li>
    	    </ol>
    	</div>
        </div>
    </div>
<?php } ?>

<div id="printablediv" style="margin: 2%; font-size: 90%; max-height: 290mm;">
    <section class="panel">
	<div id="p1" style="overflow: hidden; left: 5%; position: relative; width: 935px;max-height: 200px;">

	    <!-- Begin shared CSS values -->
	    <style type="text/css" >
		.t {
		    -webkit-transform-origin: top left;
		    -moz-transform-origin: top left;
		    -o-transform-origin: top left;
		    -ms-transform-origin: top left;
		    -webkit-transform: scale(0.25);
		    -moz-transform: scale(0.25);
		    -o-transform: scale(0.25);
		    -ms-transform: scale(0.25);
		    z-index: 2;
		    position: absolute;
		    white-space: pre;
		    overflow: visible;
		}
	    </style>
	    <!-- End shared CSS values -->


	    <!-- Begin inline CSS -->
	    <style type="text/css" >

		#t1_1{left:26%;top:47px;letter-spacing:0.2px;word-spacing:-0.4px;}
		#t2_1{left:26%;top:92px;letter-spacing:-0.6px;word-spacing:1.4px; font-size: 4em;}
		#t3_1{left:26%;top:120px; font-size: 4em;}
		#t4_1{left:26%;top:142px; font-size: 4em;}
		#t5_1{left:26%;top:178px; font-size: 4em;}
		#t6_1{left:26%;top:168px;letter-spacing:-0.4px;word-spacing:0.5px; font-size: 4em;font-size: 4em;}
		#t7_1{left:4%;top:190px; border-bottom: 8px solid black;
		width: 70em;}

		.s4_1{
		    FONT-SIZE: 93.3px;
		    FONT-FAMILY: sans-serif;
		    color: rgb(0,0,0);
		}


		.s2_1{
		    FONT-SIZE: 67.2px;
		    FONT-FAMILY: sans-serif;
		    color: rgb(0,0,0);
		}

		.s1_1{
		    FONT-SIZE: 11em;
		    FONT-FAMILY: sans-serif;
		    color: rgb(0,0,0);
		}

		.s3_1{
		    FONT-SIZE: 67.2px;
		    FONT-FAMILY: sans-serif;
		    color: rgb(5,99,193);
		}

		#form1_1{	z-index:2;	cursor: pointer;	border-style:none;	position: absolute;	left:376px;	top:197px;	width:242px;	height:23px;	background: transparent;	font:normal 18px Arial, Helvetica, sans-serif;}

	    </style>
	    <!-- End inline CSS -->

	    <!-- Begin embedded font definitions -->
	    <style id="fonts1" type="text/css" >

		@font-face {
		    font-family: DejaVuSans_b;
		    src: url("../../assets/fonts/DejaVuSans_b.woff") format("woff");
		}
		table.table.table-striped td,table.table.table-striped th, table.table {
		    border: 1px solid #000000;
		    margin: 0 auto !important;
		}
		#report_footer{
		    margin-left: 2em;
		}
		.headings{
		    margin-left: 2em;
		}
		@media print { 
		    table.table.table-striped td,table.table.table-striped th, table.table {
			border: 1px solid #000000 !important;

		    }
		    .table-stripe {
			background-color: #dedede !important;
			border: 1px solid #000000 !important;
		    }
		    body, .panel {
			margin: 0;
			box-shadow: 0;
		    }
		   

		}

	    </style>
	    <?php if ($level->result_format == 'CSEE') { ?>
    	    <style  type="text/css" >
    		.panel{
    		    color: #000000 !important;

    		}
    		#p1{
    		    height: 220px !important;
    		}
    		table.table.table-striped td,table.table.table-striped th, table.table {
    		    font-size: 12px !important;
    		    margin-left: 10%;
    		}
    		@media print { 
    		    .panel{
    			padding-left: 10%;
    		    }
    		    table.table.table-striped td,table.table.table-striped th, table.table {
    			border: 1px solid #000000 !important;
    			margin-left: 1% !important;
    			font-size: 11px !important;
    		    }
    		    .table-stripe {
    			background-color: #dedede !important;
    			border: 1px solid #000000 !important;
    		    }
    		    body, .panel {
    			margin: 0;
    			box-shadow: 0;
    		    }
    		    table.table{
    			width: 55em !important;
			max-width: 55em !important;
    			margin-left:auto; 
    			margin-right:auto;}
    		    table.table.table-striped td,table.table.table-striped th, table.table td{
    			padding: 4px !important;
    		    }
    		    .headings{
    			margin-left:2em;
    		    }
		     #t7_1{
			width: 25em !important;
		    }
		    
    		}

    	    </style>
	    <?php } ?>
	    <!-- End embedded font definitions -->
	    <div class="letterhead">
		<div id="t1_1" class="t s1_1"><?= $siteinfos->sname ?></div>
		<div id="t2_1" class="t s2_1"><?= 'P.O BOX ' . $siteinfos->box . ', ' . $siteinfos->address ?>.</div>
		<div id="t3_1" class="t s2_1">Cell: <?= str_replace(',', ' / ', $siteinfos->phone) ?></div>
		<div id="t4_1" class="t s2_1">Email: <?= $siteinfos->email ?></div>
		<div id="t5_1" class="t s3_1" ></div>
		<div id="t6_1" class="t s2_1">Website: <?= $siteinfos->website ?></div>
	    </div>
	    <div id="t7_1" class="t s4_1"></div>

	    <!-- End text definitions -->


	    <!-- Begin Form Data -->
	    <form>
		<input type="button"  tabindex="1"  id="form1_1" data-objref="5 0 R" title="mailto:<?= $siteinfos->email ?>" onclick="window.location.href = 'mailto:<?= $siteinfos->email ?>'" />

	    </form>
	    <!-- End Form Data -->
	    <!-- Begin page background -->
	    <div id="pg1Overlay" style="width:100%; height:10%; position:absolute; z-index:1; background-color:rgba(0,0,0,0); -webkit-user-select: none;"></div>
	    <div id="pg1"><?php
		$array = array(
		    "src" => base_url('uploads/images/' . $siteinfos->photo),
		    'width' => '160px',
		    'height' => '180px',
		    'class' => 'img-rounded',
		    'style' => 'margin-left:2em; padding-top: 5%;'
		);
		echo img($array);
		?></div>


	</div>

	<div class="">
	    <h1 align='center' style="font-size: 19px; margin-top: 0.7em; font-weight: bolder">STUDENT PROGRESSIVE REPORT (<?= strtoupper($classes->classes) . ' ' . $student->year ?>)</h1>
	</div>
	<div <?php if ($level->result_format != 'CSEE') { ?> class="panel-body profile-view-dis" <?php } ?>>
	    <div class="row" style="margin: 2%;">
		<div class="col-sm-6" style="float:left;" >
		    <h1 style="font-weight: bolder;"> NAME: <?= $student->name ?></h1>
		</div>
		<div class="col-sm-3" style="float: right;">
		    <h1 style="font-weight: bolder; font-size: 14px;"><?=
			(isset($section_id) && $section_id == '') ?
				'CLASS: ' . strtoupper($classes->classes) . '' :
				'SECTION ' . $student->section
			?></h1>
		</div>
		<div style="clear:both;"></div>
	    </div>

	</div>

	<?php if (count($exams) > 0 && !empty($exams)) {
	    ?>

    	<div class="row"  style="margin:0 2%; ">
		<?php
		if ($marks && $exams) {

		    $examController = new Exam();
		    ?>
		    <div class="col-lg-12">
			<table class="table table-striped table-bordered center">
			    <thead>
				<tr>
				    <th style="font-weight: bolder">SUBJECT</th>

				    <?php foreach ($exams as $exam) { ?>
	    			    <th style="font-weight: bolder"><?= strtoupper($exam->exam) ?></th>
				    <?php } ?>

				    <th style="font-weight: bolder">AVERAGE</th>
				    <th style="font-weight: bolder">GRADE</th>
				    <?php if ($level->result_format == 'ACSEE') { ?>
	    			    <th style="font-weight: bolder">POINTS</th>
				    <?php } ?>
				    <th style="font-weight: bolder">REMARKS</th>
				</tr>
			    </thead>
			    <tbody>
				<?php
				$total_marks = 0;
				foreach ($subjects as $subject) {
				    ?>
	    			<tr>
	    			    <td style="font-weight: bolder"><?= strtoupper($subject->subject) ?></td>

					<?php
					$total = 0;
					$penalty = 0;
					$exam_done=0;
					foreach ($exams as $exam) {

					    $mark = $examController->get_total_marks($student->studentID, $exam->examID, $subject->classesID, $subject->subjectID);
					    $total+=$mark->total_mark;

					    /*
					     * check penalty for that subject
					     */
					    if ($subject->is_penalty == 1) {
//						foreach ($grades as $grade) {
//						    if ($grade->gradefrom == 0) {
//							if ($mark->total_mark < $grade->gradeupto) {
//							    $penalty = 1;
//							}
//						    }
//						}
						$penalty = $mark->total_mark < $subject->pass_mark ? 1 : 0;
					    }
					    ?>
					    <th><?= $mark->total_mark==0 ? '-':$mark->total_mark?></th>
					    <?php
					    if($mark->total_mark =='' || $mark->total_mark ==0){
						 
					    }else{
						$exam_done++; 
					    }
					}
					$avg =$exam_done !=0 ?  round($total / $exam_done, 1) : 0;
					?>

	    			    <td><?= $avg==0 ? '-' :$avg ?></td>

					<?php
					if($avg==0){
					    //this is a short time solution, we put empty but
					    //later on we need to see if this student takes this subject or not
					    echo '<td></td><td></td>';
					}else{
					echo return_grade($grades, round($avg), $level->result_format);
					}
					$total_marks+=$avg;
					?>
	    			</tr>
				    <?php
				}

				/**
				 * 
				 * 
				 * ----------------------------------------------------------------
				 * 
				 * We show Division & points only to A-level students. Those with
				 * grade format of ACSEE
				 */
				if ($level->result_format == 'ACSEE') {
				    ?>			

	    			<tr>
	    			    <td style="font-weight: bolder">DIVISION</td>
					<?php
					$i = 0;
					$total_points = 0;
					foreach ($exams as $exam) {
					    $division[$i] = $examController->getDivision($student->studentID, $exam->examID);
					    $rank_info = $examController->get_rank_per_all_subjects($exam->examID, $student->classesID, $student->studentID);
					    ?>
					    <td><?= $division[$i][0] ?></td>
					    <?php
					    $total_points+=$division[$i][1];
					    $i++;
					}
					$total_i = $i;
					$total_average_points = floor($total_points / $total_i);
					if ($level->result_format == 'CSEE') {
					    $div = cseeDivision($total_average_points, $penalty);
					} elseif ($level->result_format == 'ACSEE') {
					    $div = acseeDivision($total_average_points, $penalty);
					}
					?>
	    			    <td><?= $div ?></td>
	    			    <td></td>
	    			    <td></td>
	    			    <td></td>
	    			</tr>
	    			<tr>
	    			    <td style="font-weight: bolder">POINTS</td>
					<?php
					for ($i = 0; $i < $total_i; $i++) {
					    ?>
					    <td><?= $division[$i][1] ?></td>
					    <?php
					}
					?>
	    			    <td><?= $total_average_points ?></td>
	    			    <td></td>
	    			    <td></td>
	    			    <td></td>
	    			</tr>

				    <?php
				}

				/**
				 * Otherwise, we don't show points and division
				 * ------------------------------------------------------
				 */
				$section = isset($section_id) && $section_id == '' ? 'CLASS' : 'SECTION';
				$sectionID = $section == 'CLASS' ? NULL : $student->sectionID;
				?>
				<tr>
				    <td style="font-weight: bolder">POSITION IN <?= $section ?></td>
				    <?php
				    foreach ($exams as $exam) {

					$rank_info = $examController->get_rank_per_all_subjects($exam->examID, $student->classesID, $student->studentID, $sectionID);
					?>
	    			    <td><?= $rank_info->rank ?></td>
					<?php
				    }
				    ?>
				    <td><?= $rank ?></td>
				    <?= $level->result_format == 'ACSEE' ? "<td></td>" : '' ?>
				    <td></td>
				    <td></td>
				</tr>
			    </tbody>
			</table>


			<div class="">Summary</div>


			<table class="table table-striped table-bordered">
			    <thead>
				<tr>
				    <th style="font-weight: bolder">Total Marks</th>
				    <th style="font-weight: bolder">Performance Average</th>
				    <th style="font-weight: bolder">Number of Students</th>
				    <th style="font-weight: bolder">Position In <?= isset($section_id) && $section_id == '' ? 'Class' : 'Section' ?></th>

				</tr>
			    </thead>
			    <tbody>
				<tr>
				    <td><?= $total_marks ?></td>
				    <td><?php
				    $total_average=round($total_marks / count($subjects), 1);
				    echo $total_average;?></td>
				    <td><?= count($students) ?></td>
				    <td><?= $rank ?></td>

				</tr>
			    </tbody>
			</table>

			<br>
			<table class="table table-striped table-bordered">
			    <thead>
				<tr>
				    <th style="font-weight: bolder">GRADES</th>
				    <?php foreach ($grades as $grade) { ?>
	    			    <th><?= $grade->grade ?> = (<?= $grade->gradefrom ?>-<?= $grade->gradeupto ?>)</th>
				    <?php } ?>
				</tr>
			    </thead>
			</table>
		    </div>
		<?php } ?>
    	</div>

	<?php } ?>
	<div  class="headings">


	    <p>Class Teacher’s Signature: <img src="<?= $class_teacher_signature ?>" width="54" height="32"></p>

	     <p>General Remarks:  <?php
		    foreach ($grades as $grade) {
			if ($grade->gradefrom <= round($total_average, 0) && $grade->gradeupto >= round($total_average, 0)) {
			    echo "<span class='bolder' style='border-bottom: 1px solid black'>";
			    echo $grade->overall_note;
			    echo "</span>";
			    break;
			}
		    }
		    ?>
    	    </p>
	    <div>
		<div>Signature of Headmistress: <img src="<?= $signature ?>" width="54" height="32"></div>
	    </div>		               	 
	    <br/>

	</div>
    </section>
    <div class="page-break"></div>
</div>

<?php if ($usertype == "Admin") { ?>
    <!-- email modal starts here -->
    <form class="form-horizontal" role="form" action="<?= base_url('parentes/send_mail'); ?>" method="post">
        <div class="modal fade" id="mail">
    	<div class="modal-dialog">
    	    <div class="modal-content">
    		<div class="modal-header">
    		    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    		    <h4 class="modal-title"><?= $this->lang->line('mail') ?></h4>
    		</div>
    		<div class="modal-body">

			<?php
			if (form_error('to'))
			    echo "<div class='form-group has-error' >";
			else
			    echo "<div class='form-group' >";
			?>
    		    <label for="to" class="col-sm-2 control-label">
			    <?= $this->lang->line("to") ?>
    		    </label>
    		    <div class="col-sm-6">
    			<input type="email" class="form-control" id="to" name="to" value="<?= set_value('to') ?>" >
    		    </div>
    		    <span class="col-sm-4 control-label" id="to_error">
    		    </span>
    		</div>

		    <?php
		    if (form_error('subject'))
			echo "<div class='form-group has-error' >";
		    else
			echo "<div class='form-group' >";
		    ?>
    		<label for="subject" class="col-sm-2 control-label">
			<?= $this->lang->line("subject") ?>
    		</label>
    		<div class="col-sm-6">
    		    <input type="text" class="form-control" id="subject" name="subject" value="<?= set_value('subject') ?>" >
    		</div>
    		<span class="col-sm-4 control-label" id="subject_error">
    		</span>

    	    </div>

		<?php
		if (form_error('message'))
		    echo "<div class='form-group has-error' >";
		else
		    echo "<div class='form-group' >";
		?>
    	    <label for="message" class="col-sm-2 control-label">
		    <?= $this->lang->line("message") ?>
    	    </label>
    	    <div class="col-sm-6">
    		<textarea class="form-control" id="message" style="resize: vertical;" name="message" value="<?= set_value('message') ?>" ></textarea>
    	    </div>
    	</div>


        </div>
        <div class="modal-footer">
    	<button type="button" class="btn btn-default" style="margin-bottom:0px;" data-dismiss="modal"><?= $this->lang->line('close') ?></button>
    	<input type="button" id="send_pdf" class="btn btn-success" value="<?= $this->lang->line("send") ?>" />
        </div>
    </div>
    </div>
    </div>
    </form>
    <!-- email end here -->

    <script language="javascript" type="text/javascript">
        function printDiv(divID) {
    	//Get the HTML of div
    	var divElements = document.getElementById(divID).innerHTML;
    	//Get the HTML of whole page
    	var oldPage = document.body.innerHTML;

    	//Reset the page's HTML with div's HTML only
    	document.body.innerHTML =
    		"<html><head><title></title></head><body>" +
    		divElements + "</body>";

    	//Print Page
    	window.print();

    	//Restore orignal HTML
    	document.body.innerHTML = oldPage;
        }
        function closeWindow() {
    	location.reload();
        }

        function check_email(email) {
    	var status = false;
    	var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    	if (email.search(emailRegEx) == -1) {
    	    $("#to_error").html('');
    	    $("#to_error").html("<?= $this->lang->line('mail_valid') ?>").css("text-align", "left").css("color", 'red');
    	} else {
    	    status = true;
    	}
    	return status;
        }


        $("#send_pdf").click(function () {
    	var to = $('#to').val();
    	var subject = $('#subject').val();
    	var message = $('#message').val();
    	var id = "<?php // $exams->examID                       ?>";
    	var error = 0;

    	if (to == "" || to == null) {
    	    error++;
    	    $("#to_error").html("");
    	    $("#to_error").html("<?= $this->lang->line('mail_to') ?>").css("text-align", "left").css("color", 'red');
    	} else {
    	    if (check_email(to) == false) {
    		error++
    	    }
    	}

    	if (subject == "" || subject == null) {
    	    error++;
    	    $("#subject_error").html("");
    	    $("#subject_error").html("<?= $this->lang->line('mail_subject') ?>").css("text-align", "left").css("color", 'red');
    	} else {
    	    $("#subject_error").html("");
    	}

    	if (error == 0) {
    	    $.ajax({
    		type: 'POST',
    		url: "<?= base_url('parentes/send_mail') ?>",
    		data: 'to=' + to + '&subject=' + subject + "&id=" + id + "&message=" + message,
    		dataType: "html",
    		success: function (data) {
    		    location.reload();
    		}
    	    });
    	}
        });
    </script>

<?php } ?>
